package com.trade_accounting.components.purchases;

import com.trade_accounting.components.general.ProductSelectModal;
import com.trade_accounting.components.profile.SalesChannelModalWindow;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.company.ContractDto;
import com.trade_accounting.models.dto.company.ContractorDto;
import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.models.dto.invoice.InvoiceDto;
import com.trade_accounting.models.dto.invoice.InvoiceProductDto;
import com.trade_accounting.models.dto.units.SalesChannelDto;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.ProductDto;
import com.trade_accounting.models.dto.warehouse.ProductPriceDto;
import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.company.ContractService;
import com.trade_accounting.services.interfaces.company.ContractorService;
import com.trade_accounting.services.interfaces.company.SupplierAccountService;
import com.trade_accounting.services.interfaces.invoice.InvoiceProductService;
import com.trade_accounting.services.interfaces.invoice.InvoiceService;
import com.trade_accounting.services.interfaces.units.SalesChannelService;
import com.trade_accounting.services.interfaces.util.ProjectService;
import com.trade_accounting.services.interfaces.warehouse.ProductService;
import com.trade_accounting.services.interfaces.warehouse.SupplierAccountProductsListService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Shortcuts;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import retrofit2.Response;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.trade_accounting.config.SecurityConstants.PURCHASES_SUPPLIERS_INVOICES_VIEW;

@Slf4j
@PreserveOnRefresh
@SpringComponent
@UIScope
public class SupplierAccountAddOrEditView  extends VerticalLayout implements BeforeLeaveObserver {
    private final PurchasesSubMenuView purchasesSubMenuView;
    private final ProductService productService;
    private final SupplierAccountService supplierAccountService;
    private final CompanyService companyService;
    private final WarehouseService warehouseService;
    private final ContractorService contractorService;
    private final ContractService contractService;
    private final ProjectService projectService;
    private final InvoiceService invoiceService;
    private final InvoiceProductService invoiceProductService;
    private final EmployeeService employeeService;
    private final SupplierAccountProductsListService supplierAccountProductsListService;
    private final SalesChannelService salesChannelService;
    private SupplierAccountDto supplierAccountDtoToSave = new SupplierAccountDto();
    private final ComboBox<CompanyDto> companyDtoComboBox = new ComboBox<>();
    private final ComboBox<WarehouseDto> warehouseDtoComboBox = new ComboBox<>();
    private final ComboBox<ContractDto> contractDtoComboBox = new ComboBox<>();
    private final ComboBox<ProjectDto> projectDtoComboBox = new ComboBox<>();
    private final ComboBox<ContractorDto> contractorDtoComboBox = new ComboBox<>();
    private final ComboBox<SalesChannelDto> salesChannelSelect = new ComboBox<>();
    private final ComboBox<InvoiceDto> invoiceSelectField = new ComboBox<>();
    private final DateTimePicker dateTimePicker = new DateTimePicker();
    private final DatePicker paymentDatePicker = new DatePicker();
    private final DatePicker incomingDatePicker = new DatePicker();
    private final TextField text = new TextField();
    private final Checkbox isSpend = new Checkbox("Проведено");
    private final Checkbox isSent = new Checkbox("Отправлено");
    private final Checkbox isPrint = new Checkbox("Напечатано");
    private final TextField invoiceNumber = new TextField();
    private final TextArea commentConfig = new TextArea();
    private final Notifications notifications;
    private final Binder<SupplierAccountDto> supplierAccountDtoBinder = new Binder<>(SupplierAccountDto.class);
    private final String TEXT_FOR_REQUEST_FIELD = "Обязательное поле";
    private final Dialog dialogOnCloseView = new Dialog();
    private Grid<SupplierAccountProductsListDto> grid = new Grid<>(SupplierAccountProductsListDto.class, false);
    private final GridPaginator<SupplierAccountProductsListDto> paginator;
    private final ProductSelectModal productSelectModal;
    private List<SupplierAccountProductsListDto> tempSupplierAccountProductsListDtoList = new ArrayList<>();
    private List<SupplierAccountProductsListDto> supplierAccountProductsListDtoList = new ArrayList<>();
    private final H4 totalPrice = new H4();
    private final Button deleteButton = new Button("Удалить", new Icon(VaadinIcon.TRASH));
    private final Button buttonFillFromOrder = new Button("Заполнить");
    private final H2 title = new H2();
    private static final String LABEL_WIDTH = "150px";
    private static final String FIELD_WIDTH = "350px";
    private String location = null;

    @Autowired
    public SupplierAccountAddOrEditView(SupplierAccountService supplierAccountService,
                                           CompanyService companyService,
                                           WarehouseService warehouseService,
                                           ContractorService contractorService,
                                           ContractService contractService,
                                           ProjectService projectService, Notifications notifications,
                                           ProductSelectModal productSelectModal,
                                           ProductService productService,
                                           InvoiceService invoiceService,
                                           InvoiceProductService invoiceProductService,
                                           SupplierAccountProductsListService supplierAccountProductsListService,
                                           @Lazy PurchasesSubMenuView purchasesSubMenuView,
                                           SalesChannelService salesChannelService,
                                           EmployeeService employeeService) {
        this.supplierAccountService = supplierAccountService;
        this.companyService = companyService;
        this.warehouseService = warehouseService;
        this.contractorService = contractorService;
        this.contractService = contractService;
        this.projectService = projectService;
        this.notifications = notifications;
        this.productSelectModal = productSelectModal;
        this.productService = productService;
        this.invoiceService = invoiceService;
        this.invoiceProductService = invoiceProductService;
        this.supplierAccountProductsListService = supplierAccountProductsListService;
        this.purchasesSubMenuView = purchasesSubMenuView;
        this.salesChannelService = salesChannelService;
        this.employeeService = employeeService;

        configureGrid();
        configureInvoiceSelectField();
        paginator = new GridPaginator<>(grid, tempSupplierAccountProductsListDtoList, 50);
        configureCloseViewDialog();
        setSizeFull();
        configureDateTimePickerField();
        add(upperButtonInModalView(), formToAddInvoicesToBuyers(), grid, commentAndTotalPriceLayout());
        title.setWidthFull();
        buttonFillInOrder();
        productSelectModal.addDetachListener(detachEvent -> {
            if (productSelectModal.isFormValid()) {
                addProduct(productSelectModal.productSelect.getValue(),
                        productSelectModal.priceSelect.getValue(),
                        productSelectModal.amountField.getValue());
                productSelectModal.productSelect.setValue(null);
                productSelectModal.priceSelect.setValue(null);
                productSelectModal.amountField.setValue(BigDecimal.valueOf(0.0));
            }
        });
    }

    private void buttonFillInOrder() {
        buttonFillFromOrder.addClickListener(buttonClickEvent -> {
            dateTimePicker.setValue(LocalDateTime.parse(invoiceSelectField.getValue().getDate()));
            commentConfig.setValue(invoiceSelectField.getValue().getComment());
            companyDtoComboBox.setValue(companyService.getById(invoiceSelectField.getValue().getCompanyId()));
            contractorDtoComboBox.setValue(contractorService.getById(invoiceSelectField.getValue().getContractorId()));
            warehouseDtoComboBox.setValue(warehouseService.getById(invoiceSelectField.getValue().getWarehouseId()));
            isSpend.setValue(invoiceSelectField.getValue().getIsSpend());
            tempSupplierAccountProductsListDtoList = convertInvoiceProdToSupplier(invoiceProductService.getByInvoiceId(invoiceSelectField.getValue().getId()));
            paginator.setData(tempSupplierAccountProductsListDtoList);
            setTotalPrice();
        });
    }

    private void configureInvoiceSelectField() {
        List<InvoiceDto> invoices = invoiceService.getAll("EXPENSE");
        if (invoices != null) {
            invoiceSelectField.setItems(invoices);
        }
        invoiceSelectField.setWidth("500px");
        invoiceSelectField.setPlaceholder("Заказ");
        invoiceSelectField.setItemLabelGenerator(e -> "Id: " + e.getId() +
                ", Контрагент: " + contractorService.getById(e.getContractorId()).getName() +
                ", Компания: " + companyService.getById(e.getCompanyId()).getName()  +
                ", на сумму: " + getTotalPriceForSelectFild(e));
    }

    public BigDecimal getTotalPriceForSelectFild(InvoiceDto invoiceDto) {
        BigDecimal totalPriceForSelectFild = BigDecimal.valueOf(0);
        for (InvoiceProductDto iDto : invoiceProductService.getByInvoiceId(invoiceDto.getId())) {
            totalPriceForSelectFild = totalPriceForSelectFild.add(iDto.getPrice().multiply(iDto.getAmount()));
        }
        return totalPriceForSelectFild;
    }

    public void addProduct(ProductDto productDto, ProductPriceDto productPriceDto, BigDecimal amount) {
        SupplierAccountProductsListDto supplierAccountProductsListDto = new SupplierAccountProductsListDto();
        supplierAccountProductsListDto.setProductId(productDto.getId());
        supplierAccountProductsListDto.setAmount(amount);
        supplierAccountProductsListDto.setPrice(productPriceDto.getValue());
        supplierAccountProductsListDto.setSum(supplierAccountProductsListDto.getAmount().multiply(supplierAccountProductsListDto.getPrice()).setScale(2, RoundingMode.DOWN));
        supplierAccountProductsListDto.setPercentNds("20");
        supplierAccountProductsListDto.setNds(supplierAccountProductsListDto.getSum().multiply(BigDecimal.valueOf(0.2)).setScale(2, RoundingMode.DOWN));
        supplierAccountProductsListDto.setTotal(supplierAccountProductsListDto.getSum().add(supplierAccountProductsListDto.getNds()));
        if (!isProductInList(productDto)) {
            tempSupplierAccountProductsListDtoList.add(supplierAccountProductsListDto);
            paginator.setData(tempSupplierAccountProductsListDtoList);
            setTotalPrice();
        }
    }

    private void configureGrid() {
        grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        grid.removeAllColumns();
        grid.setItems(tempSupplierAccountProductsListDtoList);
        grid.addColumn(inPrDto -> tempSupplierAccountProductsListDtoList.indexOf(inPrDto) + 1).setHeader("№");
        grid.addColumn(inPrDto -> productService.getById(inPrDto.getProductId()).getName()).setHeader("Название").setSortable(true);
        grid.addColumn(inPrDto -> productService.getById(inPrDto.getProductId()).getDescription()).setHeader("Описание");
        grid.addColumn(SupplierAccountProductsListDto::getAmount).setHeader("Количество").setSortable(true).setKey("productAmount").setId("Количество");
        grid.addColumn(SupplierAccountProductsListDto::getPrice).setHeader("Цена").setSortable(true).setKey("productPrice").setId("Цена");
        grid.addColumn(SupplierAccountProductsListDto::getSum).setHeader("Сумма").setSortable(true).setKey("productSum").setId("Сумма");
        grid.addColumn(SupplierAccountProductsListDto::getPercentNds).setHeader("% НДС").setSortable(true).setKey("productPercentNds").setId("% НДС");
        grid.addColumn(SupplierAccountProductsListDto::getNds).setHeader("НДС").setSortable(true).setKey("productNds").setId("НДС");
        grid.addColumn(SupplierAccountProductsListDto::getTotal).setHeader("Всего").setSortable(true).setKey("productTotal").setId("Всего");

        grid.addComponentColumn(column -> {
            Button edit = new Button(new Icon(VaadinIcon.TRASH));
            edit.addClassName("delete");
            edit.addClickListener(e -> deleteProduct(column.getId()));
            return edit;
        });
    }

    public void clearField() {
        incomingDatePicker.setValue(null);
        companyDtoComboBox.setValue(null);
        warehouseDtoComboBox.setValue(null);
        contractDtoComboBox.setValue(null);
        contractorDtoComboBox.setValue(null);
        invoiceSelectField.setValue(null);
        configureInvoiceSelectField();
        dateTimePicker.setValue(null);
        projectDtoComboBox.setValue(null);
        paymentDatePicker.setValue(null);
        text.setValue("");
        isSpend.setValue(false);
        invoiceNumber.setValue("");
        commentConfig.setValue("");
        totalPrice.setText("");
        tempSupplierAccountProductsListDtoList = new ArrayList<>();
        isSent.setValue(false);
        isPrint.setValue(false);
        isSpend.setValue(false);
        configureGrid();
    }

    public void setSupplierAccountsForEdit(SupplierAccountDto editSupplierAccounts) {
        supplierAccountDtoBinder.setValidatorsDisabled(true);
        supplierAccountDtoToSave = editSupplierAccounts;
        deleteButton.addClickListener(buttonClickEvent -> {
            deleteInvoice(editSupplierAccounts);
            resetSourceTab(false);
            supplierAccountDtoBinder.setValidatorsDisabled(true);
            clearField();
        });
        invoiceNumber.setValue(editSupplierAccounts.getId().toString());
        dateTimePicker.setValue(LocalDateTime.parse(editSupplierAccounts.getDate(), DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));
        paymentDatePicker.setValue(LocalDateTime.parse(editSupplierAccounts.getPlannedDatePayment(), DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")).toLocalDate());
        commentConfig.setValue(supplierAccountDtoToSave.getComment());
        companyDtoComboBox.setValue(companyService.getById(supplierAccountDtoToSave.getCompanyId()));
        warehouseDtoComboBox.setValue(warehouseService.getById(supplierAccountDtoToSave.getWarehouseId()));
        contractDtoComboBox.setValue(contractService.getById(supplierAccountDtoToSave.getContractId()));
        contractorDtoComboBox.setValue(contractorService.getById(supplierAccountDtoToSave.getContractorId()));
        if (editSupplierAccounts.getProjectId() != null) {
            projectDtoComboBox.setValue(projectService.getById(editSupplierAccounts.getProjectId()));
        }
        isSpend.setValue(editSupplierAccounts.getIsSpend());
        isSent.setValue(editSupplierAccounts.getIsSent());
        isPrint.setValue(editSupplierAccounts.getIsPrint());
        salesChannelSelect.setItems(salesChannelService.getAll());
        tempSupplierAccountProductsListDtoList = supplierAccountProductsListService.getBySupplierId(supplierAccountDtoToSave.getId());
        supplierAccountProductsListDtoList = supplierAccountProductsListService.getBySupplierId(supplierAccountDtoToSave.getId());
        paginator.setData(tempSupplierAccountProductsListDtoList);
        setTotalPrice();
    }

    private HorizontalLayout upperButtonInModalView() {
        HorizontalLayout upperButton = new HorizontalLayout();
        upperButton.add(title, buttonsAndProductsAndInvoiceslayout());
        upperButton.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        return upperButton;
    }

    private VerticalLayout buttonsAndProductsAndInvoiceslayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayoutFirstRow = new HorizontalLayout();
        horizontalLayoutFirstRow.add(saveButton(), deleteButton, closeButton(), addProductButton());
        HorizontalLayout horizontalLayoutSecondRow = new HorizontalLayout();
        horizontalLayoutSecondRow.add(invoiceSelectField, buttonFillFromOrder);
        verticalLayout.add(horizontalLayoutFirstRow, horizontalLayoutSecondRow);
        return verticalLayout;
    }
    private Button saveButton() {
        return new Button("Сохранить", e -> {
            supplierAccountDtoBinder.setValidatorsDisabled(false);
            if (!supplierAccountDtoBinder.validate().isOk()) {
                supplierAccountDtoBinder.validate().notifyBindingValidationStatusHandlers();
            } else {
                SupplierAccountDto dto = new SupplierAccountDto();
                if (!invoiceNumber.getValue().equals("")) {
                    dto.setId(Long.parseLong(invoiceNumber.getValue()));
                }
                dto.setDate(dateTimePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
                dto.setPlannedDatePayment(
                                LocalDateTime.of(
                                        paymentDatePicker.getValue(),LocalTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
                dto.setCompanyId(companyDtoComboBox.getValue().getId());
                dto.setWarehouseId(warehouseDtoComboBox.getValue().getId());
                dto.setContractId(contractDtoComboBox.getValue().getId());
                dto.setTypeOfInvoice("EXPENSE");
                dto.setContractorId(contractorDtoComboBox.getValue().getId());
                dto.setProjectId(projectDtoComboBox.getValue() == null ?
                        null : projectDtoComboBox.getValue().getId());
                dto.setIsSpend(isSpend.getValue());
                dto.setIsRecyclebin(false);
                dto.setIsSent(isSent.getValue());
                dto.setIsPrint(isPrint.getValue());
                dto.setComment(commentConfig.getValue());
                dto.setEmployeeId(employeeService.getPrincipal().getId());
                dto.setLastModifiedDate(LocalDateTime.now().toString());
                dto.setId(supplierAccountService.create(dto).getId());
                supplierAccountService.create(dto);

                deleteRemovedProducts();

                saveProducts(dto);
                resetSourceTab(false);
                notifications.infoNotification(String.format("Счет поставщика № %s сохранен", dto.getId()));
                supplierAccountDtoBinder.setValidatorsDisabled(true);
                clearField();
            }
        });
    }

    private Button closeButton() {
        return new Button("Закрыть",
                new Icon(VaadinIcon.CLOSE),
                event -> {
                    resetSourceTab(true);
                });
    }

    private Button addProductButton() {
        Button button = new Button("Добавить из справочника", new Icon(VaadinIcon.PLUS_CIRCLE));
        button.addClickListener(buttonClickEvent -> {
            productSelectModal.updateProductList();
            productSelectModal.open();
        });
        return button;
    }

    private VerticalLayout formToAddInvoicesToBuyers() {
        VerticalLayout form = new VerticalLayout();
        form.add(horizontalLayout1(), horizontalLayout2(),
                horizontalLayout3(), dataPlaneConfigure(), configureSalesChannelSelect(), incomingConfigure());
        return form;
    }

    private HorizontalLayout horizontalLayout1() {
        HorizontalLayout hLay1 = new HorizontalLayout();
        hLay1.add(dataConfigure(), isSpend, isSent, isPrint);
        return hLay1;
    }

    private HorizontalLayout horizontalLayout2() {
        HorizontalLayout hLay2 = new HorizontalLayout();
        hLay2.add(companyConfigure(), warehouseConfigure());
        return hLay2;
    }

    private HorizontalLayout horizontalLayout3() {
        HorizontalLayout hLay3 = new HorizontalLayout();
        hLay3.add(contractorConfigure(), contractConfigure());
        return hLay3;
    }

    private HorizontalLayout dataConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("Счет покупателя №");
        label.setWidth(LABEL_WIDTH);

        invoiceNumber.setWidth("50px");
        invoiceNumber.setEnabled(true);
        invoiceNumber.setRequired(true);
        invoiceNumber.setRequiredIndicatorVisible(true);
        supplierAccountDtoBinder.forField(invoiceNumber)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getIdValid, SupplierAccountDto::setIdValid);

        Label label2 = new Label("от");
        dateTimePicker.setWidth(FIELD_WIDTH);
        dateTimePicker.setRequiredIndicatorVisible(true);
        supplierAccountDtoBinder.forField(dateTimePicker)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getDateValid, SupplierAccountDto::setDateValid);
        horizontalLayout.add(label, invoiceNumber, label2, dateTimePicker);
        return horizontalLayout;
    }

    private HorizontalLayout contractorConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<ContractorDto> contractorDtos = contractorService.getAll();
        if (contractorDtos != null) {
            contractorDtoComboBox.setItems(contractorDtos);
        }
        contractorDtoComboBox.setItemLabelGenerator(ContractorDto::getName);
        contractorDtoComboBox.setWidth(FIELD_WIDTH);
        contractorDtoComboBox.setRequired(true);
        contractorDtoComboBox.setRequiredIndicatorVisible(true);
        supplierAccountDtoBinder.forField(contractorDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getContractorDtoValid, SupplierAccountDto::setContractorDtoValid);
        Label label = new Label("Контрагент");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, contractorDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout companyConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<CompanyDto> companyDtos = companyService.getAll();
        if (companyDtos != null) {
            companyDtoComboBox.setItems(companyDtos);
        }
        companyDtoComboBox.setItemLabelGenerator(CompanyDto::getName);
        companyDtoComboBox.setWidth(FIELD_WIDTH);
        companyDtoComboBox.setRequired(true);
        companyDtoComboBox.setRequiredIndicatorVisible(true);
        supplierAccountDtoBinder.forField(companyDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getCompanyDtoValid, SupplierAccountDto::setCompanyDtoValid);
        Label label = new Label("Компания");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, companyDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout warehouseConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<WarehouseDto> warehouseDtos = warehouseService.getAll();
        if (warehouseDtos != null) {
            warehouseDtoComboBox.setItems(warehouseDtos);
        }
        warehouseDtoComboBox.setItemLabelGenerator(WarehouseDto::getName);
        warehouseDtoComboBox.setWidth(FIELD_WIDTH);
        warehouseDtoComboBox.setRequired(true);
        warehouseDtoComboBox.setRequiredIndicatorVisible(true);
        supplierAccountDtoBinder.forField(warehouseDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getWarehouseDtoValid, SupplierAccountDto::setWarehouseDtoValid);
        Label label = new Label("Склад");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, warehouseDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout contractConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<ContractDto> contractDtos = contractService.getAll();
        if (contractDtos != null) {
            contractDtoComboBox.setItems(contractDtos);
        }
        contractDtoComboBox.setItemLabelGenerator(ContractDto::getNumber);
        contractDtoComboBox.setWidth(FIELD_WIDTH);
        contractDtoComboBox.setRequired(true);
        contractDtoComboBox.setRequiredIndicatorVisible(true);
        supplierAccountDtoBinder.forField(contractDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getContractDtoValid, SupplierAccountDto::setContractDtoValid);
        Label label = new Label("Договор");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, contractDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout totalPriceLayout() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        H4 label = new H4("Итого:");
        totalPrice.setText(getTotalPrice().toString());
        totalPrice.setHeight("2.0em");
        horizontalLayout.add(label,totalPrice);
        return horizontalLayout;
    }

    private HorizontalLayout dataPlaneConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("План. дата оплаты");
        label.setWidth(LABEL_WIDTH);
        paymentDatePicker.setWidth(FIELD_WIDTH);
        supplierAccountDtoBinder.forField(paymentDatePicker)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(SupplierAccountDto::getPaymentDateValid, SupplierAccountDto::setPaymentDateValid);
        horizontalLayout.add(label, paymentDatePicker, projectConfigure());
        return horizontalLayout;
    }

    private HorizontalLayout commentAndTotalPriceLayout() {
        HorizontalLayout horizontal3 = new HorizontalLayout();
        commentConfig.setPlaceholder("Комментарий");
        commentConfig.setWidth("500px");
        horizontal3.add(commentConfig, totalPriceLayout());
        return horizontal3;
    }

    private void configureCloseViewDialog() {
        dialogOnCloseView.add(new Text("Вы уверены? Несохраненные данные будут потеряны!!!"));
        dialogOnCloseView.setCloseOnEsc(false);
        dialogOnCloseView.setCloseOnOutsideClick(false);
        Button confirmButton = new Button("Продолжить", event -> {
            clearField();
            dialogOnCloseView.close();
        });
        Button cancelButton = new Button("Отменить", event -> dialogOnCloseView.close());
        Shortcuts.addShortcutListener(dialogOnCloseView, dialogOnCloseView::close, Key.ESCAPE);
        dialogOnCloseView.add(new Div(confirmButton, new Div(), cancelButton));
    }

    private boolean isProductInList(ProductDto productDto) {
        return tempSupplierAccountProductsListDtoList.stream()
                .anyMatch(invoiceProductDtoElem -> invoiceProductDtoElem.getProductId().equals(productDto.getId()));
    }

    public BigDecimal getTotalPrice() {
        BigDecimal totalPrice = BigDecimal.valueOf(0.0);
        for (SupplierAccountProductsListDto supplierAccountProductsListDto : tempSupplierAccountProductsListDtoList) {
            totalPrice = totalPrice.add(supplierAccountProductsListDto.getTotal());
        }
        return totalPrice;
    }

    private void setTotalPrice() {
        totalPrice.setText(
                String.format("%.2f", getTotalPrice())
        );
    }

    private void configureDateTimePickerField() {
        dateTimePicker.setValue(LocalDateTime.now());
    }

    private void saveProducts(SupplierAccountDto supplierAccountDto) {
        for (SupplierAccountProductsListDto supplierAccountProductsListDto: tempSupplierAccountProductsListDtoList) {
            supplierAccountProductsListDto.setSupplierAccountId(supplierAccountDto.getId());
            supplierAccountProductsListService.create(supplierAccountProductsListDto);
        }
    }

    private void deleteProduct(Long id) {
        SupplierAccountProductsListDto found = new SupplierAccountProductsListDto();
        for (SupplierAccountProductsListDto supplierAccountProductsListDto : tempSupplierAccountProductsListDtoList) {
            if (Objects.equals(supplierAccountProductsListDto.getId( ), id)) {
                found = supplierAccountProductsListDto;
                break;
            }
        }
        tempSupplierAccountProductsListDtoList.remove(found);
        paginator.setData(tempSupplierAccountProductsListDtoList);
        setTotalPrice();
    }

    private List<SupplierAccountProductsListDto> convertInvoiceProdToSupplier(List<InvoiceProductDto> invoiceProductDto) {
        List<SupplierAccountProductsListDto> supplierAccountProductsListDtos = new ArrayList<>();
        for (InvoiceProductDto iPDto: invoiceProductDto) {
            SupplierAccountProductsListDto supplierAccountProductsListDto = new SupplierAccountProductsListDto();
            supplierAccountProductsListDto.setProductId(iPDto.getProductId());
            supplierAccountProductsListDto.setAmount(iPDto.getAmount());
            supplierAccountProductsListDto.setPrice(iPDto.getPrice());
            supplierAccountProductsListDto.setSum(supplierAccountProductsListDto.getAmount().multiply(supplierAccountProductsListDto.getPrice()).setScale(2, RoundingMode.DOWN));
            supplierAccountProductsListDto.setPercentNds("20");
            supplierAccountProductsListDto.setNds(supplierAccountProductsListDto.getSum().multiply(BigDecimal.valueOf(0.2)).setScale(2, RoundingMode.DOWN));
            supplierAccountProductsListDto.setTotal(supplierAccountProductsListDto.getSum().add(supplierAccountProductsListDto.getNds()));
            supplierAccountProductsListDtos.add(supplierAccountProductsListDto);
        }
        return supplierAccountProductsListDtos;
    }

    private HorizontalLayout projectConfigure() {
        HorizontalLayout projectLayout = new HorizontalLayout();
        List<ProjectDto> projects = projectService.findByArchiveFalse();
        if (projects != null) {
            projectDtoComboBox.setItems(projects);
        }
        projectDtoComboBox.setItemLabelGenerator(ProjectDto::getName);
        projectDtoComboBox.setWidth(FIELD_WIDTH);
        Label label = new Label("Проект");
        label.setWidth(LABEL_WIDTH);
        projectLayout.add(label, projectDtoComboBox);
        return projectLayout;
    }

    private HorizontalLayout configureSalesChannelSelect() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<SalesChannelDto> salesChannels = salesChannelService.getAll();
        if (salesChannels != null) {
            salesChannelSelect.setItems(salesChannels);
        }
        salesChannelSelect.setItemLabelGenerator(SalesChannelDto::getName);
        salesChannelSelect.setWidth(FIELD_WIDTH);
        Label label = new Label("Канал продаж");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, salesChannelSelect, buttonSalesChannel());
        return horizontalLayout;
    }

    private Button buttonSalesChannel() {
        Button salesChannelButton = new Button(new Icon(VaadinIcon.PLUS_CIRCLE));
        salesChannelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        SalesChannelModalWindow salesChannelModalWindow = new SalesChannelModalWindow(new SalesChannelDto(), salesChannelService);
        salesChannelButton.addClickListener(event -> salesChannelModalWindow.open());
        return salesChannelButton;
    }

        private HorizontalLayout incomingConfigure() {
        HorizontalLayout horizontal1 = new HorizontalLayout();

        Label label = new Label("Входящий номер");
        label.setWidth(LABEL_WIDTH);

        text.setWidth("50px");
        Label label2 = new Label("от");
        incomingDatePicker.setWidth("250px");
        horizontal1.add(label, text, label2, incomingDatePicker);
        return horizontal1;
    }

    private void deleteRemovedProducts() {
        for (SupplierAccountProductsListDto removed : supplierAccountProductsListDtoList) {
            if(!tempSupplierAccountProductsListDtoList.contains(removed)) {
                supplierAccountProductsListService.deleteById(removed.getId());
            }
        }
    }

    private void deleteInvoice(SupplierAccountDto supplierAccountDto) {
        List<SupplierAccountProductsListDto> productListDtos = supplierAccountProductsListService.getBySupplierId(supplierAccountDto.getId());
        for (SupplierAccountProductsListDto supplierAccountProductsListDto: productListDtos) {
            supplierAccountProductsListService.deleteById(supplierAccountProductsListDto.getId());
        }
        supplierAccountService.deleteById(supplierAccountDto.getId());
        notifications.infoNotification("Выбранные счета успешно удалены");
    }

    private void terminateCloseDialog(BeforeLeaveEvent beforeLeaveEvent) {
        BeforeLeaveEvent.ContinueNavigationAction action = beforeLeaveEvent.postpone();
        dialogOnCloseView.removeAll();

        Button confirmButton = new Button("Продолжить", event -> {
            dialogOnCloseView.close();
            changeTabSwitchType(false);
            action.proceed();
        });
        Button cancelButton = new Button("Отменить", event -> {
            dialogOnCloseView.close();
        });

        dialogOnCloseView.add(new VerticalLayout(
                new Text("Вы уверены? Несохраненные данные будут утеряны!"),
                new HorizontalLayout(cancelButton, confirmButton))
        );

        dialogOnCloseView.open();
    }

    private void changeTabSwitchType(boolean isProtected) {
        switch (location) {
            case PURCHASES_SUPPLIERS_INVOICES_VIEW:
                if (isProtected) {
                    purchasesSubMenuView.setProtectedTabSwitch();
                } else {
                    purchasesSubMenuView.releaseProtectedTabSwitch();
                }
        }
    }

    private void resetSourceTab(boolean isProtected) {
        switch (location) {
            case PURCHASES_SUPPLIERS_INVOICES_VIEW:
                purchasesSubMenuView.resetTabSelection(location, isProtected);
        }
    }

    public void setProtectedTabSwitch() {
        changeTabSwitchType(true);
    }

    public void setUpdateState(boolean isUpdate) {
        title.setText(isUpdate ? "Редактирование счета" : "Добавление счета");
        deleteButton.setVisible(isUpdate);
        buttonFillFromOrder.setVisible(!isUpdate);
        invoiceSelectField.setVisible(!isUpdate);
    }

    public void setLocation(String location) {
        this.location = location;
    }
    @Override
    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        terminateCloseDialog(beforeLeaveEvent);
    }
}
