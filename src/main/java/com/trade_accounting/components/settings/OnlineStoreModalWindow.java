package com.trade_accounting.components.settings;

import com.trade_accounting.components.util.MenuBarConfigurer;
import com.trade_accounting.models.dto.units.OnlineStoreDto;
import com.trade_accounting.services.interfaces.units.OnlineStoreService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;

public class OnlineStoreModalWindow extends Dialog {

    private final Long id;
    private final ComboBox<String> onlineStores = new ComboBox<>();
    private final TextField nameField = new TextField();
    private final TextField typeField = new TextField();
    private final TextField ordersField = new TextField();
    private final TextField remainsField = new TextField();
    private final OnlineStoreService onlineStoreService;

    public OnlineStoreModalWindow(OnlineStoreDto onlineStoreDto, OnlineStoreService onlineStoreService) {
        this.onlineStoreService = onlineStoreService;

        setCloseOnEsc(true);
        setCloseOnOutsideClick(true);

        id = onlineStoreDto.getId();
        nameField.setValue(getFieldValueNotNull(onlineStoreDto.getName()));
        typeField.setValue((getFieldValueNotNull(onlineStoreDto.getType())));
        ordersField.setValue(getFieldValueNotNull(onlineStoreDto.getOrders()));
        remainsField.setValue(getFieldValueNotNull(onlineStoreDto.getRemains()));

        add(
                header(),
                configureOnlineStoresField(),
                getHorizontalLayout("Тип", typeField),
                getHorizontalLayout("Заказы", ordersField),
                getHorizontalLayout("Остатки", remainsField)
        );
    }

    private HorizontalLayout configureOnlineStoresField() {
        HorizontalLayout onlineStoresField = new HorizontalLayout();
        onlineStores.setItems("1С-Битрикс", "1С-Битрикс малый бизнес", "AdvantShop", "Diafan.CMS", "InSales",
                "Netcat", "Nethouse", "PrestaShop", "Shopify", "Simpla", "UMI.CMS", "UMI.ru", "Webasyst Shop-Script",
                "Обмен по CommerceML", "Sbermegamarket.ru", "Яндекс.Маркет", "Импорт товаров из YML",
                "ВКонтакте новый", "ВКонтакте старый", "Tilda Publishing");
        Label label = new Label("Интернет-магазины");
        label.setWidth("300px");
        onlineStores.setWidth("600px");
        onlineStoresField.add(label, onlineStores);
        return onlineStoresField;
    }

    private Button getSaveButton() {
        return new Button("Сохранить", event -> {
            OnlineStoreDto onlineStoreDto = new OnlineStoreDto();

            onlineStoreDto.setId(id);
            onlineStoreDto.setName(onlineStores.getValue());
            onlineStoreDto.setType(typeField.getValue());
            onlineStoreDto.setOrders(ordersField.getValue());
            onlineStoreDto.setRemains(remainsField.getValue());

            if (onlineStoreDto.getId() == null) {
                onlineStoreService.create(onlineStoreDto);
            } else {
                onlineStoreService.update(onlineStoreDto);
            }
            close();
        });
    }

    private Button getCancelButton() {
        return new Button("Закрыть", event -> close());
    }

    private Button getDeleteButton() {
        return new Button("Удалить", event -> {
            try {
                onlineStoreService.deleteById(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            close();
        });
    }

    private Component header() {
        return MenuBarConfigurer.getMenuBar
                (
                        getSaveButton(),
                        getCancelButton(),
                        getDeleteButton()
                );
    }

    private <T extends Component & HasSize> HorizontalLayout getHorizontalLayout(String labelText, T field) {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label(labelText);
        field.setWidth("600px");
        label.setWidth("300px");
        horizontalLayout.setVerticalComponentAlignment(FlexComponent.Alignment.CENTER, label);
        horizontalLayout.add(label, field);
        return horizontalLayout;
    }

    private String getFieldValueNotNull(String value) {
        return value == null ? "" : value;
    }
}

