package com.trade_accounting.components.settings;

import com.trade_accounting.components.AppView;
import com.trade_accounting.components.goods.RevenueModalWindow;
import com.trade_accounting.components.util.Buttons;
import com.trade_accounting.components.util.GridFilter;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.MenuBarConfigurer;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.components.util.SearchTextField;
import com.trade_accounting.components.util.TitleConfigurer;
import com.trade_accounting.components.util.configure.components.button.ButtonConfigurer;
import com.trade_accounting.components.util.configure.components.select.SelectConfigurer;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.RevenueDto;
import com.trade_accounting.services.interfaces.finance.PaymentService;
import com.trade_accounting.services.interfaces.finance.ReturnToSupplierService;
import com.trade_accounting.services.interfaces.invoice.InvoiceService;
import com.trade_accounting.services.interfaces.util.ProjectService;
import com.trade_accounting.services.interfaces.warehouse.AcceptanceService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static com.trade_accounting.config.SecurityConstants.*;

@Route(value = PROFILE_PROFILE__SETTINGS__PROJECTS_SETTINGS, layout = SettingsView.class)
@PageTitle("Проекты")
@Slf4j
public class ProjectSettingsView extends VerticalLayout {

    private final ProjectService projectService;
    private final InvoiceService invoiceService;
    private final PaymentService paymentService;
    private final AcceptanceService acceptanceService;
    private final ReturnToSupplierService returnToSupplierService;
    private Grid<ProjectDto> grid = new Grid<>(ProjectDto.class);
    private GridPaginator<ProjectDto> paginator;
    private final GridFilter<ProjectDto> filter;
    private final Notifications notifications;

    public ProjectSettingsView(ProjectService projectService,
                               InvoiceService invoiceService,
                               PaymentService paymentService,
                               AcceptanceService acceptanceService,
                               ReturnToSupplierService returnToSupplierService, Notifications notifications) {
        this.projectService = projectService;
        this.invoiceService = invoiceService;
        this.paymentService = paymentService;
        this.acceptanceService = acceptanceService;
        this.returnToSupplierService = returnToSupplierService;
        paginator = new GridPaginator<>(grid, addArchivePrefix(projectService.findByArchiveFalse()), 100);
        this.notifications = notifications;
        setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, paginator);
        configureGrid();
        this.filter = new GridFilter<>(grid);
        updateList();
        configureFilter();
        add(getAppView(), toolsUp(), filter, grid, paginator);
    }

    private AppView getAppView() {
        return new AppView();
    }

    private void configureFilter() {
        Select<Boolean> archive = new Select<>();
        archive.setItems(false, true);
        archive.setItemLabelGenerator(b -> (b) ? "Архивные проекты" : "Действующие проекты");
        archive.setLabel("Архив");
        archive.setValue(false);
        filter.add(archive);
        filter.onSearchClick(e -> {
            paginator.setData(addArchivePrefix(projectService.search(filter.getFilterData(), archive.getValue())));
        });
        filter.onClearClick(e -> {
            paginator.setData(addArchivePrefix(projectService.findByArchiveFalse()));
        });
    }

    private Button buttonQuestion() {
        return Buttons.buttonQuestion(
                "Проекты позволяют оценить успех акции или, например, нового канала продаж. Это атрибут, " +
                        "который присваивается документам и потом становится доступен в отчетах.");
    }

    private H2 title() {
        return TitleConfigurer.titleH2("Проекты");
    }

    private Button buttonRefresh() {
        Button buttonRefresh = ButtonConfigurer.buttonRefresh();
        buttonRefresh.addClickListener(ev -> updateList());
        return buttonRefresh;
    }

    private Button buttonFilter() {
        Button buttonFilter = new Button("Фильтр");
        buttonFilter.addClickListener(e -> filter.setVisible(!filter.isVisible()));
        return buttonFilter;
    }

    private Button buttonProject() {
        Button buttonProject = new Button("Проект", new Icon(VaadinIcon.PLUS_CIRCLE));
        AddEditProjectModal addEditProjectModal =
                new AddEditProjectModal(new ProjectDto(), projectService, invoiceService, paymentService,
                        acceptanceService, returnToSupplierService);
        buttonProject.addClickListener(event -> addEditProjectModal.open());
        addEditProjectModal.addDetachListener(event -> updateList());
        return buttonProject;
    }

    private void updateList() {
        grid.setItems(addArchivePrefix(projectService.findByArchiveFalse()));
        GridSortOrder<ProjectDto> gridSortOrder = new GridSortOrder(grid.getColumnByKey("name"), SortDirection.ASCENDING);
        List<GridSortOrder<ProjectDto>> gridSortOrderList = new ArrayList<>();
        gridSortOrderList.add(gridSortOrder);
        grid.sort(gridSortOrderList);
    }

    private void updateList(String text) {
        grid.setItems(addArchivePrefix(projectService.findBySearch(text)));
    }

    private List<ProjectDto> addArchivePrefix(List<ProjectDto> list) {
        for(ProjectDto project: list) {
            if(project.getArchive()) {
                project.setName("[A] " + project.getName());
            }
        }
        return list;
    }
    private ProjectDto removeArchivePrefix(ProjectDto project) {
        if(project.getName().startsWith("[A] ")) {
            project.setName(project.getName().substring(4));
        }
        return project;
    }

    private void archiveSelectedItems() {
        if (!grid.getSelectedItems().isEmpty()) {
            for (ProjectDto archiveProject : grid.getSelectedItems()) {
                grid.deselect(archiveProject);
                if (archiveProject.getArchive()) { continue; }
                archiveProject.setArchive(true);
                removeArchivePrefix(archiveProject);
                projectService.update(archiveProject);
            }
            updateList();
        }
        else {
            notifications.errorNotification("Сначала отметьте проекты для изменения");
        }
    }
    private void unarchiveSelectedItems() {
        if (!grid.getSelectedItems().isEmpty()) {
            for (ProjectDto unarchiveProject : grid.getSelectedItems()) {
                grid.deselect(unarchiveProject);
                if (!unarchiveProject.getArchive()) { continue; }
                unarchiveProject.setArchive(false);
                removeArchivePrefix(unarchiveProject);
                projectService.update(unarchiveProject);
            }
            updateList();
        }
        else {
            notifications.errorNotification("Сначала отметьте проекты для изменения");
        }
    }


    private Select<String> valueSelect() {
        return SelectConfigurer.configureArchiveSelect(() -> {
            if (!grid.getSelectedItems().isEmpty()) {
                archiveSelectedItems();
            } else {
                notifications.errorNotification("Сначала отметьте проекты для изменения");
            }
        },() -> {
            if (!grid.getSelectedItems().isEmpty()) {
                unarchiveSelectedItems();
            } else {
                notifications.errorNotification("Сначала отметьте проекты для изменения");
            }
        });
    }

    private TextField text() {
        TextField textField = SearchTextField.Search("Наименование", "300px");
        textField.addValueChangeListener(e -> updateList(textField.getValue()));
        return textField;
    }

    private Button settingsButton() {
        return new Button(new Icon(VaadinIcon.COG));
    }

    private Component toolsUp() {
        return MenuBarConfigurer.getMenuBar
                (
                        buttonQuestion(),
                        title(),
                        buttonRefresh(),
                        buttonProject(),
                        buttonFilter(),
                        text(),
                        valueSelect(),
                        settingsButton()
                );
    }

    private void configureGrid() {
        grid.setItems(addArchivePrefix(projectService.findByArchiveFalse()));
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setColumns("name", "code", "description");
        grid.getColumnByKey("name").setHeader("Наименование").setId("Наименование");
        grid.getColumnByKey("code").setHeader("Код").setId("Код");
        grid.getColumnByKey("description").setHeader("Описание").setId("Описание");
        grid.setHeight("64vh");
        grid.addItemDoubleClickListener(event -> {
            ProjectDto dto = event.getItem();
            AddEditProjectModal addEditProjectModal =
                    new AddEditProjectModal(removeArchivePrefix(dto), projectService, invoiceService, paymentService,
                            acceptanceService, returnToSupplierService);
            addEditProjectModal.addDetachListener(e -> updateList());
            addEditProjectModal.open();
        });
    }
}
