package com.trade_accounting.components.settings;

import com.trade_accounting.components.AppView;
import com.trade_accounting.components.util.Buttons;
import com.trade_accounting.components.util.GridFilter;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.components.util.SearchTextField;
import com.trade_accounting.components.util.configure.components.button.ButtonConfigurer;

import com.trade_accounting.components.util.TitleConfigurer;
import com.trade_accounting.components.util.configure.components.input.number.NumberFieldConfigurer;
import com.trade_accounting.components.util.configure.components.select.SelectConfigurer;
import com.trade_accounting.components.util.MenuBarConfigurer;
import com.trade_accounting.models.dto.units.OnlineStoreDto;
import com.trade_accounting.services.interfaces.units.OnlineStoreService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;


import static com.trade_accounting.config.SecurityConstants.PROFILE_PROFILE__SETTINGS__ONLINE_STORES_SETTINGS;

@Route(value = PROFILE_PROFILE__SETTINGS__ONLINE_STORES_SETTINGS, layout = SettingsView.class)
@PageTitle("Интернет-магазины")
@Slf4j
public class OnlineStoreSettingsView extends VerticalLayout {
    private final OnlineStoreService onlineStoreService;
    private final Notifications notifications;
    private final Grid<OnlineStoreDto> grid = new Grid<>(OnlineStoreDto.class);
    private final GridPaginator<OnlineStoreDto> paginator;
    private final NumberField selectedNumberField;
    private final GridFilter<OnlineStoreDto> filter;

    public OnlineStoreSettingsView(OnlineStoreService onlineStoreService, Notifications notifications) {
        this.onlineStoreService = onlineStoreService;
        this.notifications = notifications;
        this.selectedNumberField = getSelectedNumberField();
        this.filter = new GridFilter<>(grid);

        paginator = new GridPaginator<>(grid, onlineStoreService.getAll(), 100);
        setHorizontalComponentAlignment(Alignment.CENTER, paginator);

        configureFilter();
        configureGrid();
        add(
                getAppView(),
                toolsUp(),
                filter,
                grid,
                paginator
        );
    }

    private Component getAppView() {
        return new AppView();
    }

    private void configureGrid() {
        grid.setItems(onlineStoreService.getAll());
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setColumns("id", "name", "type", "orders", "remains");
        grid.getColumnByKey("id").setHeader("№").setId("id");
        grid.getColumnByKey("name").setHeader("Магазин").setId("Магазин");
        grid.getColumnByKey("type").setHeader("Тип").setId("Тип");
        grid.getColumnByKey("orders").setHeader("Заказы").setId("Заказы");
        grid.getColumnByKey("remains").setHeader("Остатки").setId("Остатки");
        grid.setHeight("64vh");

        grid.addItemDoubleClickListener(event -> {
            OnlineStoreDto editStore = event.getItem();
            OnlineStoreModalWindow onlineStoreModalWindow =
                    new OnlineStoreModalWindow(editStore, onlineStoreService);
            onlineStoreModalWindow.addDetachListener(e -> updateList());
            onlineStoreModalWindow.open();
        });
        grid.addSelectionListener(e -> selectedNumberField.setValue((double) e.getAllSelectedItems().size()));
    }

    private void updateList() {
        grid.setItems(onlineStoreService.getAll());
    }

    private H2 titleH2() {
        return TitleConfigurer.titleH2("Синхронизация");
    }

    private Button buttonQuestion() {
        return Buttons.buttonQuestion(
                "К Моему Складу можно подключить интернет-магазин и настроить автоматическую синхронизацию каталога товаров, " +
                        "списка заказов и остатков. Читать инструкцию: Подключение интернет-магазинов");
    }

    private Button buttonRefresh() {
        Button buttonRefresh = ButtonConfigurer.buttonRefresh();
        buttonRefresh.addClickListener(ev -> updateList());
        return buttonRefresh;
    }

    private Button buttonAdd() {
        Button buttonAdd = Buttons.buttonAdd("Магазин");
        OnlineStoreModalWindow addOnlineStoreModalWindow =
                new OnlineStoreModalWindow(new OnlineStoreDto(), onlineStoreService);
        buttonAdd.addClickListener(event -> addOnlineStoreModalWindow.open());
        addOnlineStoreModalWindow.addDetachListener(event -> updateList());
        return buttonAdd;
    }

    private Button buttonFilter() {
        Button buttonFilter = new Button("Фильтр");
        buttonFilter.addClickListener(e -> filter.setVisible(!filter.isVisible()));
        return buttonFilter;
    }

    private void configureFilter() {
        filter.onSearchClick(e -> paginator.setData(onlineStoreService.search(filter.getFilterData())));
        filter.onClearClick(e -> paginator.setData(onlineStoreService.getAll()));
    }

    private void deleteSelectedItems() {
        if (!grid.getSelectedItems().isEmpty()) {
            for (OnlineStoreDto onlineStoreDto : grid.getSelectedItems()) {
                onlineStoreService.deleteById(onlineStoreDto.getId());
            }
            if (grid.getSelectedItems().size() == 1) {
                notifications.infoNotification("Выбранный магазин успешно удален");
            } else if (grid.getSelectedItems().size() > 1) {
                notifications.infoNotification("Выбранные магазины успешно удалены");
            }
        } else {
            notifications.errorNotification("Сначала отметьте магазины для удаления");
        }
    }

    private Select<String> valueSelect() {
        return SelectConfigurer.configureDeleteSelect(() -> {
            if (!grid.getSelectedItems().isEmpty()) {
                deleteSelectedItems();
            } else {
                notifications.errorNotification("Сначала отметьте магазины для удаления");
            }
            grid.deselectAll();
            paginator.setData(onlineStoreService.getAll());
        });
    }

    private NumberField getSelectedNumberField() {
        final NumberField numberField = new NumberField();
        numberField.setWidth("50px");
        numberField.setValue(0D);

        return numberField;
    }

    private NumberField numberField() {
        NumberField numberField = NumberFieldConfigurer.NumberFieldStandart();
        return numberField;
    }

    private void updateList(String text) {
        grid.setItems(onlineStoreService.searchByString(text));
    }

    private TextField text() {
        TextField textField = SearchTextField.Search("Наименование","300px");
        textField.addValueChangeListener(e -> updateList(textField.getValue()));
        return textField;
    }

    private Component toolsUp() {
        return MenuBarConfigurer.getMenuBar
                (
                        buttonQuestion(),
                        titleH2(),
                        buttonRefresh(),
                        buttonAdd(),
                        buttonFilter(),
                        text(),
                        numberField(),
                        valueSelect()
                );
    }

}
