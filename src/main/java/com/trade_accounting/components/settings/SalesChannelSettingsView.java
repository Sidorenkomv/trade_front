package com.trade_accounting.components.settings;


import com.trade_accounting.components.AppView;
import com.trade_accounting.components.profile.SalesChannelModalWindow;
import com.trade_accounting.components.util.Buttons;
import com.trade_accounting.components.util.GridFilter;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.components.util.SearchTextField;
import com.trade_accounting.components.util.configure.components.button.ButtonConfigurer;
import com.trade_accounting.components.util.TitleConfigurer;
import com.trade_accounting.components.util.configure.components.select.SelectConfigurer;
import com.trade_accounting.models.dto.units.SalesChannelDto;
import com.trade_accounting.services.interfaces.units.SalesChannelService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.trade_accounting.config.SecurityConstants.PROFILE_PROFILE__SETTINGS__SALES_CHANNEL_SETTINGS;

@Route(value = PROFILE_PROFILE__SETTINGS__SALES_CHANNEL_SETTINGS, layout = SettingsView.class)
@PageTitle("Учетная запись")
@Slf4j
public class SalesChannelSettingsView extends VerticalLayout {

    private final NumberField selectedNumberField;
    private final List<SalesChannelDto> data;
    SalesChannelService salesChannelService;
    private Grid<SalesChannelDto> grid = new Grid<>(SalesChannelDto.class);
    private GridPaginator<SalesChannelDto> paginator;
    private final GridFilter<SalesChannelDto> filter;

    private final Notifications notifications;

    public SalesChannelSettingsView(SalesChannelService salesChannelService, Notifications notifications) {
        this.selectedNumberField = getSelectedField();
        this.salesChannelService = salesChannelService;
        this.data = salesChannelService.getAll();
        this.notifications = notifications;
        paginator = new GridPaginator<>(grid, salesChannelService.getAll(), 50);
        setHorizontalComponentAlignment(Alignment.CENTER, paginator);
        configureGrid();
        this.filter = new GridFilter<>(grid);
        updateList();
        configureFilter();
        add(getAppView(), horizontalLayout(), filter, grid, paginator);
    }

    private AppView getAppView() {
        return new AppView();
    }

    private H2 title() {
        return TitleConfigurer.titleH2("Каналы продаж");
    }

    private Button buttonQuestion() {
        String anchor = "https://support.moysklad.ru/hc/ru/articles/4403265549585";
        String anchorName = "Каналы продаж";
        return Buttons.buttonQuestionWithAnchor(
                "При подключении интернет-магазинов каналы продаж создаются автоматически. " +
                        "Добавьте дополнительные каналы, например для мессенджеров и соцсетей, и анализируйте продажи в отчете \"Прибыльность\". " +
                        "В Заказах покупателя каналы проставляются несколькими способами: " +
                        "1) Автоматически при выгрузке из интернет-магазина; " +
                        "2) Вручную в каждом документе; " +
                        "3) С помощью массового редактирования; " +
                        "4) В рамках сценария. " +
                        "Далее канал автоматически отмечается в Отгрузках и Возвратах, связанных с Заказом покупателя.", anchorName, anchor);
    }

    private HorizontalLayout horizontalLayout() {
        HorizontalLayout hl = new HorizontalLayout();
        hl.add(buttonQuestion(), title(), buttonRefresh(), buttonSalesChannel(), getButtonFilter(), text(), valueSelect());
        hl.setDefaultVerticalComponentAlignment(Alignment.CENTER);
        return hl;
    }

    private Button buttonRefresh() {
        Button buttonRefresh = ButtonConfigurer.buttonRefresh();
        buttonRefresh.addClickListener(ev -> updateList());
        return buttonRefresh;
    }

    private Button buttonSalesChannel() {
        Button salesChannelButton = Buttons.buttonAdd("Канал продаж");
        SalesChannelModalWindow salesChannelModalWindow = new SalesChannelModalWindow(new SalesChannelDto(), salesChannelService);
        salesChannelButton.addClickListener(event -> salesChannelModalWindow.open());
        salesChannelButton.addDetachListener(event -> updateList());
        return salesChannelButton;
    }

    private Button getButtonFilter() {
        Button filterButton = new Button("Фильтр");
        filterButton.addClickListener(e -> filter.setVisible(!filter.isVisible()));
        return filterButton;
    }

    private TextField text() {
        TextField textField = SearchTextField.Search("Наименование","300px");
        return textField;
    }

    private void updateList() {
        grid.setItems(salesChannelService.getAll());
    }

    private List<SalesChannelDto> getData() {
        return salesChannelService.getAll();
    }


    public void configureGrid() {
        grid.setItems(salesChannelService.getAll());
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setColumns("name", "type", "description", "generalAccess", "departmentOwner", "employeeOwner", "dateOfChange", "employeeChange");
        grid.getColumnByKey("name").setHeader("Наименование").setId("Наименование");
        grid.getColumnByKey("type").setHeader("Тип").setId("Тип");
        grid.getColumnByKey("description").setHeader("Описание").setId("Описание");
        grid.getColumnByKey("generalAccess").setHeader("Общий доступ").setId("Общий доступ");
        grid.getColumnByKey("departmentOwner").setHeader("Владелец-отдел").setId("Владелец-отдел");
        grid.getColumnByKey("employeeOwner").setHeader("Владелец-сотрудник").setId("Владелец-сотрудник");
        grid.getColumnByKey("dateOfChange").setHeader("Когда изменил").setId("Когда изменил");
        grid.getColumnByKey("employeeChange").setHeader("Кто изменил").setId("Кто изменил");
        grid.setHeight("64vh");

        grid.addItemDoubleClickListener(event -> {
            SalesChannelDto editSalesChannel = event.getItem();
            SalesChannelModalWindow salesChannelModalWindow =
                    new SalesChannelModalWindow(editSalesChannel, salesChannelService);
            salesChannelModalWindow.addDetachListener(e -> updateList());
            salesChannelModalWindow.open();
        });
        grid.addSelectionListener(e -> selectedNumberField.setValue((double) e.getAllSelectedItems().size()));
    }


    private NumberField getSelectedField() {
        final NumberField numberField = new NumberField();
        numberField.setWidth("50px");
        numberField.setValue(0D);
        return numberField;
    }

    private void configureFilter() {
        filter.onSearchClick(e -> paginator.setData(salesChannelService.search(filter.getFilterData())));
        filter.onClearClick(e -> paginator.setData(salesChannelService.getAll()));
    }

    private Select<String> valueSelect() {
        return SelectConfigurer.configureDeleteSelect(() -> {
            deleteSelectedSalesChannel();
            grid.deselectAll();
            paginator.setData(getData());
        });
    }


    private void deleteSelectedSalesChannel() {
        if (!grid.getSelectedItems().isEmpty()) {
            for (SalesChannelDto salesChannelDto : grid.getSelectedItems()) {
                salesChannelService.deleteById(salesChannelDto.getId());
            }
            if (grid.getSelectedItems().size() == 1) {
                notifications.infoNotification("Выбранный канал продаж успешно удален");
            } else if (grid.getSelectedItems().size() > 1) {
                notifications.infoNotification("Выбранные каналы продаж успешно удалены");
            }
        } else {
            notifications.errorNotification("Сначала отметьте галочками нужные каналы продаж");
        }
    }
}
