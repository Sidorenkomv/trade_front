package com.trade_accounting.components.indicators;

import com.trade_accounting.components.util.LineChart;
import com.trade_accounting.components.util.configure.components.button.ButtonConfigurer;
import com.trade_accounting.models.dto.client.EmployeeDto;
import com.trade_accounting.models.dto.invoice.InvoiceDto;
import com.trade_accounting.models.dto.invoice.InvoiceProductDto;
import com.trade_accounting.models.dto.util.OperationsDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.invoice.InvoiceProductService;
import com.trade_accounting.services.interfaces.invoice.InvoiceService;
import com.trade_accounting.services.interfaces.util.OperationsService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@SpringComponent
//Если на страницу не ссылаются по URL или она не является отдельной страницей, а подгружается родительским классом, то URL и Title не нужен
/*@Route(value = INDICATORS_DASHBOARD_VIEW, layout = AppView.class)
@PageTitle("Показатели")*/
@UIScope
public class DashboardView extends VerticalLayout {

    private final InvoiceService invoiceService;
    private final InvoiceProductService invoiceProductService;
    private final OperationsService operationsService;
    private final CompanyService companyService;

    private final EmployeeService employeeService;

    private final OperationsView operationsView;
    private List<String> category;
    private final VerticalLayout content = new VerticalLayout();
    private Integer[] data;
    private List<InvoiceDto> invoiceDtoList;
    private List<InvoiceProductDto> invoiceProductDtoList;

    private final GridVariant[] GRID_STYLE = {GridVariant.LUMO_ROW_STRIPES,
            GridVariant.LUMO_WRAP_CELL_CONTENT, GridVariant.LUMO_COLUMN_BORDERS};

    private final Grid<OperationsDto> gridRecentDocuments = new Grid<>(OperationsDto.class, false);


    @Autowired
    public DashboardView(InvoiceService invoiceService,
                         InvoiceProductService invoiceProductService,
                         OperationsService operationsService,
                         CompanyService companyService,
                         OperationsView operationsView,
                         EmployeeService employeeService) {
        this.invoiceProductService = invoiceProductService;
        this.invoiceService = invoiceService;
        this.operationsService = operationsService;
        this.companyService = companyService;
        this.operationsView = operationsView;
        this.employeeService = employeeService;
        this.configureGrid();
        HorizontalLayout refreshDoc = new HorizontalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(getUpperLeftLayout());
        horizontalLayout.setPadding(false);
        add(horizontalLayout);
        add(refreshDoc);
        add(gridRecentDocuments);
        refreshDoc.add(new Label("Недавние документы"), this.buttonRefresh());
        refreshDoc.setAlignItems(Alignment.START);
        setPadding(false);
        setSpacing(false);
        updateList();
    }

    private void updateList() {
        gridRecentDocuments.setItems(getData());
    }

    private void configureGrid() {
        gridRecentDocuments.addThemeVariants(GRID_STYLE);
        gridRecentDocuments.addColumn(new ComponentRenderer<>(item -> {
            Anchor anchor = new Anchor();
            Paragraph p = new Paragraph();
            anchor.setText(this.getIdAndTypeDocColumn(item));
            anchor.setHref("http://localhost:4445/app/indicators");
            p.add(anchor);
            p.addClickListener(e -> operationsView.openModalWindow(item));
            return p;
        })).setHeader("ID").setKey("ID");
        gridRecentDocuments.addColumn(new ComponentRenderer<>(this::isApprovedDocumentIcon)).setHeader("Пров.").setId("Проверен");
        gridRecentDocuments.addColumn(this::getFormatDate).setHeader("Момент").setId("Момент");
        gridRecentDocuments.addColumn(operationsView::getContractor).setHeader("Контрагент").setId("Контрагент");
        gridRecentDocuments.addColumn(operationsDto->companyService.getById(operationsDto.getCompanyId()).getName())
                .setHeader("Организация").setKey("company").setId("Организация");
        gridRecentDocuments.addColumn(operationsView::getTotalPrice).setHeader("Сумма").setId("Сумма");
        gridRecentDocuments.addColumn(this::getCurrency).setHeader("Валюта").setId("Валюта");
        gridRecentDocuments.addColumn(this::getChangedTime).setHeader("Когда изменен").setId("Когда изменен");
        gridRecentDocuments.addColumn(this::whomChanged).setHeader("Кем изменен").setId("Кем изменен");

        gridRecentDocuments.getColumns().forEach(column -> column.setAutoWidth(true));
        gridRecentDocuments.setAllRowsVisible(true);
        gridRecentDocuments.setWidth("100%");
    }

    private String getFormatDate(OperationsDto operationsDto) {
        LocalDateTime date = LocalDateTime.parse(operationsDto.getDate());
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    }

    private String getIdAndTypeDocColumn(OperationsDto operationsDto) {
        return String.format("%06d %s", operationsDto.getId(), operationsView.getType(operationsDto));
    }

    private String getCurrency(OperationsDto operationsDto) {
        // Пока в программе нет валют, просто возвращаем рубли
        return "руб.";
    }

    private static String getMinutesAndHoursDeclination(long num, String measure) {
        int lastDigit = (int) (num % 10);
        Map<String, String[]> declinations = new HashMap<>();
        declinations.put("минуты", new String[]{"минута", "минут", "минуты"});
        declinations.put("часы", new String[]{"час", "часов", "часа"});

        if (num >= 5 && num <= 20) {
            return declinations.get(measure)[1];
        }

        if (lastDigit == 1) {
            return declinations.get(measure)[0];
        } else if (lastDigit > 1 && lastDigit < 5) {
            return declinations.get(measure)[2];
        }
        return declinations.get(measure)[1];
    }

    private long getDifferenceDateInMinutes(OperationsDto operationsDto) {
        return ChronoUnit.MINUTES.between(LocalDateTime.parse(operationsDto.getLastModifiedDate()), LocalDateTime.now());
    }

    private String getChangedTime(OperationsDto operationsDto) {
        long countMinutes = getDifferenceDateInMinutes(operationsDto);
        return (countMinutes < 60) ?
                String.format("%d %s назад", countMinutes, getMinutesAndHoursDeclination(countMinutes, "минуты")) :
                String.format("%d %s назад", countMinutes / 60, getMinutesAndHoursDeclination(countMinutes / 60, "часы"));
    }


    private String whomChanged(OperationsDto operationsDto) {
        EmployeeDto employeeDto = employeeService.getById(operationsDto.getEmployeeId());
        return employeeDto.getLastName() + " " + employeeDto.getFirstName();
    }

    private Component isApprovedDocumentIcon(OperationsDto operationsDto) {
        if (operationsDto.getIsPrint()) {
            Icon icon = new Icon(VaadinIcon.CHECK);
            icon.setColor("green");
            return icon;
        } else {
            return new Span("");
        }
    }

//    private Component getChart() {
//        HorizontalLayout chartLayout = new HorizontalLayout();
//        LineChart lineChart = new LineChart(category, data, "Сумма продаж");
//        lineChart.setHeight("100%");
//        lineChart.setWidth("100%");
//        chartLayout.setAlignItems(Alignment.CENTER);
//        chartLayout.removeAll();
//        chartLayout.add(lineChart);
//        return lineChart;
//    }

    private List<OperationsDto> getData() {
        return operationsService.getAll().stream()
                .filter(operationsDto -> !operationsDto.getIsRecyclebin() &&
                        getDifferenceDateInMinutes(operationsDto) < 24 * 60) // выводим документы за 24 часа
                .sorted(Comparator.comparingLong(this::getDifferenceDateInMinutes))
                .limit(10) // ограничиваем вывод 10-ю документами
                .collect(Collectors.toList());
    }

    private Button buttonRefresh() {
        Button buttonRefresh = ButtonConfigurer.buttonRefresh();
        buttonRefresh.addClickListener(ev -> updateList());
        return buttonRefresh;
    }

    private Component getUpperLeftLayout() {
        VerticalLayout layout = new VerticalLayout();
        Tab week = new Tab("Неделя");
        Tab month = new Tab("Месяц");
        Tab year = new Tab("Год");
        Tabs tabs = new Tabs(week, month, year);
        tabs.addSelectedChangeListener(event -> setContent(event.getSelectedTab().getLabel()));
        tabs.setSelectedTab(week);
        setContent("Неделя");
        layout.setPadding(false);
        layout.setAlignItems(Alignment.CENTER);
        content.setAlignItems(Alignment.CENTER);
        layout.add(tabs, addDateToTab(), content);
        return layout;
    }


    private void setContent(String tabName) {
        switch (tabName) {
            case "Месяц":
                content.removeAll();
                content.add(addTodaySells());
                content.add(addMonthSells());
                category = IntStream.range(1, YearMonth.now().lengthOfMonth() + 1).mapToObj(String::valueOf)
                        .collect(Collectors.toList());
                addMonthData();
                break;
            case "Год":
                content.removeAll();
                content.add(addTodaySells());
                content.add(addYearSells());
                category = Arrays.asList("Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сент", "Окт",
                        "Нояб", "Дек");
                addYearData();
                break;
            default:
                content.removeAll();
                content.add(addTodaySells());
                content.add(addWeekSells());
                category = Arrays.asList("Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс");
                addWeekData();
                break;
        }
    }


    private void addWeekData() {
        List<Integer> integerList = new ArrayList<>();
        LocalDateTime dateFrom = LocalDateTime.now().minusDays(LocalDateTime.now().getDayOfWeek().getValue())
                .with(LocalTime.MIN);
        for (int i = 1; i < LocalDateTime.now().getDayOfWeek().getValue() + 1; i++) {
            integerList.add(getSells(dateFrom, dateFrom.plusDays(1).with(LocalTime.MAX))[1]);
            dateFrom = dateFrom.plusDays(1);
        }
        data = integerList.toArray(new Integer[0]);
    }

    private void addMonthData() {
        List<Integer> integerList = new ArrayList<>();
        LocalDateTime dateFrom = LocalDateTime.now().minusDays(LocalDateTime.now().getDayOfMonth())
                .with(LocalTime.MIN);
        for (int i = 1; i < LocalDateTime.now().getDayOfMonth() + 1; i++) {
            integerList.add(getSells(dateFrom, dateFrom.plusDays(1).with(LocalTime.MAX))[1]);
            dateFrom = dateFrom.plusDays(1);
        }
        data = integerList.toArray(new Integer[0]);
    }


    private void addYearData() {
        List<Integer> integerList = new ArrayList<>();
        LocalDateTime dateFrom = LocalDateTime.now().minusDays(LocalDateTime.now().getDayOfYear())
                .with(LocalTime.MIN);
        for (int i = LocalDate.now().getMonthValue(); i > 0; i--) {
            integerList.add(getSells(dateFrom, dateFrom.plusMonths(1).with(LocalTime.MAX))[1]);
            dateFrom = dateFrom.plusMonths(1);
        }
        data = integerList.toArray(new Integer[0]);
    }


    private HorizontalLayout addYearSells() {
        HorizontalLayout layout = new HorizontalLayout();
        long daysInYear = Year.of(LocalDate
                .now().minusYears(1).getYear()).length();
        long yearDay = LocalDate.now().getDayOfYear();
        int[] yearData = getSells(LocalDateTime.of(LocalDate.now().minusDays(yearDay - 1), LocalTime.MIN), null);
        int[] lastYearData = getSells(LocalDateTime.of(LocalDate.now().minusDays(yearDay + daysInYear - 1), LocalTime.MIN),
                LocalDateTime.of(LocalDate.now().minusDays(yearDay), LocalTime.MIN));
        layout.add(new VerticalLayout(new H4(String.valueOf(yearData[0])), new H6("Продаж")));
        layout.add(new VerticalLayout(new H4(String.valueOf(yearData[1])), new H6("Руб")));
        layout.add(new VerticalLayout(new H4(lastYearData[0] + " " + "Руб" + " " + "(" + (percentage(lastYearData[1], yearData[1])) + "%)"),
                new H6("По сравнению с прошлым годом")));
        layout.setWidth("50%");
        layout.setHeight("50%");
        return layout;
    }


    private HorizontalLayout addWeekSells() {
        HorizontalLayout layout = new HorizontalLayout();
        long weekDay = LocalDate.now().getDayOfWeek().getValue();
        int[] weeksData = getSells(LocalDateTime.of(LocalDate.now().minusDays(weekDay - 1), LocalTime.MIN), null);
        int[] lastWeeksData = getSells(LocalDateTime.of(LocalDate.now().minusDays(weekDay + 6), LocalTime.MIN),
                LocalDateTime.of(LocalDate.now().minusDays(weekDay - 1), LocalTime.MIN));
        layout.add(new VerticalLayout(new H4(String.valueOf(weeksData[0])), new H6("Продаж")));
        layout.add(new VerticalLayout(new H4(String.valueOf(weeksData[1])), new H6("Руб")));
        layout.add(new VerticalLayout(new H4(lastWeeksData[0] + " " + "Руб" + " " + "(" +
                (percentage(lastWeeksData[1], weeksData[1])) + "%)"), new H6("По сравнению с прошлой неделей")));
        layout.setWidth("50%");
        layout.setHeight("50%");
        return layout;
    }


    private HorizontalLayout addMonthSells() {
        HorizontalLayout layout = new HorizontalLayout();
        LocalDate date = LocalDate.now();
        long monthDay = LocalDate.now().getDayOfMonth();
        int[] monthData = getSells(LocalDateTime.of(date.minusDays(monthDay - 1), LocalTime.MIN), null);
        int[] lastMonthData = getSells(LocalDateTime.of(date.minusDays(monthDay +
                        YearMonth.of(date.getYear(), date.getMonthValue()).lengthOfMonth() - 1), LocalTime.MIN),
                LocalDateTime.of(LocalDate.now().minusDays(monthDay - 1), LocalTime.MIN));
        layout.add(new VerticalLayout(new H4(String.valueOf(monthData[0])), new H6("Продаж")));
        layout.add(new VerticalLayout(new H4(String.valueOf(monthData[1])), new H6("Руб")));
        layout.add(new VerticalLayout(new H4(lastMonthData[0] + " " + "Руб" + " " + "(" +
                (percentage(lastMonthData[1], monthData[1])) + "%)"), new H6("По сравнению с прошлым месяцем")));
        layout.setWidth("50%");
        layout.setHeight("50%");
        return layout;
    }


    private HorizontalLayout addTodaySells() {
        HorizontalLayout layout = new HorizontalLayout();
        int[] todayData = getSells(LocalDateTime.of(LocalDate.now(), LocalTime.MIN), null);
        int[] yesterdaysData = getSells(LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.MIN), LocalDateTime
                .now().with(LocalTime.MIN));
        VerticalLayout v1 = new VerticalLayout(new H4(String.valueOf(todayData[0])), new H6("Продаж"));
        layout.add(v1);
        VerticalLayout v2 = new VerticalLayout(new H4(String.valueOf(todayData[1])), new H6("Руб"));
        layout.add(v2);
        VerticalLayout v3 = new VerticalLayout(new H4(yesterdaysData[1] + " " + "Руб" + " " + "(" +
                (percentage(yesterdaysData[1], todayData[1])) + "%)"), new H6("По сравнению со вчера"));
        layout.add(v3);
        layout.setSpacing(false);
        layout.setWidth("50%");
        layout.setHeight("50%");
        return layout;
    }


    private void setInvoiceDtoAndProductList(LocalDateTime from, LocalDateTime to) {
        invoiceDtoList = invoiceService.searchFromDate(from);
        if(to != null) {
            invoiceDtoList = invoiceDtoList.stream().filter(invoiceDto -> LocalDateTime.parse(invoiceDto.getDate())
                            .isBefore(to))
                    .collect(Collectors.toList());
        }
        invoiceProductDtoList = new ArrayList<>();
        for (InvoiceDto i:
                invoiceDtoList) {
            invoiceProductDtoList.addAll(invoiceProductService.getByInvoiceId(i.getId()));
        }
    }


    private int[] getSells(LocalDateTime dateFrom, LocalDateTime to) {
        setInvoiceDtoAndProductList(dateFrom, to);
        int numberOfSales = invoiceDtoList.size();
        int revenue = invoiceProductDtoList.stream().mapToInt(s -> s.getPrice().intValue() * s.getAmount().intValue())
                .sum();

        return new int[]{numberOfSales, revenue};
    }

    private HorizontalLayout addDateToTab() {
        LocalDate date = LocalDate.now();
        int dayOfWeek = DayOfWeek.from(date).getValue();
        String[] daysOfWeek = {"понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"};
        String[] months = {"января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября",
                "октября", "ноября", "декабря"};
        HorizontalLayout hl = new HorizontalLayout(new H6("Сегодня " + daysOfWeek[dayOfWeek - 1] + " " + date.getDayOfMonth() +
                " " + months[date.getMonthValue() - 1]));
        hl.setPadding(false);
        return hl;
    }

    private long percentage(int first, int second) {
        if(first == 0 && second == 0) {
            return 0;
        }
       return first == 0 ? 100 : Math.round(((double) second/first) * 100);
    }
}
