package com.trade_accounting.components.production;

import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.production.TechnicalCardDto;
import com.trade_accounting.models.dto.production.TechnicalOperationsDto;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.production.TechnicalCardService;
import com.trade_accounting.services.interfaces.production.TechnicalOperationsService;
import com.trade_accounting.services.interfaces.util.ProjectService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.trade_accounting.config.SecurityConstants.PRODUCTION_TECHNOLOGICAL_VIEW;

@UIScope
@SpringComponent
@Slf4j
public class TechnologicalOperationsModalView extends Dialog {

    private final TechnicalCardService technicalCardService;
    private final TechnicalOperationsService technicalOperationsService;
    private final WarehouseService warehouseService;
    private final CompanyService companyService;
    private final ProjectService projectService;
    private final EmployeeService employeeService;
    private TechnicalOperationsDto dto;
    private final Notifications notifications;

    private final String LABEL_WIDTH = "150px";

    private final String COMBO_BOX_WIDTH = "350px";

    private final ComboBox<TechnicalCardDto> technicalCardComboBox = new ComboBox<>();
    private final ComboBox<WarehouseDto> materialWarehouseComboBox = new ComboBox<>();
    private final ComboBox<WarehouseDto> productionWarehouseComboBox = new ComboBox<>();
    private final ComboBox<CompanyDto> companyDtoComboBox = new ComboBox<>();

    private final ComboBox<ProjectDto> projectDtoComboBox = new ComboBox<>();
    private final DateTimePicker dateTimePicker = new DateTimePicker();
    private final Checkbox checkboxIsSpend = new Checkbox("Проведено");
    private final TextField returnNumber = new TextField();
    private final TextField returnVolume = new TextField();
    private final TextArea commentTextArea = new TextArea();

    private final Binder<TechnicalOperationsDto> technicalOperationsBinder =
            new Binder<>(TechnicalOperationsDto.class);
    private final String TEXT_FOR_REQUEST_FIELD = "Обязательное поле";

    public TechnologicalOperationsModalView(TechnicalCardService technicalCardService,
                                            TechnicalOperationsService technicalOperationsService,
                                            WarehouseService warehouseService,
                                            CompanyService companyService,
                                            ProjectService projectService,
                                            EmployeeService employeeService,
                                            Notifications notifications) {
        this.technicalCardService = technicalCardService;
        this.technicalOperationsService = technicalOperationsService;
        this.employeeService = employeeService;
        this.warehouseService = warehouseService;
        this.companyService = companyService;
        this.projectService = projectService;
        this.notifications = notifications;
        setSizeFull();
        add(headerLayout(), formLayout());
    }

    public void setTechnicalOperationsEdit(TechnicalOperationsDto editDto) {
        this.dto = editDto;
        this.dto.setId(editDto.getId());
        returnNumber.setValue(editDto.getId().toString());
        dateTimePicker.setValue(LocalDateTime.parse(editDto.getDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        returnVolume.setValue(editDto.getVolume().toString());
        commentTextArea.setValue(editDto.getComment());
        materialWarehouseComboBox.setValue(warehouseService.getById(editDto.getMaterialWarehouseId()));
        productionWarehouseComboBox.setValue(warehouseService.getById(editDto.getProductionWarehouseId()));
        technicalCardComboBox.setValue(technicalCardService.getById(editDto.getTechnicalCardId()));
        projectDtoComboBox.setValue(projectService.getById(editDto.getProjectId()));
        companyDtoComboBox.setValue(companyService.getById(editDto.getCompanyId()));
    }

    private HorizontalLayout headerLayout() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(title(), saveButton(), closeButton());
        return horizontalLayout;
    }

    private VerticalLayout formLayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(formLayout1(), formLayout2(), formLayout3(), formLayout4(), formLayout5());
        return verticalLayout;
    }

    private H2 title() {
        return new H2("Добавление технологической операции");
    }

    private Button saveButton() {
        return new Button("Сохранить", e -> {
            if (!technicalOperationsBinder.validate().isOk()) {
                technicalOperationsBinder.validate().notifyBindingValidationStatusHandlers();
            } else {
                TechnicalOperationsDto newDto = new TechnicalOperationsDto();
                if (this.dto != null) {
                    newDto.setId(dto.getId());
                    newDto.setEmployeeId(dto.getId());
                    newDto.setIsRecycleBin(dto.getIsRecycleBin());
                } else {
                    newDto.setEmployeeId(employeeService.getPrincipal().getId());
                    newDto.setIsRecycleBin(false);
                }
                newDto.setTechnicalCardId(technicalCardComboBox.getValue().getId());
                newDto.setProductionWarehouseId(productionWarehouseComboBox.getValue().getId());
                newDto.setMaterialWarehouseId(materialWarehouseComboBox.getValue().getId());
                newDto.setProjectId(projectDtoComboBox.getValue().getId());
                newDto.setDate(dateTimePicker.getValue().toString());
                newDto.setLastModifiedDate(LocalDateTime.now().toString());
                newDto.setComment(commentTextArea.getValue());
                newDto.setVolume(Integer.parseInt(returnVolume.getValue()));
                newDto.setCompanyId(companyDtoComboBox.getValue().getId());
                newDto.setIsChecked(checkboxIsSpend.getValue());
                newDto.setIsPrint(false);
                newDto.setIsSent(false);
                newDto.setIsRecycleBin(false);
                newDto.setIsShared(true);
                newDto.setNumber(Integer.parseInt(returnNumber.getValue()));
                technicalOperationsService.create(newDto);

                UI.getCurrent().navigate(PRODUCTION_TECHNOLOGICAL_VIEW);
                close();
                clearAllFieldsModalView();
                notifications.infoNotification(String.format("технологическая операция c ID=%s сохранена", newDto.getNumber()));
            }
        });
    }

    private Button closeButton() {
        Button button = new Button("Закрыть", new Icon(VaadinIcon.CLOSE));
        button.addClickListener(e -> {
            close();
            clearAllFieldsModalView();
        });
        return button;
    }

    private HorizontalLayout formLayout1() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(numberConfigure(), dateConfigure(), checkboxLayout());
        return horizontalLayout;
    }

    private HorizontalLayout formLayout2() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(technicalCardConfigure(), warehouseConfigure(materialWarehouseComboBox, "Склад для материалов"));
        return horizontalLayout;
    }

    private HorizontalLayout formLayout3() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(projectConfigure(), warehouseConfigure(productionWarehouseComboBox, "Склад для продукции"));
        return horizontalLayout;
    }

    private HorizontalLayout formLayout4() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(companyConfigure());
        return horizontalLayout;
    }

    private HorizontalLayout formLayout5() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(volumeConfig(), commentConfig());
        return horizontalLayout;
    }

    private HorizontalLayout companyConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<CompanyDto> list = companyService.getAll();
        if (list != null) {
            companyDtoComboBox.setItems(list);
        }
        companyDtoComboBox.setItemLabelGenerator(CompanyDto::getName);
        companyDtoComboBox.setWidth(COMBO_BOX_WIDTH);
        Label label = new Label("Организация");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, companyDtoComboBox);
        technicalOperationsBinder.forField(companyDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD);
        return horizontalLayout;
    }

    private HorizontalLayout projectConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<ProjectDto> list = projectService.getAll();
        if (list != null) {
            projectDtoComboBox.setItems(list);
        }
        projectDtoComboBox.setItemLabelGenerator(ProjectDto::getName);
        projectDtoComboBox.setWidth(COMBO_BOX_WIDTH);
        Label label = new Label("Проект");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, projectDtoComboBox);
        technicalOperationsBinder.forField(projectDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD);
        return horizontalLayout;
    }

    private HorizontalLayout numberConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("Технологическая операция №");
        label.setWidth(LABEL_WIDTH);
        returnNumber.setWidth("50px");
        horizontalLayout.add(label, returnNumber);
        technicalOperationsBinder.forField(returnNumber)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(TechnicalOperationsDto::getIdValid, TechnicalOperationsDto::setIdValid);
        return horizontalLayout;
    }

    private HorizontalLayout dateConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("От");
        dateTimePicker.setWidth(COMBO_BOX_WIDTH);
        horizontalLayout.add(label, dateTimePicker);
        technicalOperationsBinder.forField(dateTimePicker)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(TechnicalOperationsDto::getDateValid, TechnicalOperationsDto::setDateValid);
        return horizontalLayout;
    }

    private VerticalLayout checkboxLayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(checkboxIsSpend);
        return verticalLayout;
    }

    private HorizontalLayout technicalCardConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<TechnicalCardDto> list = technicalCardService.getAll();
        if (list != null) {
            technicalCardComboBox.setItems(list);
        }
        technicalCardComboBox.setItemLabelGenerator(TechnicalCardDto::getName);
        technicalCardComboBox.setWidth(COMBO_BOX_WIDTH);
        Label label = new Label("Технологическая карта");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, technicalCardComboBox);
        technicalOperationsBinder.forField(technicalCardComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(TechnicalOperationsDto::getTechnicalCardDtoValid, TechnicalOperationsDto::setTechnicalCardDtoValid);
        return horizontalLayout;
    }

    private HorizontalLayout warehouseConfigure(ComboBox<WarehouseDto> warehouse, String warehouseType) {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<WarehouseDto> list = warehouseService.getAll();
        if (list != null) {
            warehouse.setItems(list);
        }
        warehouse.setItemLabelGenerator(WarehouseDto::getName);
        warehouse.setWidth(COMBO_BOX_WIDTH);
        Label label = new Label(warehouseType);
        label.setWidth("100px");
        horizontalLayout.add(label, warehouse);
        technicalOperationsBinder.forField(warehouse)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(TechnicalOperationsDto::getWarehouseDtoValid, TechnicalOperationsDto::setWarehouseDtoValid);
        return horizontalLayout;
    }

    private HorizontalLayout volumeConfig() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("Объем производства");
        label.setWidth("100px");
        horizontalLayout.setWidth("550px");
        horizontalLayout.setHeight("50px");
        returnVolume.setWidth("50px");
        horizontalLayout.add(label, returnVolume);
        technicalOperationsBinder.forField(returnVolume);
              //  .asRequired(TEXT_FOR_REQUEST_FIELD)
              //  .bind(TechnicalOperationsDto::getVolumeValid, TechnicalOperationsDto::setVolumeValid);
        return horizontalLayout;
    }

    private HorizontalLayout commentConfig() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("Комментарий");
        label.setWidth("100px");
        horizontalLayout.setWidth("750px");
        horizontalLayout.setHeight("100px");
        horizontalLayout.add(label, commentTextArea);
        technicalOperationsBinder.forField(commentTextArea);
            //    .asRequired(TEXT_FOR_REQUEST_FIELD)
                //.bind(TechnicalOperationsDto::getCommentValid, TechnicalOperationsDto::setCommentValid);
        return horizontalLayout;
    }

    private void clearAllFieldsModalView() {
        technicalCardComboBox.setValue(null);
        materialWarehouseComboBox.setValue(null);
        productionWarehouseComboBox.setValue(null);
        companyDtoComboBox.setValue(null);
        projectDtoComboBox.setValue(null);
        returnVolume.setValue("");
        dateTimePicker.setValue(null);
        commentTextArea.setValue("");
        returnNumber.setValue("");
        checkboxIsSpend.setValue(false);
    }
}
