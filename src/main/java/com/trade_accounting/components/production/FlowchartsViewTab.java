package com.trade_accounting.components.production;


import com.trade_accounting.components.util.Buttons;
import com.trade_accounting.components.util.GridConfigurer;
import com.trade_accounting.components.util.GridFilter;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.components.util.SearchTextField;
import com.trade_accounting.components.util.configure.components.button.ButtonConfigurer;
import com.trade_accounting.components.util.configure.components.select.SelectConfigurer;
import com.trade_accounting.models.dto.production.TechnicalCardDto;
import com.trade_accounting.models.dto.production.TechnicalCardGroupDto;
import com.trade_accounting.models.dto.warehouse.ProductDto;
import com.trade_accounting.services.interfaces.production.TechnicalCardGroupService;
import com.trade_accounting.services.interfaces.production.TechnicalCardProductionService;
import com.trade_accounting.services.interfaces.production.TechnicalCardService;
import com.trade_accounting.services.interfaces.util.ColumnsMaskService;
import com.trade_accounting.services.interfaces.warehouse.ProductService;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static com.trade_accounting.config.SecurityConstants.GRID_PRODUCTION_MAIN_FLOWCHART;

@SpringComponent
@UIScope
//Если на страницу не ссылаются по URL или она не является отдельной страницей, а подгружается родительским классом, то URL и Title не нужен
/*@PageTitle("Тех. карты")
@Route(value = PRODUCTION_FLOWCHARTS_VIEW, layout = AppView.class)*/
public class FlowchartsViewTab extends VerticalLayout {

    private final TextField text = new TextField();
    private final Grid<TechnicalCardDto> grid = new Grid<>(TechnicalCardDto.class, false);
    private final GridConfigurer<TechnicalCardDto> gridConfigurer;
    private final List<TechnicalCardDto> data;
    private final GridPaginator<TechnicalCardDto> paginator;
    private final TechnicalCardService technicalCardService;
    private final GridFilter<TechnicalCardDto> filter;
    private final TechnicalCardGroupDto technicalCardGroupDto;
    private final TechnicalCardGroupService technicalCardGroupService;
    private final TechnicalCardDto technicalCardDto;
    private final ProductService productService;
    private final List<ProductDto> productDtoList;
    private final TechnicalCardProductionService technicalCardProductionService;
    private final Notifications notifications;
    private final GridVariant[] GRID_STYLE = {GridVariant.LUMO_ROW_STRIPES,
            GridVariant.LUMO_WRAP_CELL_CONTENT, GridVariant.LUMO_COLUMN_BORDERS};
    private Optional<TechnicalCardGroupDto> optional = Optional.empty();
    private final TreeGrid<TechnicalCardGroupDto> treeGrid;

    public FlowchartsViewTab(TechnicalCardService technicalCardService,
                             TechnicalCardGroupService technicalCardGroupService,
                             ProductService productService, TechnicalCardProductionService technicalCardProductionService,
                             ColumnsMaskService columnsMaskService, Notifications notifications) {
        this.technicalCardService = technicalCardService;
        this.technicalCardGroupService = technicalCardGroupService;
        this.productService = productService;
        this.notifications = notifications;
        this.technicalCardGroupDto = new TechnicalCardGroupDto();
        this.technicalCardDto = new TechnicalCardDto();
        this.productDtoList = productService.getAll();
        this.gridConfigurer = new GridConfigurer<>(grid, columnsMaskService, GRID_PRODUCTION_MAIN_FLOWCHART);
        this.technicalCardProductionService = technicalCardProductionService;
        this.filter = new GridFilter<>(grid);
        this.data = getData();
        configureGrid();
        treeGrid = getTreeGrid();
        reloadTreeGrid();
        configureFilter();
        setSizeFull();
        paginator = new GridPaginator<>(grid, data, 100);
        setHorizontalComponentAlignment(Alignment.CENTER, paginator);
        add(getToolBar(), filter, getMiddleLayout(grid), paginator);
    }

    private void configureGrid() {
        grid.addThemeVariants(GRID_STYLE);
        grid.addColumn("id").setHeader("ID").setId("ID");
        grid.addColumn("name").setHeader("Наименование").setId("Наименование");
        grid.addColumn("comment").setHeader("Комментарий").setId("Комментарий");
        grid.addColumn("productionCost").setHeader("Затраты на производство").setTextAlign(ColumnTextAlign.END)
                .setId("Затраты на производство");
//        grid.addColumn(iDto -> iDto.getTechnicalCardGroupDto().getName()).setHeader("Группа").setId("Группа");
        grid.addColumn(iDto -> technicalCardGroupService.getById(iDto.getTechnicalCardGroupId()).getName()).setHeader("Группа").setId("Группа");
        grid.getColumns().forEach(column -> column.setResizable(true).setAutoWidth(true).setSortable(true));
        gridConfigurer.addConfigColumnToGrid();

        grid.setHeight("64vh");
        grid.setWidth("150vh");
        grid.setColumnReorderingAllowed(true);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.addItemDoubleClickListener(event -> {
            TechnicalCardDto technicalCardDto = event.getItem();
            TechnicalCardModalWindow addTechnicalCardModalWindow =
                    new TechnicalCardModalWindow(technicalCardDto,
                            technicalCardService, technicalCardGroupService, productDtoList, technicalCardProductionService);
            addTechnicalCardModalWindow.addDetachListener(e -> updateList());
            addTechnicalCardModalWindow.getSaveButton();
            addTechnicalCardModalWindow.open();
        });
    }

    private HorizontalLayout getToolBar() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(buttonQuestion(), title(), buttonRefresh(), buttonPlusFlowcharts(), buttonPlusGroup(),
                buttonFilter(), text(), bigDecimalField(), valueSelect());
        horizontalLayout.setDefaultVerticalComponentAlignment(Alignment.CENTER);
        return horizontalLayout;
    }

    private Button buttonQuestion() {
        return Buttons.buttonQuestion(
                new VerticalLayout(
                        new Text("Технологическая карта описывает состав изделия — с комплектующими, сырьем и материалами. " +
                                "Технологическая карта может использоваться как в базовом, так и в расширенном способе производства"),
                        new Div(
                                new Text("Читать инструкцию: "),
                                new Anchor("#", "Учет производственных операций")),
                        new Div(
                                new Text("Видео: "),
                                new Anchor("#", "Базовый способ"))
                )
        );
    }

    private H2 title() {
        H2 title = new H2("Тех. карты");
        title.setHeight("2.2em");
        return title;
    }

    private Button buttonRefresh() {
        Button buttonRefresh = ButtonConfigurer.buttonRefresh();
        buttonRefresh.addClickListener(ev -> updateList());
        return buttonRefresh;
    }

    private Button buttonPlusFlowcharts() {
        Button addFlowchartsButton = new Button("Тех. карта", new Icon(VaadinIcon.PLUS_CIRCLE));
        TechnicalCardModalWindow addTechnicalCardModal =
                new TechnicalCardModalWindow(new TechnicalCardDto(), technicalCardService, technicalCardGroupService, productDtoList,
                        technicalCardProductionService);
        addFlowchartsButton.addClickListener(event -> addTechnicalCardModal.open());
        addTechnicalCardModal.addDetachListener(event -> updateList());
        addFlowchartsButton.getStyle().set("cursor", "pointer");
        return addFlowchartsButton;
    }

    private Button buttonPlusGroup() {
        Button buttonUnit = new Button("Группа", new Icon(VaadinIcon.PLUS_CIRCLE));
        TechnicalCardGroupModalWindow addTechnicalCardGroupModal =
                new TechnicalCardGroupModalWindow(technicalCardGroupDto, technicalCardGroupService);
        buttonUnit.addClickListener(event -> addTechnicalCardGroupModal.open());
        addTechnicalCardGroupModal.addDetachListener(event -> reloadTreeGrid());
        return buttonUnit;
    }

    private Button buttonFilter() {
        Button buttonFilter = new Button("Фильтр");
        buttonFilter.addClickListener(e -> filter.setVisible(!filter.isVisible()));
        return buttonFilter;
    }

    private TextField text() {
        TextField textField = SearchTextField.Search("Наименование или комментарий","300px");
        textField.addValueChangeListener(e -> updateListTextField());
        return textField;
    }

    public void updateListTextField() {
        if (!(text.getValue().equals(""))) {
            grid.setItems(technicalCardService.search(text.getValue()));
        } else {
            grid.setItems(technicalCardService.search("null"));
        }
    }

    private BigDecimalField bigDecimalField() {
        BigDecimalField numberField = new BigDecimalField();
        numberField.setPlaceholder("0");
        numberField.setWidth("35px");
        return numberField;
    }

    private Select<String> valueSelect() {
        return SelectConfigurer.configureDeleteSelect(() -> {
            deleteSelectedInternalOrders();
            grid.deselectAll();
            paginator.setData(getData());
        });
    }



    private void deleteSelectedInternalOrders() {
        if (!grid.getSelectedItems().isEmpty()) {
            for (TechnicalCardDto technicalCardDto : grid.getSelectedItems()) {
                technicalCardService.deleteById(technicalCardDto.getId());
                notifications.infoNotification("Выбранные заказы успешно удалены");
            }
        } else {
            notifications.errorNotification("Сначала отметьте галочками нужные заказы");
        }
    }

    private TreeGrid<TechnicalCardGroupDto> getTreeGrid() {
        TreeGrid<TechnicalCardGroupDto> treeGridLocal = new TreeGrid<>();
        treeGridLocal.setHeight("66vh");
        treeGridLocal.setWidth("25%");
        treeGridLocal.setThemeName("dense", true);
        treeGridLocal.addClassName("treeGreed");

        Grid.Column<TechnicalCardGroupDto> column = treeGridLocal
                .addHierarchyColumn(x -> "")
                .setHeader("Тех.карты")
                .setFlexGrow(0)
                .setWidth("auto")
                .setSortProperty("sortNumber")
                .setComparator(Comparator.comparing(TechnicalCardGroupDto::getSortNumber));

        treeGridLocal.addColumn(TechnicalCardGroupDto::getName);
        HeaderRow.HeaderCell cell = treeGridLocal.appendHeaderRow().getCell(column);
        Label label = new Label();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setPadding(false);
        horizontalLayout.setMargin(false);
        horizontalLayout.setDefaultVerticalComponentAlignment(Alignment.CENTER);
        Button closeButton = new Button(new Icon(VaadinIcon.CLOSE));
        closeButton.setVisible(false);
        closeButton.setMaxHeight("20px");
        horizontalLayout.add(label, closeButton);
        closeButton.addClickListener(event -> {
            closeButton.setVisible(false);
            label.setText("");
            paginator.setData(getData(), false);
            treeGridLocal.deselectAll();
        });
        cell.setComponent(horizontalLayout);
        treeGridLocal.setSelectionMode(Grid.SelectionMode.SINGLE);
        treeGridLocal.addSelectionListener(event -> {
            optional = event.getFirstSelectedItem();
            if (optional.isPresent()) {
                paginator.setData(filteredByGroupData(optional.get()), false);
                label.setText(optional.get().getName());
                closeButton.setVisible(true);
            }
        });
        return treeGridLocal;
    }

    private List<TechnicalCardDto> filteredByGroupData(TechnicalCardGroupDto technicalCardGroupDto) {
        return technicalCardService.getAllByTechnicalCardGroupId(technicalCardGroupDto.getId());
    }

    private SplitLayout getMiddleLayout(Grid<TechnicalCardDto> grid) {
        SplitLayout middleLayout = new SplitLayout();
        middleLayout.setWidth("100%");
        middleLayout.setHeight("66vh");
        middleLayout.addToPrimary(treeGrid);
        middleLayout.addToSecondary(grid);
        return middleLayout;
    }

    private void reloadTreeGrid() {
        treeGrid.getTreeData().clear();
        treeGrid.getTreeData().addRootItems(technicalCardGroupService.getAll());
        treeGrid.getDataProvider().refreshAll();
    }


    private List<TechnicalCardDto> getData() {
        return technicalCardService.getAll();
    }

    private void configureFilter() {
        filter.setFieldToIntegerField("id");
        filter.onSearchClick(e ->
                paginator.setData(technicalCardService.searchTechnicalCard(filter.getFilterData())));
        filter.onClearClick(e -> {
            if(optional.isPresent()){
                paginator.setData(filteredByGroupData(optional.get()), false);
            } else {
                paginator.setData(technicalCardService.getAll());
            }
        });
    }

    private void updateList() {
        GridPaginator<TechnicalCardDto> paginatorUpdateList
                = new GridPaginator<>(grid, technicalCardService.getAll(), 100);
        setHorizontalComponentAlignment(Alignment.CENTER, paginatorUpdateList);
        removeAll();
        add(getToolBar(), filter, getMiddleLayout(grid), paginator);
        reloadTreeGrid();
    }
}
