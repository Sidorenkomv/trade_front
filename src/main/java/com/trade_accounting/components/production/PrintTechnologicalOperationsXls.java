package com.trade_accounting.components.production;

import com.trade_accounting.components.util.PrintExcelDocument;
import com.trade_accounting.models.dto.production.TechnicalOperationsDto;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.production.TechnicalCardService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import org.apache.poi.ss.usermodel.Cell;

import java.util.List;

public class PrintTechnologicalOperationsXls extends PrintExcelDocument<TechnicalOperationsDto> {


    private final WarehouseService warehouseService;
    private final TechnicalCardService technicalCardService;
    private final CompanyService companyService;

    protected PrintTechnologicalOperationsXls(String pathToXlsTemplate, List<TechnicalOperationsDto> list,
                                              WarehouseService warehouseService,
                                              TechnicalCardService technicalCardService,
                                              CompanyService companyService) {
        super(pathToXlsTemplate, list);
        this.warehouseService = warehouseService;
        this.technicalCardService = technicalCardService;
        this.companyService = companyService;
    }

    @Override
    protected void selectValue(Cell editCell) {

    }

    @Override
    protected void tableSelectValue(String value, TechnicalOperationsDto model, Cell editCell) {

        switch (value) {
            case ("<date>"):
                editCell.setCellValue(model.getDate());
                break;
            case ("<comment>"):
                editCell.setCellValue(model.getComment());
                break;
            case ("<company>"):
                editCell.setCellValue(companyService.getById(model.getCompanyId()).getName());
                break;
            case ("<technicalCard>"):
                editCell.setCellValue(technicalCardService.getById(model.getTechnicalCardId()).getName());
                break;
            case("<volume>"):
                editCell.setCellValue(model.getVolume().toString());
                break;
            case ("<isSent>"):
                editCell.setCellValue(model.getIsSent());
                break;
            case ("<isPrint>"):
                editCell.setCellValue(model.getIsPrint());
                break;
        }
    }
}
