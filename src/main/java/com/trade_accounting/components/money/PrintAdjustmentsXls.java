package com.trade_accounting.components.money;

import com.trade_accounting.components.util.PrintExcelDocument;
import com.trade_accounting.models.dto.finance.BalanceAdjustmentDto;
import com.trade_accounting.models.dto.finance.PaymentDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.company.ContractService;
import com.trade_accounting.services.interfaces.company.ContractorService;
import com.trade_accounting.services.interfaces.util.ProjectService;
import org.apache.poi.ss.usermodel.Cell;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PrintAdjustmentsXls extends PrintExcelDocument<BalanceAdjustmentDto> {
    private final CompanyService companyService;
    private final ContractorService contractorService;
    private final EmployeeService employeeService;
    private final List<String> sumList;
    private int lengthOfsumList = 0;

    private String formatDate(String stringDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime formatDateTime = LocalDateTime.parse(stringDate);
        return formatDateTime.format(formatter);
    }

    protected PrintAdjustmentsXls(String pathToXlsTemplate, List<BalanceAdjustmentDto> list, CompanyService companyService,
                                  ContractorService contractorService, EmployeeService employeeService, List<String> sumList) {
        super(pathToXlsTemplate, list);
        this.companyService = companyService;
        this.contractorService = contractorService;
        this.employeeService = employeeService;
        this.sumList = sumList;
    }

    @Override
    protected void selectValue(Cell editCell) {
        String formula = editCell.getStringCellValue();
        switch (formula) {
            case ("<date>"):
                editCell.setCellValue(LocalDateTime.now());
                break;
            case ("<authorName>"):
                editCell.setCellValue(employeeService.getPrincipal().getEmail());
                break;
            default:
        }
    }

    @Override
    protected void tableSelectValue(String value, BalanceAdjustmentDto model, Cell editCell) {
        switch (value) {
            case ("<id>"):
                editCell.setCellValue(model.getId());
                break;
            case ("<data>"):
                editCell.setCellValue(formatDate(model.getDate()));
                break;
            case ("<company>"):
                editCell.setCellValue(companyService.getById(model.getCompanyId()).getName());
                break;
            case ("<contractor>"):
                editCell.setCellValue(contractorService.getById(model.getContractorId()).getName());
                break;
            case ("<account>"):
                editCell.setCellValue(model.getAccount());
                break;
            case ("<cashOffice>"):
                editCell.setCellValue(model.getCashOffice());
                break;
            case ("<sum>"):
                editCell.setCellValue(sumList.get(lengthOfsumList++));
                if (lengthOfsumList >= sumList.size()) {
                    lengthOfsumList = 0;
                }
                break;
            case ("<comment>"):
                editCell.setCellValue(model.getComment());
                break;
            case ("<when_changed>"):
                editCell.setCellValue(formatDate(model.getDateChanged()));
                break;
            case ("<who_changed>"):
                editCell.setCellValue(model.getWhoChanged());
                break;

            default:
        }
    }
}