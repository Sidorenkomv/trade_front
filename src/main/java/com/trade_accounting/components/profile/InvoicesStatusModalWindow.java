package com.trade_accounting.components.profile;

import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.models.dto.invoice.InvoicesStatusDto;
import com.trade_accounting.services.interfaces.invoice.InvoicesStatusService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;


public class InvoicesStatusModalWindow extends Dialog {

    private final TextArea statusField = new TextArea();
    private final InvoicesStatusService invoicesStatusService;
    private final Notifications notifications;
    private Long id;

    public InvoicesStatusModalWindow(InvoicesStatusDto invoicesStatusDto, InvoicesStatusService invoicesStatusService, Notifications notifications) {
        this.invoicesStatusService = invoicesStatusService;
        this.notifications = notifications;

        setCloseOnEsc(true);
        setCloseOnOutsideClick(true);

        id = invoicesStatusDto.getId();
        statusField.setValue(getFieldValueNotNull(invoicesStatusDto.getStatusName()));

        add(
                header(),
                configureStatusField()
        );
    }

    private HorizontalLayout header() {
        HorizontalLayout header = new HorizontalLayout();
        header.add(saveButton(), getCancelButton());
        return header;
    }

    private HorizontalLayout configureStatusField() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("Статус");
        label.setWidth("100px");
        statusField.setWidth("400px");
        horizontalLayout.add(label, statusField);
        return horizontalLayout;
    }

    private Button saveButton() {
        Button saveButton = new Button("Сохранить");
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);
        saveButton.addClickListener(event -> {
            InvoicesStatusDto invoicesStatusDto = new InvoicesStatusDto();
            invoicesStatusDto.setId(id);
            invoicesStatusDto.setStatusName(statusField.getValue());
            if (invoicesStatusDto.getId() != null) {
                invoicesStatusService.update(invoicesStatusDto);
                notifications.infoNotification("Статус успешно изменен");
                close();
            } else if (!invoicesStatusDto.getStatusName().equals("")) {
                invoicesStatusService.create(invoicesStatusDto);
                notifications.infoNotification("Статус успешно создан");
                close();
            } else {
                notifications.errorNotification("Поле статус не может быть пустым!");
            }
            close();
        });
        return saveButton;
    }

    private Button getCancelButton() {
        return new Button("Закрыть", event -> close());
    }

    private String getFieldValueNotNull(String value) {
        return value == null ? "" : value;
    }
}
