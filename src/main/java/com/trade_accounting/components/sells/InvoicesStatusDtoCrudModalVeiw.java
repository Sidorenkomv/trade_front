package com.trade_accounting.components.sells;

import com.trade_accounting.components.profile.InvoicesStatusModalWindow;
import com.trade_accounting.components.util.Buttons;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.models.dto.invoice.InvoicesStatusDto;
import com.trade_accounting.services.interfaces.invoice.InvoicesStatusService;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;


@UIScope
@SpringComponent
public class InvoicesStatusDtoCrudModalVeiw extends Dialog {
    private final InvoicesStatusService invoicesStatusService;
    private final Grid<InvoicesStatusDto> invoicesStatusDtoGrid = new Grid<>(InvoicesStatusDto.class, false);
    private final Notifications notifications;

    public InvoicesStatusDtoCrudModalVeiw(InvoicesStatusService invoicesStatusService, Notifications notifications) {
        this.invoicesStatusService = invoicesStatusService;
        this.notifications = notifications;

        setCloseOnOutsideClick(false);
        setCloseOnEsc(false);
        add(getToolbar());
        configureDialogView();
        add(getFooters());
    }

    private void configureDialogView() {
        invoicesStatusDtoGrid.addColumn(InvoicesStatusDto::getId).setHeader("ID");
        invoicesStatusDtoGrid.addColumn(InvoicesStatusDto::getStatusName).setHeader("Статуc");
        invoicesStatusDtoGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        invoicesStatusDtoGrid.setItems(invoicesStatusService.getAll());

        invoicesStatusDtoGrid.addItemDoubleClickListener(event -> {
            InvoicesStatusDto editInvoicesStatus = event.getItem();
            InvoicesStatusModalWindow onlineStoreModalWindow =
                    new InvoicesStatusModalWindow(editInvoicesStatus, invoicesStatusService, notifications);
            onlineStoreModalWindow.addDetachListener(e -> updateList());
            onlineStoreModalWindow.open();
        });

        setHeight("600px");
        setWidth("600px");
        add(invoicesStatusDtoGrid);
    }
    private HorizontalLayout getFooters() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(buttonUnit(), deleteButton(), closeButton());
        horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        return horizontalLayout;
    }

    private HorizontalLayout getToolbar() {
        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.add(buttonQuestion(), title());
        toolbar.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        return toolbar;
    }

    private H4 title() {
        H4 title = new H4("Настройки статусов");
        title.setHeight("2em");
        title.setWidth("200px");
        return title;
    }

    private Button buttonQuestion() {
        return Buttons.buttonQuestion(
                new Text("Вы находитесь на странице настройки статусов документов. " +
                        "При помощи статусов вы можете отслеживать жизненный цикл своих документов. " +
                        "Например, заказы покупателей могут проходить через статусы «Принят», «Оплачен», «Отгружен»."));
    }

    private Button buttonUnit() {
        return new Button("Добавить статус", new Icon(VaadinIcon.PLUS_CIRCLE),
                event -> new InvoicesStatusModalWindow(new InvoicesStatusDto(), invoicesStatusService, notifications)
                        .open());
    }

    private Button deleteButton() {
        return new Button("Удалить", new Icon(VaadinIcon.MINUS_CIRCLE), event -> {
            if (!invoicesStatusDtoGrid.getSelectedItems().isEmpty()) {
                for (InvoicesStatusDto invoicesStatusDto : invoicesStatusDtoGrid.getSelectedItems()) {
                    invoicesStatusService.deleteById(invoicesStatusDto.getId());
                    notifications.infoNotification("Выбранный статус успешно удален");
                    close();
                }
            } else {
                notifications.errorNotification("Сначала отметьте статус");
            }
            close();
        });
    }

    private Button closeButton() {
        return new Button("Закрыть", new Icon(VaadinIcon.CLOSE_CIRCLE),
                event -> close());
    }

    private void updateList() {
        invoicesStatusDtoGrid.setItems(invoicesStatusService.getAll());
    }
}
