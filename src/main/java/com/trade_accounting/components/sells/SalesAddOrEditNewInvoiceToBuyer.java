package com.trade_accounting.components.sells;

import com.trade_accounting.components.profile.SalesChannelModalWindow;
import com.trade_accounting.components.purchases.PurchasesChooseGoodsModalWin;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.company.ContractDto;
import com.trade_accounting.models.dto.company.ContractorDto;
import com.trade_accounting.models.dto.invoice.InvoiceDto;
import com.trade_accounting.models.dto.invoice.InvoiceProductDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductListDto;
import com.trade_accounting.models.dto.units.SalesChannelDto;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.ProductDto;
import com.trade_accounting.models.dto.warehouse.ProductPriceDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.company.ContractService;
import com.trade_accounting.services.interfaces.company.ContractorService;
import com.trade_accounting.services.interfaces.invoice.InvoiceProductService;
import com.trade_accounting.services.interfaces.invoice.InvoiceService;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersProductsListService;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersService;
import com.trade_accounting.services.interfaces.units.SalesChannelService;
import com.trade_accounting.services.interfaces.util.ProjectService;
import com.trade_accounting.services.interfaces.warehouse.ProductService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Shortcuts;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.trade_accounting.config.SecurityConstants.SELLS_INVOICES_TO_BUYERS_VIEW;

@Slf4j
@PreserveOnRefresh
@SpringComponent
@UIScope
public class SalesAddOrEditNewInvoiceToBuyer extends VerticalLayout implements BeforeLeaveObserver {
    private final SalesSubMenuView salesSubMenuView;
    private final ProductService productService;
    private final InvoicesToBuyersService invoicesToBuyersService;
    private final CompanyService companyService;
    private final WarehouseService warehouseService;
    private final ContractorService contractorService;
    private final ContractService contractService;
    private final ProjectService projectService;
    private final InvoiceService invoiceService;
    private final InvoiceProductService invoiceProductService;
    private final EmployeeService employeeService;
    private final InvoicesToBuyersProductsListService invoicesToBuyersProductsListService;
    private final SalesChannelService salesChannelService;
    private boolean isEdit = false;
    private InvoicesToBuyersDto invoicesToBuyersDto;
    private InvoicesToBuyersDto editingInvoiceToBuyersDto = new InvoicesToBuyersDto();
    private final ComboBox<CompanyDto> companyDtoComboBox = new ComboBox<>();
    private final ComboBox<WarehouseDto> warehouseDtoComboBox = new ComboBox<>();
    private final ComboBox<ContractDto> contractDtoComboBox = new ComboBox<>();
    private final ComboBox<ProjectDto> projectDtoComboBox = new ComboBox<>();
    private final ComboBox<ContractorDto> contractorDtoComboBox = new ComboBox<>();
    private final ComboBox<SalesChannelDto> salesChannelSelect = new ComboBox<>();
    private final ComboBox<InvoiceDto> invoiceSelectField = new ComboBox<>();
    private final DateTimePicker dateTimePicker = new DateTimePicker();
    private final DatePicker paymentDatePicker = new DatePicker();
    private final TextField text = new TextField();
    private final Checkbox isSpend = new Checkbox("Проведено");
    private final Checkbox isSent = new Checkbox("Отправлено");
    private final Checkbox isPrint = new Checkbox("Напечатано");
    private final TextField invoiceNumber = new TextField();
    private final TextArea commentConfig = new TextArea();
    private final Notifications notifications;
    private final Binder<InvoicesToBuyersDto> invoicesToBuyersDtoBinder = new Binder<>(InvoicesToBuyersDto.class);
    private final String TEXT_FOR_REQUEST_FIELD = "Обязательное поле";
    private final Dialog dialogOnCloseView = new Dialog();
    private Grid<InvoicesToBuyersProductListDto> grid = new Grid<>(InvoicesToBuyersProductListDto.class, false);
    private final GridPaginator<InvoicesToBuyersProductListDto> paginator;
    private final PurchasesChooseGoodsModalWin purchasesChooseGoodsModalWin;
    private List<InvoicesToBuyersProductListDto> tempInvoicesToBuyersProductsListDtos = new ArrayList<>();
    private List<InvoicesToBuyersProductListDto> invoicesToBuyersProductsListDto = new ArrayList<>();
    private final H4 totalPrice = new H4();
    private final Button deleteButton = new Button("Удалить", new Icon(VaadinIcon.TRASH));
    private final Button buttonFillFromOrder = new Button("Заполнить");
    private final H2 title = new H2();
    private static final String LABEL_WIDTH = "150px";
    private static final String FIELD_WIDTH = "350px";
    private String location = null;

    @Autowired
    public SalesAddOrEditNewInvoiceToBuyer(InvoicesToBuyersService invoicesToBuyersService,
                                           CompanyService companyService,
                                           WarehouseService warehouseService,
                                           ContractorService contractorService,
                                           ContractService contractService,
                                           ProjectService projectService, Notifications notifications,
                                           PurchasesChooseGoodsModalWin purchasesChooseGoodsModalWin,
                                           ProductService productService,
                                           InvoiceService invoiceService,
                                           InvoiceProductService invoiceProductService,
                                           InvoicesToBuyersProductsListService invoicesToBuyersProductsListService,
                                           @Lazy SalesSubMenuView salesSubMenuView,
                                           SalesChannelService salesChannelService,
                                           EmployeeService employeeService) {
        this.invoicesToBuyersService = invoicesToBuyersService;
        this.companyService = companyService;
        this.warehouseService = warehouseService;
        this.contractorService = contractorService;
        this.contractService = contractService;
        this.projectService = projectService;
        this.notifications = notifications;
        this.purchasesChooseGoodsModalWin = purchasesChooseGoodsModalWin;
        this.productService = productService;
        this.invoiceService = invoiceService;
        this.invoiceProductService = invoiceProductService;
        this.invoicesToBuyersProductsListService = invoicesToBuyersProductsListService;
        this.salesSubMenuView = salesSubMenuView;
        this.salesChannelService = salesChannelService;
        this.employeeService = employeeService;

        configureGrid();
        configureInvoiceSelectField();
        paginator = new GridPaginator<>(grid, tempInvoicesToBuyersProductsListDtos, 50);
        configureCloseViewDialog();
        setSizeFull();
        configureDateTimePickerField();
        add(upperButtonInModalView(), formToAddInvoicesToBuyers(), grid, commentAndTotalPriceLayout());
        purchasesChooseGoodsModalWin.addDetachListener(detachEvent -> {
            if (purchasesChooseGoodsModalWin.productSelect.getValue() != null
                    && purchasesChooseGoodsModalWin.priceSelect.getValue() != null
                    && purchasesChooseGoodsModalWin.amoSelect.getValue() != null) {

                addProduct(purchasesChooseGoodsModalWin.productSelect.getValue(),
                        purchasesChooseGoodsModalWin.priceSelect.getValue(),
                        purchasesChooseGoodsModalWin.amoSelect.getValue());
                purchasesChooseGoodsModalWin.productSelect.setValue(null);
                purchasesChooseGoodsModalWin.priceSelect.setValue(null);
                purchasesChooseGoodsModalWin.amoSelect.setValue(BigDecimal.valueOf(0.0));
            }
        });
        title.setWidthFull();
        buttonFillInOrder();
    }

    private void buttonFillInOrder() {
        buttonFillFromOrder.addClickListener(buttonClickEvent -> {
            dateTimePicker.setValue(LocalDateTime.parse(invoiceSelectField.getValue().getDate()));
            commentConfig.setValue(invoiceSelectField.getValue().getComment());
            companyDtoComboBox.setValue(companyService.getById(invoiceSelectField.getValue().getCompanyId()));
            contractorDtoComboBox.setValue(contractorService.getById(invoiceSelectField.getValue().getContractorId()));
            warehouseDtoComboBox.setValue(warehouseService.getById(invoiceSelectField.getValue().getWarehouseId()));
            isSpend.setValue(invoiceSelectField.getValue().getIsSpend());
            tempInvoicesToBuyersProductsListDtos = convertInvoiceProdToSupplier(invoiceProductService.getByInvoiceId(invoiceSelectField.getValue().getId()));
            paginator.setData(tempInvoicesToBuyersProductsListDtos);
            setTotalPrice();
        });
    }

    private void configureInvoiceSelectField() {
        List<InvoiceDto> invoices = invoiceService.getAll("EXPENSE");
        if (invoices != null) {
            invoiceSelectField.setItems(invoices);
        }
        invoiceSelectField.setWidth("400px");
        invoiceSelectField.setPlaceholder("Заполнить из заказа");
        invoiceSelectField.setItemLabelGenerator(e ->
                "Контрагент: " + contractorService.getById(e.getContractorId()).getName() +
                ", Компания: " + companyService.getById(e.getCompanyId()).getName()  +
                ", на сумму: " + getTotalPriceForSelectFild(e));
    }

    public BigDecimal getTotalPriceForSelectFild(InvoiceDto invoiceDto) {
        BigDecimal totalPriceForSelectFild = BigDecimal.valueOf(0);
        for (InvoiceProductDto iDto : invoiceProductService.getByInvoiceId(invoiceDto.getId())) {
            totalPriceForSelectFild = totalPriceForSelectFild.add(iDto.getPrice().multiply(iDto.getAmount()));
        }
        return totalPriceForSelectFild;
    }

    public void addProduct(ProductDto productDto, ProductPriceDto productPriceDto, BigDecimal amount) {
        InvoicesToBuyersProductListDto invoicesToBuyersProductListDto = new InvoicesToBuyersProductListDto();
        invoicesToBuyersProductListDto.setProductId(productDto.getId());
        invoicesToBuyersProductListDto.setAmount(amount);
        invoicesToBuyersProductListDto.setPrice(productPriceDto.getValue());
        invoicesToBuyersProductListDto.setSum(invoicesToBuyersProductListDto.getAmount().multiply(invoicesToBuyersProductListDto.getPrice()).setScale(2, RoundingMode.DOWN));
        invoicesToBuyersProductListDto.setPercentNds("20");
        invoicesToBuyersProductListDto.setNds(invoicesToBuyersProductListDto.getSum().multiply(BigDecimal.valueOf(0.2)).setScale(2, RoundingMode.DOWN));
        invoicesToBuyersProductListDto.setTotal(invoicesToBuyersProductListDto.getSum().add(invoicesToBuyersProductListDto.getNds()));
        if (!isProductInList(productDto)) {
            tempInvoicesToBuyersProductsListDtos.add(invoicesToBuyersProductListDto);
            paginator.setData(tempInvoicesToBuyersProductsListDtos);
            setTotalPrice();
        }
    }

    private void configureGrid() {
        grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        grid.removeAllColumns();
        grid.setItems(tempInvoicesToBuyersProductsListDtos);
        grid.addColumn(inPrDto -> tempInvoicesToBuyersProductsListDtos.indexOf(inPrDto) + 1).setHeader("№");
        grid.addColumn(inPrDto -> productService.getById(inPrDto.getProductId()).getName()).setHeader("Название").setSortable(true);
        grid.addColumn(inPrDto -> productService.getById(inPrDto.getProductId()).getDescription()).setHeader("Описание");
        grid.addColumn(InvoicesToBuyersProductListDto::getAmount).setHeader("Количество").setSortable(true).setKey("productAmount").setId("Количество");
        grid.addColumn(InvoicesToBuyersProductListDto::getPrice).setHeader("Цена").setSortable(true).setKey("productPrice").setId("Цена");
        grid.addColumn(InvoicesToBuyersProductListDto::getSum).setHeader("Сумма").setSortable(true).setKey("productSum").setId("Сумма");
        grid.addColumn(InvoicesToBuyersProductListDto::getPercentNds).setHeader("% НДС").setSortable(true).setKey("productPercentNds").setId("% НДС");
        grid.addColumn(InvoicesToBuyersProductListDto::getNds).setHeader("НДС").setSortable(true).setKey("productNds").setId("НДС");
        grid.addColumn(InvoicesToBuyersProductListDto::getTotal).setHeader("Всего").setSortable(true).setKey("productTotal").setId("Всего");

        grid.addComponentColumn(column -> {
            Button edit = new Button(new Icon(VaadinIcon.TRASH));
            edit.addClassName("delete");
            edit.addClickListener(e -> deleteProduct(column.getId()));
            return edit;
        });
    }

    public void clearField() {
        companyDtoComboBox.setValue(null);
        warehouseDtoComboBox.setValue(null);
        contractDtoComboBox.setValue(null);
        contractorDtoComboBox.setValue(null);
        invoiceSelectField.setValue(null);
        configureInvoiceSelectField();
        dateTimePicker.setValue(null);
        projectDtoComboBox.setValue(null);
        paymentDatePicker.setValue(null);
        text.setValue("");
        isSpend.setValue(false);
        invoiceNumber.setValue("");
        commentConfig.setValue("");
        totalPrice.setText("");
        tempInvoicesToBuyersProductsListDtos = new ArrayList<>();
        isSent.setValue(false);
        isPrint.setValue(false);
        isSpend.setValue(false);
        configureGrid();
    }

    public void setEditInvoicesToBuyers(InvoicesToBuyersDto invoicesToBuyers) {
        isEdit = true;
        this.invoicesToBuyersDto = invoicesToBuyers;
        editingInvoiceToBuyersDto = invoicesToBuyers;
        deleteButton.addClickListener(buttonClickEvent -> {
            deleteInvoice(editingInvoiceToBuyersDto);
            resetSourceTab(false);
            invoicesToBuyersDtoBinder.setValidatorsDisabled(true);
            clearField();
        });
        invoiceNumber.setValue(editingInvoiceToBuyersDto.getId().toString());
        dateTimePicker.setValue(LocalDateTime.parse(invoicesToBuyers.getDate()));
        paymentDatePicker.setValue(LocalDateTime.parse(invoicesToBuyers.getPlannedDatePayment()).toLocalDate());
        commentConfig.setValue(editingInvoiceToBuyersDto.getComment());
        companyDtoComboBox.setValue(companyService.getById(editingInvoiceToBuyersDto.getCompanyId()));
        warehouseDtoComboBox.setValue(warehouseService.getById(editingInvoiceToBuyersDto.getWarehouseId()));
        contractDtoComboBox.setValue(contractService.getById(editingInvoiceToBuyersDto.getContractId()));
        contractorDtoComboBox.setValue(contractorService.getById(editingInvoiceToBuyersDto.getContractorId()));
        if (invoicesToBuyers.getProjectId() != null) {
            projectDtoComboBox.setValue(projectService.getById(invoicesToBuyers.getProjectId()));
        }
        isSpend.setValue(invoicesToBuyers.getIsSpend());
        isSent.setValue(invoicesToBuyers.getIsSent());
        isPrint.setValue(invoicesToBuyers.getIsPrint());
        salesChannelSelect.setItems(salesChannelService.getAll());
        if (invoicesToBuyers.getSalesChannelId() != null) {
            salesChannelSelect.setValue(salesChannelService.getById(invoicesToBuyers.getSalesChannelId( )));
        }
        tempInvoicesToBuyersProductsListDtos = invoicesToBuyersProductsListService.getByInvoicesId(editingInvoiceToBuyersDto.getId());
        invoicesToBuyersProductsListDto = invoicesToBuyersProductsListService.getByInvoicesId(editingInvoiceToBuyersDto.getId());
        paginator.setData(tempInvoicesToBuyersProductsListDtos);
        setTotalPrice();
    }

    private HorizontalLayout upperButtonInModalView() {
        HorizontalLayout upperButton = new HorizontalLayout();
        upperButton.add(title, buttonsAndProductsAndInvoiceslayout());
        upperButton.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        return upperButton;
    }

    private VerticalLayout buttonsAndProductsAndInvoiceslayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayoutFirstRow = new HorizontalLayout();
        horizontalLayoutFirstRow.add(saveButton(), deleteButton, closeButton(), addProductButton());
        HorizontalLayout horizontalLayoutSecondRow = new HorizontalLayout();
        horizontalLayoutSecondRow.add(invoiceSelectField, buttonFillFromOrder);
        verticalLayout.add(horizontalLayoutFirstRow, horizontalLayoutSecondRow);
        return verticalLayout;
    }
    private Button saveButton() {
        return new Button("Сохранить", e -> {
            invoicesToBuyersDtoBinder.setValidatorsDisabled(false);
            if (!invoicesToBuyersDtoBinder.validate().isOk()) {
                invoicesToBuyersDtoBinder.validate().notifyBindingValidationStatusHandlers();
            } else {
                InvoicesToBuyersDto dto = new InvoicesToBuyersDto();
                if (!invoiceNumber.getValue().equals("")) {
                    dto.setId(Long.parseLong(invoiceNumber.getValue()));
                }
                dto.setDate(dateTimePicker.getValue().toString());
                dto.setPlannedDatePayment(LocalDateTime.of(paymentDatePicker.getValue(), LocalTime.now()).toString());
                dto.setCompanyId(companyDtoComboBox.getValue().getId());
                dto.setWarehouseId(warehouseDtoComboBox.getValue().getId());
                dto.setContractId(contractDtoComboBox.getValue().getId());
                dto.setSalesChannelId(salesChannelSelect.getValue().getId());
                dto.setTypeOfInvoice("EXPENSE");
                dto.setContractorId(contractorDtoComboBox.getValue().getId());
                dto.setProjectId(projectDtoComboBox.getValue() == null ?
                        null : projectDtoComboBox.getValue().getId());
                dto.setIsSpend(isSpend.getValue());
                dto.setIsSent(isSent.getValue());
                dto.setIsPrint(isPrint.getValue());
                dto.setComment(commentConfig.getValue());
                if (isEdit) {
                    dto.setEmployeeId(this.invoicesToBuyersDto.getEmployeeId());
                    dto.setIsRecyclebin(this.invoicesToBuyersDto.getIsRecyclebin());
                } else {
                    dto.setEmployeeId(employeeService.getPrincipal().getId());
                    dto.setIsRecyclebin(false);
                }
                dto.setLastModifiedDate(LocalDateTime.now().toString());
                invoicesToBuyersService.create(dto);

                deleteRemovedProducts();

                saveProducts(dto);
                resetSourceTab(false);
                notifications.infoNotification(String.format("Счет покупателя № %s сохранен", dto.getId()));
                invoicesToBuyersDtoBinder.setValidatorsDisabled(true);
                clearField();
            }
        });
    }

    private Button closeButton() {
        return new Button("Закрыть",
                new Icon(VaadinIcon.CLOSE),
                event -> {
                    resetSourceTab(true);
                });
    }

    private Button addProductButton() {
        Button button = new Button("Добавить из справочника", new Icon(VaadinIcon.PLUS_CIRCLE));
        button.addClickListener(buttonClickEvent -> {
            purchasesChooseGoodsModalWin.updateProductList();
            purchasesChooseGoodsModalWin.open();
        });
        return button;
    }

    private VerticalLayout formToAddInvoicesToBuyers() {
        VerticalLayout form = new VerticalLayout();
        form.add(horizontalLayout1(), horizontalLayout2(),
                horizontalLayout3(), dataPlaneConfigure(), configureSalesChannelSelect());
        return form;
    }

    private HorizontalLayout horizontalLayout1() {
        HorizontalLayout hLay1 = new HorizontalLayout();
        hLay1.add(dataConfigure(), isSpend, isSent, isPrint);
        return hLay1;
    }

    private HorizontalLayout horizontalLayout2() {
        HorizontalLayout hLay2 = new HorizontalLayout();
        hLay2.add(companyConfigure(), warehouseConfigure());
        return hLay2;
    }

    private HorizontalLayout horizontalLayout3() {
        HorizontalLayout hLay3 = new HorizontalLayout();
        hLay3.add(contractorConfigure(), contractConfigure());
        return hLay3;
    }

    private HorizontalLayout dataConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Label label = new Label("Счет покупателя №");
        label.setWidth(LABEL_WIDTH);

        invoiceNumber.setWidth("50px");
        invoiceNumber.setEnabled(true);
        invoiceNumber.setRequired(true);
        invoiceNumber.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(invoiceNumber)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getIdValid, InvoicesToBuyersDto::setIdValid);

        Label label2 = new Label("от");
        dateTimePicker.setWidth(FIELD_WIDTH);
        dateTimePicker.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(dateTimePicker)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getDateValid, InvoicesToBuyersDto::setDateValid);
        horizontalLayout.add(label, invoiceNumber, label2, dateTimePicker);
        return horizontalLayout;
    }

    private HorizontalLayout contractorConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<ContractorDto> contractorDtos = contractorService.getAll();
        if (contractorDtos != null) {
            contractorDtoComboBox.setItems(contractorDtos);
        }
        contractorDtoComboBox.setItemLabelGenerator(ContractorDto::getName);
        contractorDtoComboBox.setWidth(FIELD_WIDTH);
        contractorDtoComboBox.setRequired(true);
        contractorDtoComboBox.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(contractorDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getContractorDtoValid, InvoicesToBuyersDto::setContractorDtoValid);
        Label label = new Label("Контрагент");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, contractorDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout companyConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<CompanyDto> companyDtos = companyService.getAll();
        if (companyDtos != null) {
            companyDtoComboBox.setItems(companyDtos);
        }
        companyDtoComboBox.setItemLabelGenerator(CompanyDto::getName);
        companyDtoComboBox.setWidth(FIELD_WIDTH);
        companyDtoComboBox.setRequired(true);
        companyDtoComboBox.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(companyDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getCompanyDtoValid, InvoicesToBuyersDto::setCompanyDtoValid);
        Label label = new Label("Компания");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, companyDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout warehouseConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<WarehouseDto> warehouseDtos = warehouseService.getAll();
        if (warehouseDtos != null) {
            warehouseDtoComboBox.setItems(warehouseDtos);
        }
        warehouseDtoComboBox.setItemLabelGenerator(WarehouseDto::getName);
        warehouseDtoComboBox.setWidth(FIELD_WIDTH);
        warehouseDtoComboBox.setRequired(true);
        warehouseDtoComboBox.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(warehouseDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getWarehouseDtoValid, InvoicesToBuyersDto::setWarehouseDtoValid);
        Label label = new Label("Склад");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, warehouseDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout contractConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<ContractDto> contractDtos = contractService.getAll();
        if (contractDtos != null) {
            contractDtoComboBox.setItems(contractDtos);
        }
        contractDtoComboBox.setItemLabelGenerator(ContractDto::getNumber);
        contractDtoComboBox.setWidth(FIELD_WIDTH);
        contractDtoComboBox.setRequired(true);
        contractDtoComboBox.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(contractDtoComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getContractDtoValid, InvoicesToBuyersDto::setContractDtoValid);
        Label label = new Label("Договор");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, contractDtoComboBox);
        return horizontalLayout;
    }

    private HorizontalLayout totalPriceLayout() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        H4 label = new H4("Итого:");
        totalPrice.setText(getTotalPrice().toString());
        totalPrice.setHeight("2.0em");
        horizontalLayout.add(label,totalPrice);
        return horizontalLayout;
    }

    private HorizontalLayout dataPlaneConfigure() {
        HorizontalLayout horizontal = new HorizontalLayout();

        Label label = new Label("План. дата оплаты");
        label.setWidth(LABEL_WIDTH);
        paymentDatePicker.setWidth(FIELD_WIDTH);
        paymentDatePicker.setRequiredIndicatorVisible(true);
        invoicesToBuyersDtoBinder.forField(paymentDatePicker)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(InvoicesToBuyersDto::getPaymentDateValid, InvoicesToBuyersDto::setPaymentDateValid);
        horizontal.add(label, paymentDatePicker, projectConfigure());
        return horizontal;
    }

    private HorizontalLayout commentAndTotalPriceLayout() {
        HorizontalLayout horizontal3 = new HorizontalLayout();
        commentConfig.setPlaceholder("Комментарий");
        commentConfig.setWidth("500px");
        horizontal3.add(commentConfig, totalPriceLayout());
        return horizontal3;
    }

    private void configureCloseViewDialog() {
        dialogOnCloseView.add(new Text("Вы уверены? Несохраненные данные будут потеряны!!!"));
        dialogOnCloseView.setCloseOnEsc(false);
        dialogOnCloseView.setCloseOnOutsideClick(false);
        Button confirmButton = new Button("Продолжить", event -> {
            clearField();
            dialogOnCloseView.close();
        });
        Button cancelButton = new Button("Отменить", event -> dialogOnCloseView.close());
        Shortcuts.addShortcutListener(dialogOnCloseView, dialogOnCloseView::close, Key.ESCAPE);
        dialogOnCloseView.add(new Div(confirmButton, new Div(), cancelButton));
    }

    private boolean isProductInList(ProductDto productDto) {
        boolean isExists = false;
        for (InvoicesToBuyersProductListDto invoicesToBuyersProductListDto : tempInvoicesToBuyersProductsListDtos) {
            if (invoicesToBuyersProductListDto.getProductId().equals(productDto.getId())) {
                isExists = true;
                break;
            }
        }
        return isExists;
    }

    public BigDecimal getTotalPrice() {
        BigDecimal totalPrice = BigDecimal.valueOf(0.0);
        for (InvoicesToBuyersProductListDto invoicesToBuyersProductListDto : tempInvoicesToBuyersProductsListDtos) {
            totalPrice = totalPrice.add(invoicesToBuyersProductListDto.getTotal());
        }
        return totalPrice;
    }

    private void setTotalPrice() {
        totalPrice.setText(
                String.format("%.2f", getTotalPrice())
        );
    }

    private void configureDateTimePickerField() {
        dateTimePicker.setValue(LocalDateTime.now());
    }

    private void saveProducts(InvoicesToBuyersDto invoicesToBuyersDto) {
        for (InvoicesToBuyersProductListDto invoicesToBuyersProductListDto: tempInvoicesToBuyersProductsListDtos) {
            invoicesToBuyersProductListDto.setInvoicesToBuyersId(invoicesToBuyersDto.getId());
            invoicesToBuyersProductsListService.create(invoicesToBuyersProductListDto);
        }
    }

    private void deleteProduct(Long id) {
        InvoicesToBuyersProductListDto found = new InvoicesToBuyersProductListDto();
        for (InvoicesToBuyersProductListDto invoicesToBuyersProductListDto : tempInvoicesToBuyersProductsListDtos) {
            if (Objects.equals(invoicesToBuyersProductListDto.getId( ), id)) {
                found = invoicesToBuyersProductListDto;
                break;
            }
        }
        tempInvoicesToBuyersProductsListDtos.remove(found);
        paginator.setData(tempInvoicesToBuyersProductsListDtos);
        setTotalPrice();
    }

    private List<InvoicesToBuyersProductListDto> convertInvoiceProdToSupplier(List<InvoiceProductDto> invoiceProductDto) {
        List<InvoicesToBuyersProductListDto> invoicesToBuyersProductListDtos = new ArrayList<>();
        for (InvoiceProductDto iPDto: invoiceProductDto) {
            InvoicesToBuyersProductListDto invoicesToBuyersProductListDto = new InvoicesToBuyersProductListDto();
            invoicesToBuyersProductListDto.setProductId(iPDto.getProductId());
            invoicesToBuyersProductListDto.setAmount(iPDto.getAmount());
            invoicesToBuyersProductListDto.setPrice(iPDto.getPrice());
            invoicesToBuyersProductListDto.setSum(invoicesToBuyersProductListDto.getAmount().multiply(invoicesToBuyersProductListDto.getPrice()).setScale(2, RoundingMode.DOWN));
            invoicesToBuyersProductListDto.setPercentNds("20");
            invoicesToBuyersProductListDto.setNds(invoicesToBuyersProductListDto.getSum().multiply(BigDecimal.valueOf(0.2)).setScale(2, RoundingMode.DOWN));
            invoicesToBuyersProductListDto.setTotal(invoicesToBuyersProductListDto.getSum().add(invoicesToBuyersProductListDto.getNds()));
            invoicesToBuyersProductListDtos.add(invoicesToBuyersProductListDto);
        }
        return invoicesToBuyersProductListDtos;
    }

    private HorizontalLayout projectConfigure() {
        HorizontalLayout projectLayout = new HorizontalLayout();
        List<ProjectDto> projects = projectService.findByArchiveFalse();
        if (projects != null) {
            projectDtoComboBox.setItems(projects);
        }
        projectDtoComboBox.setItemLabelGenerator(ProjectDto::getName);
        projectDtoComboBox.setWidth(FIELD_WIDTH);
        Label label = new Label("Проект");
        label.setWidth(LABEL_WIDTH);
        projectLayout.add(label, projectDtoComboBox);
        return projectLayout;
    }

    private HorizontalLayout configureSalesChannelSelect() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<SalesChannelDto> salesChannels = salesChannelService.getAll();
        if (salesChannels != null) {
            salesChannelSelect.setItems(salesChannels);
        }
        salesChannelSelect.setItemLabelGenerator(SalesChannelDto::getName);
        salesChannelSelect.setWidth(FIELD_WIDTH);
        Label label = new Label("Канал продаж");
        label.setWidth(LABEL_WIDTH);
        horizontalLayout.add(label, salesChannelSelect, buttonSalesChannel());
        return horizontalLayout;
    }

    private Button buttonSalesChannel() {
        Button salesChannelButton = new Button(new Icon(VaadinIcon.PLUS_CIRCLE));
        salesChannelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        SalesChannelModalWindow salesChannelModalWindow = new SalesChannelModalWindow(new SalesChannelDto(), salesChannelService);
        salesChannelButton.addClickListener(event -> salesChannelModalWindow.open());
        return salesChannelButton;
    }

    private void deleteRemovedProducts() {
        for (InvoicesToBuyersProductListDto removed : invoicesToBuyersProductsListDto) {
            if(!tempInvoicesToBuyersProductsListDtos.contains(removed)) {
                invoicesToBuyersProductsListService.deleteById(removed.getId());
            }
        }
    }

    private void deleteInvoice(InvoicesToBuyersDto invoice) {
                List<InvoicesToBuyersProductListDto> productListDtos = invoicesToBuyersProductsListService.getByInvoicesId(invoice.getId());
                for (InvoicesToBuyersProductListDto invoicesToBuyersProductListDto: productListDtos) {
                    invoicesToBuyersProductsListService.deleteById(invoicesToBuyersProductListDto.getId());
                }
                invoicesToBuyersService.deleteById(invoice.getId());
                notifications.infoNotification("Выбранные счета успешно удалены");
    }

    // Поменять реализацию (выполняется избыточное "пересобирание" диалога при каждом его вызове)
    private void terminateCloseDialog(BeforeLeaveEvent beforeLeaveEvent) {
        BeforeLeaveEvent.ContinueNavigationAction action = beforeLeaveEvent.postpone();
        dialogOnCloseView.removeAll();

        Button confirmButton = new Button("Продолжить", event -> {
            dialogOnCloseView.close();
            changeTabSwitchType(false);
            action.proceed();
        });
        Button cancelButton = new Button("Отменить", event -> {
            dialogOnCloseView.close();
        });

        dialogOnCloseView.add(new VerticalLayout(
                new Text("Вы уверены? Несохраненные данные будут утеряны!"),
                new HorizontalLayout(cancelButton, confirmButton))
        );

        dialogOnCloseView.open();
    }

    private void changeTabSwitchType(boolean isProtected) {
        switch (location) {
            case SELLS_INVOICES_TO_BUYERS_VIEW:
                if (isProtected) {
                    salesSubMenuView.setProtectedTabSwitch();
                } else {
                    salesSubMenuView.releaseProtectedTabSwitch();
                }
        }
    }

    private void resetSourceTab(boolean isProtected) {
        switch (location) {
            case SELLS_INVOICES_TO_BUYERS_VIEW:
                salesSubMenuView.resetTabSelection(location, isProtected);
        }
    }

    public void setProtectedTabSwitch() {
        changeTabSwitchType(true);
    }

    public void setUpdateState(boolean isUpdate) {
        title.setText(isUpdate ? "Редактирование счета" : "Добавление счета");
        deleteButton.setVisible(isUpdate);
        buttonFillFromOrder.setVisible(!isUpdate);
        invoiceSelectField.setVisible(!isUpdate);
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        terminateCloseDialog(beforeLeaveEvent);
    }
}
