package com.trade_accounting.components.sells;

import com.trade_accounting.components.util.Buttons;
import com.trade_accounting.components.util.GridConfigurer;
import com.trade_accounting.components.util.GridFilter;
import com.trade_accounting.components.util.GridPaginator;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.components.util.SearchTextField;
import com.trade_accounting.components.util.configure.components.button.ButtonConfigurer;
import com.trade_accounting.components.util.configure.components.input.number.NumberFieldConfigurer;
import com.trade_accounting.components.util.configure.components.select.SelectConfigurer;
import com.trade_accounting.components.util.configure.components.select.SelectConstants;
import com.trade_accounting.components.util.configure.components.select.SelectExt;
import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.company.ContractorDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductListDto;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.company.ContractorService;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersProductsListService;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersService;
import com.trade_accounting.services.interfaces.util.ColumnsMaskService;
import com.trade_accounting.services.interfaces.util.ProjectService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static com.trade_accounting.config.SecurityConstants.GRID_SALES_MAIN_INVOICES_TO_BUYERS;
import static com.trade_accounting.config.SecurityConstants.SELLS_INVOICES_TO_BUYERS_VIEW;

@Slf4j
@SpringComponent
@UIScope
public class SalesSubInvoicesToBuyersView extends VerticalLayout implements AfterNavigationObserver {
    private final CompanyService companyService;
    private final WarehouseService warehouseService;
    private final ContractorService contractorService;
    private final InvoicesToBuyersService invoicesToBuyersService;
    private final InvoicesToBuyersProductsListService invoicesToBuyersProductsListService;
    private final ProjectService projectService;
    private final SalesAddOrEditNewInvoiceToBuyer salesAddNewInvoiceTuBuyer;
    private final Notifications notifications;
    private HorizontalLayout actions;
    private final Grid<InvoicesToBuyersDto> grid = new Grid<>(InvoicesToBuyersDto.class, false);
    private final GridConfigurer<InvoicesToBuyersDto> gridConfigurer;
    private GridPaginator<InvoicesToBuyersDto> paginator;
    private final GridFilter<InvoicesToBuyersDto> filter;
    private final String typeOfInvoice = "RECEIPT";
    private final GridVariant[] GRID_STYLE = {GridVariant.LUMO_ROW_STRIPES, GridVariant.LUMO_WRAP_CELL_CONTENT, GridVariant.LUMO_COLUMN_BORDERS};

    @Autowired
    public SalesSubInvoicesToBuyersView(CompanyService companyService, WarehouseService warehouseService,
                                        ContractorService contractorService,
                                        InvoicesToBuyersProductsListService invoicesToBuyersProductsListService,
                                        InvoicesToBuyersService invoicesToBuyersService,
                                        ProjectService projectService, ColumnsMaskService columnsMaskService,
                                        @Lazy Notifications notifications,
                                        @Lazy SalesAddOrEditNewInvoiceToBuyer salesAddNewInvoiceTuBuyer) {
        this.companyService = companyService;
        this.warehouseService = warehouseService;
        this.contractorService = contractorService;
        this.invoicesToBuyersProductsListService = invoicesToBuyersProductsListService;
        this.projectService = projectService;
        this.invoicesToBuyersService = invoicesToBuyersService;
        this.salesAddNewInvoiceTuBuyer = salesAddNewInvoiceTuBuyer;
        this.notifications = notifications;
        this.gridConfigurer = new GridConfigurer<>(grid, columnsMaskService, GRID_SALES_MAIN_INVOICES_TO_BUYERS);

        configureActions();
        configureGrid();
        this.filter = new GridFilter<>(grid);
        this.paginator = new GridPaginator<>(grid);
        configureFilter();

        refreshContent();
    }

    private void configureActions() {
        actions = new HorizontalLayout();
        actions.add(buttonQuestion(), title(), buttonRefresh(), buttonUnit(), buttonFilter(), text(),
                numberField(), valueSelect(), valueStatus(), valueCreate(), valuePrint(), buttonSettings());
        actions.setDefaultVerticalComponentAlignment(Alignment.CENTER);
    }

    private Button buttonQuestion() {
        return Buttons.buttonQuestion(
                new Text("Счет является основанием для получения оплаты и проведения отгрузки. " +
                        "Счет можно создать на основе заказа покупателя или вручную. " +
                        "Несколько счетов можно объединить: отметьте нужные счета флажками, нажмите справа вверху Изменить → Объединить. " +
                        "Исходные счета также сохранятся. " +
                        "Счета можно распечатывать или отправлять покупателям в виде файлов. " +
                        "Читать инструкцию: "),
                new Anchor("#", "Счета покупателям"));
    }

    private void configureGrid() {
        grid.addThemeVariants(GRID_STYLE);
        grid.addColumn("id").setHeader("№").setId("№");
        grid.addColumn(dto -> formatDate(dto.getDate())).setHeader("Дата и время")
                .setKey("date").setId("Дата и время");
        grid.addColumn(dto -> companyService.getById(dto.getCompanyId()).getName()).setHeader("Компания")
                .setKey("companyId").setId("Компания");
        grid.addColumn(dto -> contractorService.getById(dto.getContractorId()).getName()).setHeader("Контрагент")
                .setKey("contractorId").setId("Контрагент");
        grid.addColumn(dto -> dto.getProjectId() != null ?
                        projectService.getById(dto.getProjectId()).getName() : "").setHeader("Проект")
                .setKey("projectDto")
                .setId("Проект");

        grid.addColumn(dto -> warehouseService.getById(dto.getWarehouseId()).getName()).setHeader("Со склада")
                .setKey("warehouseId").setId("Со склада");
        grid.addColumn(dto -> getTotalPrice(dto.getId())).setHeader("Сумма").setTextAlign(ColumnTextAlign.END)
                .setId("Сумма");
        grid.getColumns().forEach(column -> column.setResizable(true).setAutoWidth(true).setSortable(true));
        gridConfigurer.addConfigColumnToGrid();

        grid.setHeight("66vh");
        grid.setColumnReorderingAllowed(true);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.addItemClickListener(event -> {
            if (event.getClickCount() == 2) {
                InvoicesToBuyersDto invoicesToBuyersDto = event.getItem();
                salesAddNewInvoiceTuBuyer.setEditInvoicesToBuyers(invoicesToBuyersDto);
                salesAddNewInvoiceTuBuyer.setUpdateState(true);
                salesAddNewInvoiceTuBuyer.setLocation(SELLS_INVOICES_TO_BUYERS_VIEW);
                salesAddNewInvoiceTuBuyer.setProtectedTabSwitch( );
                removeAll( );
                add(salesAddNewInvoiceTuBuyer);
            }
        });
    }

    private void configureFilter() {
        filter.setFieldToIntegerField("id");
        filter.setFieldToDatePicker("date");
        filter.setFieldToComboBox("companyId", CompanyDto::getName, companyService.getAll());
        filter.setFieldToComboBox("contractorId", ContractorDto::getName, contractorService.getAll());
        filter.setFieldToComboBox("projectDto", ProjectDto::getName, projectService.findByArchiveFalse());
        filter.setFieldToComboBox("warehouseId", WarehouseDto::getName, warehouseService.getAll());
        filter.onSearchClick(e -> {
            Map<String, String> map = filter.getFilterData();
            map.put("typeOfInvoice", typeOfInvoice);
            paginator.setData(invoicesToBuyersService.searchByFilter(map));
        });
        filter.onClearClick(e -> paginator.setData(invoicesToBuyersService.getAll(typeOfInvoice)));
    }

    private Button buttonRefresh() {
        Button buttonRefresh = ButtonConfigurer.buttonRefresh();
        buttonRefresh.addClickListener(ev -> updateList());
        return buttonRefresh;
    }

    private Button buttonUnit() {
        Button buttonUnit = new Button("Счёт", new Icon(VaadinIcon.PLUS_CIRCLE));
        buttonUnit.addClickListener(e -> {
            salesAddNewInvoiceTuBuyer.clearField();
            salesAddNewInvoiceTuBuyer.setUpdateState(false);
            salesAddNewInvoiceTuBuyer.setLocation(SELLS_INVOICES_TO_BUYERS_VIEW);
            salesAddNewInvoiceTuBuyer.setProtectedTabSwitch();
            removeAll();
            add(salesAddNewInvoiceTuBuyer);
        });
        return buttonUnit;
    }


    public void refreshContent() {
        List<InvoicesToBuyersDto> data = getData();
        paginator.setData(data);
        paginator.setItemsPerPage(50);
        removeAll();
        add(actions, filter, grid, paginator);
    }

    private Button buttonFilter() {
        Button buttonFilter = new Button("Фильтр");
        buttonFilter.addClickListener(e -> filter.setVisible(!filter.isVisible()));
        return buttonFilter;
    }

    private Button buttonSettings() {
        return new Button(new Icon(VaadinIcon.COG_O));
    }

    private NumberField numberField() {
        NumberField numberField = NumberFieldConfigurer.NumberFieldStandart();
        return numberField;
    }

    private TextField text() {
        TextField textField = SearchTextField.Search("Номер, Склад или компания","300px");
        textField.addValueChangeListener(e -> updateList(textField.getValue()));
        return textField;
    }

    private void updateList(String text) {
        grid.setItems(invoicesToBuyersService.getAll());
        grid.setItems(invoicesToBuyersService.findBySearchAndTypeOfInvoice(text, typeOfInvoice));
    }

    private void updateList() {
        grid.setItems(invoicesToBuyersService.getAll(typeOfInvoice));
    }

    private H4 title() {
        H4 title = new H4("Счета покупателям");
        title.setHeight("2.2em");
        title.setWidth("80px");
        return title;
    }

    private void deleteSelectedInvoices() {
        if (!grid.getSelectedItems().isEmpty()) {
            for (InvoicesToBuyersDto invoicesToBuyersDto : grid.getSelectedItems()) {
                List<InvoicesToBuyersProductListDto> productListDtos = invoicesToBuyersProductsListService.getByInvoicesId(invoicesToBuyersDto.getId());
                for (InvoicesToBuyersProductListDto invoicesToBuyersProductListDto: productListDtos) {
                    invoicesToBuyersProductsListService.deleteById(invoicesToBuyersProductListDto.getId());
                }
                invoicesToBuyersService.deleteById(invoicesToBuyersDto.getId());
                notifications.infoNotification("Выбранные счета успешно удалены");
            }
        } else {
            notifications.errorNotification("Сначала отметьте галочками нужные счета");
        }
    }

    private Select<String> valueSelect() {
        return SelectConfigurer.configureDeleteSelect(() -> {
            deleteSelectedInvoices();
            grid.deselectAll();
            paginator.setData(getData());
        });
    }

    private Select<String> valueStatus() {
        return SelectConfigurer.configureStatusSelect();

    }

    private Select<String> valueCreate() {
        return new SelectExt.SelectBuilder<String>()
                .item(SelectConstants.CREATE_SELECT_ITEM)
                .defaultValue(SelectConstants.CREATE_SELECT_ITEM)
                .width(SelectConstants.SELECT_WIDTH_130PX)
                .build();
    }

    private Select<String> valuePrint() {
        return SelectConfigurer.configurePrintSelect();
    }

    private List<InvoicesToBuyersDto> getData() {
        return invoicesToBuyersService.getAll(typeOfInvoice);
    }

    private String getTotalPrice(Long id) {
        var totalPrice = invoicesToBuyersProductsListService.getByInvoicesId(id).stream()
                .map(ipdto -> ipdto.getPrice().multiply(ipdto.getAmount()))
                .reduce(BigDecimal.valueOf(0.0), BigDecimal::add);
        return String.format("%.2f", totalPrice);
    }

    private static String formatDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime formatDateTime = LocalDateTime.parse(date);
        return formatDateTime.format(formatter);
    }
    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        updateList();
    }
}
