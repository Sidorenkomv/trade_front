package com.trade_accounting.components.goods;

import com.google.gson.internal.LinkedTreeMap;
import com.trade_accounting.FileWithChanges;
import com.trade_accounting.components.util.Notifications;
import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.finance.LossDto;
import com.trade_accounting.models.dto.finance.LossProductDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.finance.LossProductService;
import com.trade_accounting.services.interfaces.finance.LossService;
import com.trade_accounting.services.interfaces.warehouse.ProductService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;

import static com.trade_accounting.config.SecurityConstants.GOODS_LOSS_VIEW;

@UIScope
@SpringComponent
@Slf4j
public class LossModalWindow extends Dialog {
    LossDto lossDto;
    private final WarehouseService warehouseService;
    private final LossService lossService;
    private final CompanyService companyService;
    private final LossProductService lossProductService;
    private final ProductService productService;

    private final EmployeeService employeeService;
    private final Notifications notifications;
    private final TitleForModal title;

    private final ComboBox<CompanyDto> companyComboBox = new ComboBox<>();
    private final ComboBox<WarehouseDto> warehouseComboBox = new ComboBox<>();
    private final DateTimePicker dateTimePicker = new DateTimePicker();
    private final MultiselectComboBox<Long> lossProductsIdComboBox= new MultiselectComboBox();
    private final com.vaadin.flow.component.checkbox.Checkbox checkboxIsSent = new Checkbox("Отправленно");
    private final com.vaadin.flow.component.checkbox.Checkbox checkboxIsPrint = new Checkbox("Напечатано");
    private final com.vaadin.flow.component.textfield.TextField returnNumber = new com.vaadin.flow.component.textfield.TextField();
    private final com.vaadin.flow.component.textfield.TextArea textArea = new TextArea();
    private final Binder<LossDto> lossDtoBinder = new Binder<>(LossDto.class);
    FileWithChanges file;

    private final String TEXT_FOR_REQUEST_FIELD = "Обязательное поле";

    public LossModalWindow(WarehouseService warehouseService,
                           LossService lossService,
                           CompanyService companyService,
                           LossProductService lossProductService,
                           ProductService productService,
                           Notifications notifications,
                           TitleForModal title,
                           EmployeeService employeeService) {
        this.companyService = companyService;
        this.warehouseService = warehouseService;
        this.lossService = lossService;
        this.lossProductService = lossProductService;
        this.productService = productService;
        this.employeeService = employeeService;
        this.notifications = notifications;
        this.title = title;

        setSizeFull();
    }

    public void setLossEdit(LossDto editDto) {
        this.lossDto = editDto;
        this.file = new FileWithChanges(editDto.getId(), "Losses", "Loss", "Изменения");
        add(headerLayout(), formLayout());
        returnNumber.setValue(editDto.getId().toString());
        dateTimePicker.setValue(LocalDateTime.parse(editDto.getDate()));
        textArea.setValue(editDto.getComment());
        warehouseComboBox.setValue(warehouseService.getById(editDto.getWarehouseId()));
        companyComboBox.setValue(companyService.getById(editDto.getCompanyId()));
        Set<Long> idSet = new HashSet<>(lossDto.getLossProductsIds());
        lossProductsIdComboBox.setValue(idSet);
        checkboxIsPrint.setValue(lossDto.getIsPrint());
        checkboxIsSent.setValue(lossDto.getIsSent());
    }

    private HorizontalLayout headerLayout() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(title(), saveButton(), closeButton(), addGoodButton());
        return horizontalLayout;
    }
    private VerticalLayout formLayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(formLayout1(), formLayout2(), formLayout4());
        return verticalLayout;
    }

    private H2 title() {
        return new H2("Списание");
    }

    private com.vaadin.flow.component.button.Button saveButton() {
        return new com.vaadin.flow.component.button.Button("Сохранить", e -> {
            //save
            if (!lossDtoBinder.validate().isOk()) {
                lossDtoBinder.validate().notifyBindingValidationStatusHandlers();
            } else {
                LossDto dto = new LossDto();

                if(!Objects.equals(returnNumber.getValue(), "")) {
                    dto.setId(Long.parseLong(returnNumber.getValue()));
                }
                dto.setCompanyId(companyComboBox.getValue().getId());
                dto.setWarehouseId(warehouseComboBox.getValue().getId());
                dto.setDate(dateTimePicker.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                log.info(dateTimePicker.getValue().toString());
                dto.setIsSent(checkboxIsSent.getValue());
                dto.setIsPrint(checkboxIsPrint.getValue());
                dto.setComment(textArea.getValue());
                dto.setIsRecyclebin(this.lossDto.getIsRecyclebin());
                dto.setEmployeeId(this.lossDto.getEmployeeId());
                dto.setLastModifiedDate(LocalDateTime.now().toString());
                List<Long> idList = new ArrayList<>(lossProductsIdComboBox.getValue());
                dto.setLossProductsIds(idList);
                file.writeChanges(getChanges(dto, file), employeeService.getPrincipal());
                lossService.create(dto);
                close();
                clearAllFieldsModalView();
                notifications.infoNotification("Списание сохранено");
                UI.getCurrent().navigate(GOODS_LOSS_VIEW);
            }
        });
    }

    private Map<String, String[]> getChanges(LossDto dto, FileWithChanges file) {
        Function<Boolean, String> isTrue = bool -> bool ? "Да" : "Нет";
        // Берём все нужные нам поля и приводим их к тому виду, которые хотим видеть в файле с изменениями
        String[] fields = new String[] {"Номер", "Дата", "Склад", "Организация", "Отправлено", "Напечатано",
                "Комментарий"};
        Object[] oldValues = new Object[] {lossDto.getId(), lossDto.getDate(), this.warehouseService.getById(lossDto.getWarehouseId()).getName(),
                this.companyService.getById(lossDto.getCompanyId()).getName(), isTrue.apply(lossDto.getIsSent()),
                isTrue.apply(lossDto.getIsPrint()), lossDto.getComment()};
        Object[] newValues = {dto.getId(), dto.getDate(), this.warehouseService.getById(dto.getWarehouseId()).getName(),
                this.companyService.getById(dto.getCompanyId()).getName(), isTrue.apply(dto.getIsSent()),
                isTrue.apply(dto.getIsPrint()), dto.getComment()};
        return file.findChanges(fields, oldValues, newValues);
    }

    private com.vaadin.flow.component.button.Button closeButton() {
        com.vaadin.flow.component.button.Button button = new Button("Закрыть", new Icon(VaadinIcon.CLOSE));
        button.addClickListener(e -> {
            close();
            clearAllFieldsModalView();
        });
        return button;
    }

    private Button addGoodButton() {
        Button button = new Button("Добавить продукт", new Icon(VaadinIcon.PLUS));
        button.addClickListener(e -> {
            close();
            LossProductModalWindow modalView = new LossProductModalWindow(
                    productService,
                    companyService,
                    warehouseService,
                    lossService,
                    notifications,
                    lossProductService,
                    employeeService
            );
            modalView.open();
        });
        return button;
    }

    private HorizontalLayout formLayout1() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(numberConfigure(), dateConfigure(), checkboxLayout(), checkboxPrintLayout(),
                layoutForLinkCreatorAndLinkChanges());
        return horizontalLayout;
    }

    private HorizontalLayout formLayout2() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(companyConfigure(), warehouseConfigure());
        return horizontalLayout;
    }

    private HorizontalLayout formLayout4() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(lossProductsConfigure(),commentConfig());
        return horizontalLayout;
    }

    private HorizontalLayout numberConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        com.vaadin.flow.component.html.Label label = new com.vaadin.flow.component.html.Label("Списание №");
        label.setWidth("150px");
        returnNumber.setWidth("50px");
        horizontalLayout.add(label, returnNumber);
        lossDtoBinder.forField(returnNumber)
                .asRequired(TEXT_FOR_REQUEST_FIELD);
        //
        return horizontalLayout;
    }

    private HorizontalLayout dateConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        com.vaadin.flow.component.html.Label label = new com.vaadin.flow.component.html.Label("От");
        dateTimePicker.setWidth("350px");
        dateTimePicker.setStep(Duration.ofSeconds(1));
        horizontalLayout.add(label, dateTimePicker);
        lossDtoBinder.forField(dateTimePicker)
                .asRequired(TEXT_FOR_REQUEST_FIELD);
        //
        return horizontalLayout;
    }
    private VerticalLayout checkboxLayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(checkboxIsSent);
        return verticalLayout;
    }

    private VerticalLayout checkboxPrintLayout() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(checkboxIsPrint);
        return verticalLayout;
    }

    private HorizontalLayout layoutForLinkCreatorAndLinkChanges() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Map<String, Object> lastChanges = file.getLastChange();
        LinkedTreeMap<String, String> employee = (LinkedTreeMap) lastChanges.get("employee_who_changes_document");
        String employeeName = employee.get("name");
        Paragraph forCreator = new Paragraph();
        Paragraph forChanges = new Paragraph();
        forCreator.setText("Создал " + employeeService.getPrincipal().getLastName());
        forCreator.getStyle().set("color", "blue");
        forChanges.setText(String.format("%s: %s%n%s", lastChanges.get("operation_type"), employeeName, lastChanges.get("modified_date")));
        forChanges.getStyle().set("color", "blue");
        forCreator.addClickListener(event -> notifications.infoNotification("В разработке"));
        forChanges.addClickListener(event -> log.info("ALL CHANGES " + file.getAllChanges()));
        horizontalLayout.add(forCreator, forChanges);
        return horizontalLayout;
    }

    private HorizontalLayout companyConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        java.util.List<CompanyDto> list = companyService.getAll();
        if (list != null) {
            companyComboBox.setItems(list);
        }
        companyComboBox.setItemLabelGenerator(CompanyDto::getName);
        companyComboBox.setWidth("350px");
        com.vaadin.flow.component.html.Label label = new com.vaadin.flow.component.html.Label("Организация");
        label.setWidth("100");
        horizontalLayout.add(label, companyComboBox);
        lossDtoBinder.forField(companyComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD);
        //
        return horizontalLayout;
    }

    private HorizontalLayout warehouseConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<WarehouseDto> list = warehouseService.getAll();
        if (list != null) {
            warehouseComboBox.setItems(list);
        }
        warehouseComboBox.setItemLabelGenerator(WarehouseDto::getName);
        warehouseComboBox.setWidth("350px");
        com.vaadin.flow.component.html.Label label = new com.vaadin.flow.component.html.Label("Склад");
        label.setWidth("50");
        horizontalLayout.add(label, warehouseComboBox);
        lossDtoBinder.forField(warehouseComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD);
        //
        return horizontalLayout;
    }

    private HorizontalLayout commentConfig() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        com.vaadin.flow.component.html.Label label = new Label("Причина списания");
        label.setWidth("100px");
        horizontalLayout.setWidth("750px");
        horizontalLayout.setHeight("100px");
        horizontalLayout.add(label, textArea);
        lossDtoBinder.forField(textArea);
        return horizontalLayout;
    }
    private void clearAllFieldsModalView() {
        companyComboBox.setValue(null);
        warehouseComboBox.setValue(null);
        dateTimePicker.setValue(null);
        textArea.setValue("");
        returnNumber.setValue("");
        checkboxIsSent.setValue(false);
        checkboxIsPrint.setValue(false);
    }

    private HorizontalLayout  lossProductsConfigure() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        List<LossDto> iod = lossService.getAll();
        List<Long> checkInIod = new ArrayList<>();
        for(LossDto id : iod) {
            List<Long> list = id.getLossProductsIds();
            checkInIod.addAll(list);
        }

        List<LossProductDto> labels = lossProductService.getAll();
        List<Long> items = new ArrayList<>();
        for(LossProductDto id : labels) {
            Long check = id.getId();
            if(!(checkInIod.contains(check))) {
                items.add(id.getId());
            }
        }

        lossProductsIdComboBox.setItems(items);
        lossProductsIdComboBox.setItemLabelGenerator(item -> productService.getById(lossProductService
                .getById(item).getProductId()).getName());
        lossProductsIdComboBox.setWidth("350px");
        Label label = new Label("Список товаров");
        label.setWidth("100px");
        horizontalLayout.add(label, lossProductsIdComboBox);

        lossDtoBinder.forField(lossProductsIdComboBox)
                .asRequired(TEXT_FOR_REQUEST_FIELD)
                .bind(lossDto -> new HashSet<>(lossDto.getLossProductsIds()),
                        (lossDto, lossDto1) -> lossDto.setLossProductsIds(lossDto
                                .getLossProductsIds()));
        UI.getCurrent().navigate(GOODS_LOSS_VIEW);
        return horizontalLayout;
    }
}
