package com.trade_accounting.components.goods.print;
import com.trade_accounting.components.util.PrintExcelDocument;
import com.trade_accounting.models.dto.warehouse.ProductDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import org.apache.poi.ss.usermodel.Cell;
import java.time.LocalDateTime;
import java.util.List;

public class PrintGoodsXls extends PrintExcelDocument<ProductDto> {

    private final EmployeeService employeeService;

    public PrintGoodsXls(String pathToXlsTemplate, List<ProductDto> list, EmployeeService employeeService) {
        super(pathToXlsTemplate, list);
        this.employeeService = employeeService;
    }

    @Override
    protected void selectValue(Cell editCell) {
        String formula = editCell.getStringCellValue();
        switch (formula) {
            case ("<date>"):
                editCell.setCellValue(LocalDateTime.now());
                break;
            case ("<authorName>"):
                editCell.setCellValue(employeeService.getPrincipal().getEmail());
                break;
            default:
        }
    }

    @Override
    protected void tableSelectValue(String value, ProductDto model, Cell editCell) {
        switch (value) {
            case ("<id>"):
                editCell.setCellValue(model.getId());
                break;
            case ("<name>"):
                editCell.setCellValue(model.getName());
                break;
            case ("<weight>"):
                editCell.setCellValue(String.valueOf(model.getWeight()));
                break;
            case ("<item_number>"):
                editCell.setCellValue(String.valueOf(model.getItemNumber()));
                break;
            case ("<description>"):
                editCell.setCellValue(model.getDescription());
                break;
            case ("<volume>"):
                editCell.setCellValue(String.valueOf(model.getVolume()));
                break;
            case ("<purchase_price>"):
                editCell.setCellValue(String.valueOf(model.getPurchasePrice()));
                break;
            default:
        }
    }
}
