package com.trade_accounting.components.apps.impl.warehouse;


import com.trade_accounting.components.apps.impl.CallExecuteService;
import com.trade_accounting.models.dto.warehouse.ProductPriceDto;
import com.trade_accounting.services.interfaces.warehouse.ProductPriceService;
import com.trade_accounting.services.api.warehouse.ProductPriceApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class ProductPriceServiceImpl implements ProductPriceService {

    private final String productPriceUrl;

    private final ProductPriceApi productPriceApi;

    private final CallExecuteService<ProductPriceDto> dtoCallExecuteService;

    public ProductPriceServiceImpl(@Value("${productprice_url}") String productPriceUrl
            , Retrofit retrofit
            , CallExecuteService<ProductPriceDto> dtoCallExecuteService) {
        this.productPriceUrl = productPriceUrl;
        this.productPriceApi = retrofit.create(ProductPriceApi.class);
        this.dtoCallExecuteService = dtoCallExecuteService;
    }

    @Override
    public List<ProductPriceDto> getAll() {
        Call<List<ProductPriceDto>> productPriceGetAllLiteCall = productPriceApi.getAll(productPriceUrl);
        return dtoCallExecuteService.callExecuteBodyList(productPriceGetAllLiteCall, ProductPriceDto.class);
    }

    @Override
    public ProductPriceDto getById(Long id) {
        Call<ProductPriceDto> productPriceGetCall = productPriceApi.getById(productPriceUrl, id);
        return dtoCallExecuteService.callExecuteBodyById(productPriceGetCall, ProductPriceDto.class, id);
    }

    @Override
    public ProductPriceDto create(ProductPriceDto productPriceDto) {
        Call<ProductPriceDto> productPriceCall = productPriceApi.create(productPriceUrl, productPriceDto);
        Response<ProductPriceDto> resp = Response.success(productPriceDto);

        try {
            resp = productPriceCall.execute();
            log.info(String.format("Успешно выполнен запрос на создание экземпляра %s", ProductPriceDto.class.getSimpleName()));
        } catch (IOException e) {
            log.error(String.format("Произошла ошибка при выполнении запроса на создание экземпляра %s", ProductPriceDto.class.getSimpleName()));
        }

        return resp.body();
    }

    @Override
    public void update(ProductPriceDto productPriceDto) {
        Call<Void> productPriceUpdateCall = productPriceApi.update(productPriceUrl, productPriceDto);
        dtoCallExecuteService.callExecuteBodyUpdate(productPriceUpdateCall, ProductPriceDto.class);
    }

    @Override
    public void deleteById(Long id) {
        Call<Void> productPriceDeleteCall = productPriceApi.deleteById(productPriceUrl, id);
        dtoCallExecuteService.callExecuteBodyDelete(productPriceDeleteCall, ProductPriceDto.class, id);
    }
}
