package com.trade_accounting.components.apps.impl.invoice;

import com.trade_accounting.components.apps.impl.CallExecuteService;
import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.services.api.company.SupplierAccountApi;
import com.trade_accounting.services.api.invoice.InvoicesToBuyersApi;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class InvoicesToBuyersServiceImpl implements InvoicesToBuyersService {

    private final InvoicesToBuyersApi invoices;
    private final String invoicesUrl;
    private final CallExecuteService<InvoicesToBuyersDto> callExecuteService;
    private InvoicesToBuyersDto invoicesToBuyersDto;

    public InvoicesToBuyersServiceImpl(@Value("${invoices_to_buyers_url}") String invoicesUrl, Retrofit retrofit, CallExecuteService<InvoicesToBuyersDto> callExecuteService) {
        invoices = retrofit.create(InvoicesToBuyersApi.class);
        this.invoicesUrl = invoicesUrl;
        this.callExecuteService = callExecuteService;
    }

    @Override
    public List<InvoicesToBuyersDto> getAll() {
        List<InvoicesToBuyersDto> getAllInvoicesToBuyers = new ArrayList<>();
        Call<List<InvoicesToBuyersDto>> getAllInvoicesToBuyersCall = invoices.getAll(invoicesUrl);

        try {
            getAllInvoicesToBuyers = getAllInvoicesToBuyersCall.execute().body();
            log.info("Успешно выполнен запрос на получение списка SupplierAccountsDto");
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на получение списка SupplierAccountsDto - {IOException}", e);
        }
        return getAllInvoicesToBuyers;
    }

    @Override
    public List<InvoicesToBuyersDto> getAll(String typeOfInvoice) {
        List<InvoicesToBuyersDto> invoicesDtoList = new ArrayList<>();
        Call<List<InvoicesToBuyersDto>> invoicesDtoListCall = invoices.getAll(invoicesUrl, typeOfInvoice);
        try {
            invoicesDtoList.addAll(Objects.requireNonNull(invoicesDtoListCall.execute().body()));
            log.info("Успешно выполнен запрос на получение списка InvoicesToBuyersDto");
        } catch (IOException | NullPointerException e) {
            log.error("Попытка перехода на страницу /sells  не авторизованного пользователя  - {NullPointerException}", e);
            log.error("Произошла ошибка при выполнении запроса на получение списка InvoicesToBuyersDto - {IOException}", e);
        }
        invoicesDtoList.removeIf(supp -> Boolean.TRUE.equals(supp.getIsRecyclebin()));
        return invoicesDtoList;
    }

    @Override
    public InvoicesToBuyersDto getById(Long id) {
        Call<InvoicesToBuyersDto> getInvoicesByIdCall = invoices.getById(invoicesUrl, id);
        InvoicesToBuyersDto invoicesToBuyersDto = new InvoicesToBuyersDto();
        try {
            invoicesToBuyersDto = getInvoicesByIdCall.execute().body();
            log.info("Успешно выполнен запрос на поиск и получение счета покупателя invoicesToBuyers по его id {}", id);
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на поиск и получение счета покупателя InvoicesToBuyers" +
                    " по его id {} - {IOException}", id, e);
        }
        return invoicesToBuyersDto;
    }

    @Override
    public List<InvoicesToBuyersDto> searchByString(String nameFilter) {
        Call<List<InvoicesToBuyersDto>> getInvoicesToBuyersByNameFilter = invoices.searchByString(invoicesUrl, nameFilter);
        List<InvoicesToBuyersDto> invoicesToBuyersDto = new ArrayList<>();
        try {
            invoicesToBuyersDto = getInvoicesToBuyersByNameFilter.execute().body();
            log.info("Успешно выполнен запрос на поиск и получение счета покупателя  по фильтру {}", nameFilter);
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на поиск и получение счета покупателя {IOException}", e);
        }
        return invoicesToBuyersDto;
    }

    @Override
    public InvoicesToBuyersDto create(InvoicesToBuyersDto invoicesToBuyersDto) {
        Call<InvoicesToBuyersDto> createInvoicesToBuyers = invoices.create(invoicesUrl, invoicesToBuyersDto);
        try {
            this.invoicesToBuyersDto = createInvoicesToBuyers.execute().body();
            log.info("Успешно выполнен запрос на создание invoicesToBuyers");
        } catch (IOException e) {
            log.error("Произошла ошибка при создании счета покупателя {}", e);
        }
        return this.invoicesToBuyersDto;
    }

    @Override
    public void update(InvoicesToBuyersDto invoicesToBuyersDto) {
        Call<Void> updateInvoicesToBuyers = invoices.update(invoicesUrl, invoicesToBuyersDto);
        try {
            updateInvoicesToBuyers.execute().body();
            log.info("Успешно выполнен запрос на обновление счета покупателя {}", invoicesToBuyersDto);
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на обновление счета покупателя {}", invoicesToBuyersDto);
        }
    }

    @Override
    public void deleteById(Long id) {
        Call<Void> deleteInvoicesToBuyersById = invoices.deleteById(invoicesUrl,id);
        try {
            deleteInvoicesToBuyersById.execute();
            log.info("Успешно выполнен запрос на удаление InvoicesToBuyers  {}", id);
        } catch (IOException e) {
            log.error("Произошла ошибка при отправке запроса на удаление InvoicesToBuyers {} - {}",id, e);
        }
    }

    @Override
    public List<InvoicesToBuyersDto> searchByFilter(Map<String, String> queryInvoicesToBuyers) {
        List<InvoicesToBuyersDto> invoicesToBuyersDtoList = new ArrayList<>();
        Call<List<InvoicesToBuyersDto>> callInvoices = invoices.searchByFilter(invoicesUrl, queryInvoicesToBuyers);
        try {
            invoicesToBuyersDtoList = callInvoices.execute().body();
            log.info("Успешно выполнен запрос на поиск и получение счета покупателя  по фильтру {}", queryInvoicesToBuyers);
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на поиск и получение счета покупателя {IOException}", e);
        }
        invoicesToBuyersDtoList.removeIf(supp -> Boolean.TRUE.equals(supp.getIsRecyclebin()));
        return invoicesToBuyersDtoList;
    }

    @Override
    public List<InvoicesToBuyersDto> findBySearchAndTypeOfInvoice(String search, String typeOfInvoice) {
        List<InvoicesToBuyersDto> invoicesToBuyersDtoList = new ArrayList<>();
        Call<List<InvoicesToBuyersDto>> callInvoices = invoices
                .search(invoicesUrl, search.toLowerCase(), typeOfInvoice);

        try {
            invoicesToBuyersDtoList = callInvoices.execute().body();
            log.info("Успешно выполнен запрос на поиск и получение списка счетов invoice");
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на поиск и получение списка InvoiceDto - ", e);
        }
        return invoicesToBuyersDtoList;
    }

    @Override
    public void moveToIsRecyclebin(Long id) {
        Call<Void> dtoCall = invoices.moveToIsRecyclebin(invoicesUrl, id);
        callExecuteService.callExecuteBodyMoveToIsRecyclebin(dtoCall, InvoicesToBuyersDto.class, id);
    }

    @Override
    public void restoreFromIsRecyclebin(Long id) {
        Call<Void> dtoCall = invoices.restoreFromIsRecyclebin(invoicesUrl, id);
        callExecuteService.callExecuteBodyRestoreFromIsRecyclebin(dtoCall, InvoicesToBuyersDto.class, id);

    }
}
