package com.trade_accounting.components.apps.impl.dadata;

import com.kuliginstepan.dadata.client.DadataClient;
import com.kuliginstepan.dadata.client.domain.Suggestion;
import com.kuliginstepan.dadata.client.domain.bank.Bank;
import com.kuliginstepan.dadata.client.domain.bank.BankRequestBuilder;
import com.trade_accounting.services.interfaces.dadata.DadataBankService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class DadataBankServiceImpl implements DadataBankService {

    private final DadataClient client;
    @Override
    public List<Suggestion<Bank>> getBankInfo(String s) {
        List<Suggestion<Bank>> suggestions =
                client.suggestBank(BankRequestBuilder.create(s).build()).collectList().block();
        return suggestions;

    }
}
