package com.trade_accounting.components.apps.impl.company;

import com.trade_accounting.components.apps.impl.CallExecuteService;
import com.trade_accounting.models.dto.company.SaleTaxDto;
import com.trade_accounting.services.api.company.SaleTaxApi;
import com.trade_accounting.services.interfaces.company.SaleTaxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.util.List;

@Slf4j
@Service
public class SaleTaxServiceImpl implements SaleTaxService {

    private final SaleTaxApi saleTaxApi;

    private final String saleTaxUrl;

    private final CallExecuteService<SaleTaxDto> saleTaxDtoCallExecuteService;

    public SaleTaxServiceImpl(@Value("${sale_tax_url}") String saleTaxUrl, Retrofit retrofit, CallExecuteService<SaleTaxDto> saleTaxDtoCallExecuteService) {
        saleTaxApi = retrofit.create(SaleTaxApi.class);
        this.saleTaxUrl = saleTaxUrl;
        this.saleTaxDtoCallExecuteService = saleTaxDtoCallExecuteService;
    }

    @Override
    public List<SaleTaxDto> getAll() {
        Call<List<SaleTaxDto>> saleTaxDtoListCall = saleTaxApi.getAll(saleTaxUrl);
        return saleTaxDtoCallExecuteService.callExecuteBodyList(saleTaxDtoListCall, SaleTaxDto.class);

    }
}
