package com.trade_accounting.components.apps.impl.invoice;

import com.trade_accounting.components.apps.impl.CallExecuteService;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductListDto;
import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.services.api.invoice.InvoicesToBuyersProductsListApi;
import com.trade_accounting.services.api.warehouse.SupplierAccountProductsListApi;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersProductsListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class InvoicesToBuyersProductsListServiceImpl implements InvoicesToBuyersProductsListService {
    private final InvoicesToBuyersProductsListApi invoicesToBuyersProductsListApi;
    private final String invoicesToBuyersProductsListUrl;
    private final CallExecuteService<InvoicesToBuyersProductListDto> dtoCallExecuteService;

    public InvoicesToBuyersProductsListServiceImpl(@Value("${invoices_to_buyers_products_list_url}") String invoicesToBuyersProductsListUrl, Retrofit retrofit, CallExecuteService<InvoicesToBuyersProductListDto> dtoCallExecuteService){
        invoicesToBuyersProductsListApi = retrofit.create(InvoicesToBuyersProductsListApi.class);
        this.invoicesToBuyersProductsListUrl = invoicesToBuyersProductsListUrl;
        this.dtoCallExecuteService = dtoCallExecuteService;
    }

    @Override
    public List<InvoicesToBuyersProductListDto> getAll() {
        Call<List<InvoicesToBuyersProductListDto>> invoicesToBuyersProductsListCall = invoicesToBuyersProductsListApi.getAll(invoicesToBuyersProductsListUrl);
        return dtoCallExecuteService.callExecuteBodyList(invoicesToBuyersProductsListCall, InvoicesToBuyersProductListDto.class);
    }

    @Override
    public InvoicesToBuyersProductListDto getById(Long id) {
        Call<InvoicesToBuyersProductListDto> invoicesToBuyersProductsListDtoCall = invoicesToBuyersProductsListApi.getById(invoicesToBuyersProductsListUrl, id);
        return dtoCallExecuteService.callExecuteBodyById(invoicesToBuyersProductsListDtoCall, InvoicesToBuyersProductListDto.class, id);
    }

    @Override
    public List<InvoicesToBuyersProductListDto> getByInvoicesId(Long id) {
        List<InvoicesToBuyersProductListDto> invoicesToBuyersProductListDtos = null;
        Call<List<InvoicesToBuyersProductListDto>>  invoicesToBuyersProductsListApiByInvoicesId = invoicesToBuyersProductsListApi.getByInvoicesId(invoicesToBuyersProductsListUrl + "/invoices-to-buyers-products-list", id);
        try{
            invoicesToBuyersProductListDtos = invoicesToBuyersProductsListApiByInvoicesId.execute().body();
            log.info("Успешно выполнен запрос на получение списка InvoiceToBuyerListProductsDto с InvoicesToBuyersId.id = {}", id);
        } catch (IOException e) {
            log.error("Произошла ошибка при выполнении запроса на получение списка InvoiceToBuyerListProductsDto по id= {} - {}", id, e);
        }
        return invoicesToBuyersProductListDtos;
    }

    @Override
    public InvoicesToBuyersProductListDto create(InvoicesToBuyersProductListDto invoicesToBuyersProductListDto) {
        Call<InvoicesToBuyersProductListDto> invoicesToBuyersProductsListCall = invoicesToBuyersProductsListApi
                .create(invoicesToBuyersProductsListUrl, invoicesToBuyersProductListDto);
        return dtoCallExecuteService.callExecuteBodyCreate(invoicesToBuyersProductsListCall, InvoicesToBuyersProductListDto.class);
    }

    @Override
    public void update(InvoicesToBuyersProductListDto invoicesToBuyersProductListDto) {
        Call<Void> InvoicesToBuyersProductsListCall = invoicesToBuyersProductsListApi
                .update(invoicesToBuyersProductsListUrl, invoicesToBuyersProductListDto);
        dtoCallExecuteService.callExecuteBodyUpdate(InvoicesToBuyersProductsListCall, InvoicesToBuyersProductListDto.class);
    }

    @Override
    public void deleteById(Long id) {
        Call<Void> invoicesToBuyersProductsListCall = invoicesToBuyersProductsListApi.deleteById(invoicesToBuyersProductsListUrl, id);
        dtoCallExecuteService.callExecuteBodyDelete(invoicesToBuyersProductsListCall,InvoicesToBuyersProductListDto.class, id);
    }

    @Override
    public List<InvoicesToBuyersProductListDto> search(Map<String, String> query) {
        List<InvoicesToBuyersProductListDto> invoicesToBuyersProductListDtoList = null;
        Call<List<InvoicesToBuyersProductListDto>> callDtoList = invoicesToBuyersProductsListApi.search(invoicesToBuyersProductsListUrl, query);
        try {
            invoicesToBuyersProductListDtoList = callDtoList.execute().body();
            log.info("Успешно выполнен запрос на поиск и получение списка InvoiceToBuyerListProductsDto по ФИЛЬТРУ -{}", query);
        } catch (Exception e) {
            log.error("Произошла ошибка при выполнении запроса ФИЛЬТРА на поиск и получение списка InvoiceToBuyerListProductsDto - ");
        }
        return invoicesToBuyersProductListDtoList;
    }










}
