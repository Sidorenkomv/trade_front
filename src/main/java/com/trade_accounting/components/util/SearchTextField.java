package com.trade_accounting.components.util;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.apache.poi.sl.usermodel.Placeholder;

import java.util.function.Consumer;

public class SearchTextField extends TextField {

    public static TextField Search(String placeholder, String width) {
        TextField textField = new TextField();
        textField.setPlaceholder(placeholder);
        textField.setWidth(width);
        textField.addThemeVariants(TextFieldVariant.MATERIAL_ALWAYS_FLOAT_LABEL);
        textField.setValueChangeMode(ValueChangeMode.EAGER);
        textField.setClearButtonVisible(true);
        textField.setSizeFull();
        return textField;
    }
}
