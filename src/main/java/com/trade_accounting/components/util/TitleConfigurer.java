package com.trade_accounting.components.util;

import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;

public class TitleConfigurer {
    public static H2 titleH2(String message) {
        H2 title = new H2(message);
        title.setHeight("2.2em");
        return title;
    }

    public static H3 titleH3(String message) {
        H3 title = new H3(message);
        title.setHeight("2.2em");
        return title;
    }

    public static H4 titleH4(String message) {
        H4 title = new H4(message);
        title.setHeight("2.2em");
        title.setWidth("80px");
        return title;
    }
}
