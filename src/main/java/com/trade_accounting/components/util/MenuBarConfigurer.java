package com.trade_accounting.components.util;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;


public class MenuBarConfigurer {

//    Создает горизонтальный блок меню баром
    public static HorizontalLayout getMenuBar(Component... components) {
        HorizontalLayout toolsUp = new HorizontalLayout();
        toolsUp.add(components);
        toolsUp.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        return toolsUp;
    }


}
