package com.trade_accounting.components.util;

import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.data.binder.Binder;

import java.util.List;
import java.util.Objects;


public class ComponentFactory<T> {
    public static Label configureLabel(String text, String width) {
        Label label = new Label(text);
        label.setWidth(width);
        return label;
    }

    public ComboBox<T> createConfiguredSelect(BasicOperationsService<T> service,
                                              String width,
                                              Binder<T> binder,
                                              String bindDto,
                                              ItemLabelGenerator<T> generator) {
        return createConfiguredSelect(service, width, new ComboBox<>().getHeight(), binder, bindDto, generator);
    }

    public ComboBox<T> createConfiguredSelect(BasicOperationsService<T> service,
                                              String width,
                                              String height,
                                              Binder<T> binder,
                                              String bindDto,
                                              ItemLabelGenerator<T> generator) {
        List<T> data = service.getAll();
        ComboBox<T> select = new ComboBox<>();
        if (data != null) {
            select.setItems(data);
        }
        select.setItemLabelGenerator(generator);
        select.setWidth(width);
        select.setHeight(height);
        if (binder != null) {
            binder.forField(select)
                    .withValidator(Objects::nonNull, "Не заполнено!")
                    .bind(bindDto);
        }
        return select;
    }
}
