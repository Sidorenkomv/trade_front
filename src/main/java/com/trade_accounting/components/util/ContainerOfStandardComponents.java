package com.trade_accounting.components.util;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;


public class ContainerOfStandardComponents {

//    Создает стандартный горизонтальный блок с описанием, кнопкой знака вопроса и кнопкой обновления grid
    public static HorizontalLayout getStandardComponents(Button buttonQuestion, H2 titleH2, Button buttonRefresh) {
        HorizontalLayout toolsUp = new HorizontalLayout();
        toolsUp.add
                (
                        buttonQuestion,
                        titleH2,
                        buttonRefresh
                );
        toolsUp.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        return toolsUp;
    }


}
