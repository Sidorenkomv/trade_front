package com.trade_accounting.components.util.configure.components.input.number;

import com.trade_accounting.components.util.configure.components.button.ButtonExt;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextFieldVariant;

public class NumberFieldConfigurer {

    public static NumberFieldExt configureNumberFieldWithPlaceholderAndAlignRight(String placeholder){
        return new NumberFieldExt.NumberFieldBuilder()
                .numberFieldPlaceholder(placeholder)
                .numberFieldThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT)
                .build();
    }

    public static NumberField NumberFieldStandart (){
        NumberField numberField = new NumberFieldExt.NumberFieldBuilder()
                .numberFieldPlaceholder("0")
                .numberFieldThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT)
                .build()
                .setNumberFieldStyle("width", "75px");
        return numberField;
    }

}
