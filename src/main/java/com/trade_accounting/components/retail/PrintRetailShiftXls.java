package com.trade_accounting.components.retail;

import com.trade_accounting.components.util.PrintExcelDocument;
import com.trade_accounting.models.dto.retail.RetailShiftDto;
import com.trade_accounting.services.interfaces.client.EmployeeService;
import com.trade_accounting.services.interfaces.company.CompanyService;
import com.trade_accounting.services.interfaces.retail.RetailStoreService;
import com.trade_accounting.services.interfaces.warehouse.WarehouseService;
import org.apache.poi.ss.usermodel.Cell;


import java.time.LocalDateTime;
import java.util.List;

public class PrintRetailShiftXls extends PrintExcelDocument<RetailShiftDto> {
    private final CompanyService companyService;

    private final RetailStoreService retailStoreService;

    private final WarehouseService warehouseService;

    private final EmployeeService employeeService;

    protected PrintRetailShiftXls(String pathToXlsTemplate, List<RetailShiftDto> list, CompanyService companyService, RetailStoreService retailStoreService, WarehouseService warehouseService, EmployeeService employeeService) {
        super(pathToXlsTemplate, list);
        this.companyService = companyService;
        this.retailStoreService = retailStoreService;
        this.warehouseService = warehouseService;
        this.employeeService = employeeService;
    }

    @Override
    protected void selectValue(Cell editCell) {
        String formula = editCell.getStringCellValue();
        switch (formula) {
            case ("<date>"):
                editCell.setCellValue(LocalDateTime.now());
                break;
            case ("<authorName>"):
                editCell.setCellValue(employeeService.getPrincipal().getEmail());
                break;
        }
    }

    @Override
    protected void tableSelectValue(String value, RetailShiftDto model, Cell editCell) {
        switch (value) {
            case ("<id>"):
                editCell.setCellValue(model.getId());
                break;
            case ("<date open>"):
                editCell.setCellValue(model.getDataOpen());
                break;
            case ("<date close>"):
                editCell.setCellValue(model.getDataClose());
                break;
            case ("<point of sale>"):
                editCell.setCellValue(retailStoreService.getById(model.getRetailStoreId()).getName());
                break;

            case ("<warehouse>"):
                editCell.setCellValue(warehouseService.getById(model.getCompanyId()).getName());
                break;

            case ("<company>"):
                editCell.setCellValue(companyService.getById(model.getCompanyId()).getName());
                break;

            case ("<acquiring bank>"):
                editCell.setCellValue(model.getBank());
                break;

            case ("<revenue per shift>"):
                editCell.setCellValue(String.valueOf(model.getRevenuePerShift()));
                break;

            case ("<received>"):
                editCell.setCellValue(String.valueOf(model.getReceived()));
                break;

            case ("<discount amount>"):
                editCell.setCellValue(String.valueOf(model.getAmountOfDiscounts()));
                break;

            case ("<commission amount>"):
                editCell.setCellValue(String.valueOf(model.getCommission_amount()));
                break;

            case ("<comment>"):
                editCell.setCellValue(model.getComment());
                break;
        }
    }
}

