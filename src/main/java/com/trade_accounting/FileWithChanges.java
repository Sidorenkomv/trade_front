package com.trade_accounting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.trade_accounting.models.dto.client.EmployeeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Setter
@Getter
@Slf4j
public class FileWithChanges {
    private static final String FILE_TYPE = "json";
    private static final String DIRECTORY = "C:\\Users\\Plaho\\IdeaProjects\\KataProject\\trade_front-dev\\src\\main\\resources\\changes";
    private final Long documentId;
    private final String documentType;
    private final String dirName;
    private final String operationType; // на данный момент только изменение и создание документа
    private final LocalDateTime modifiedDate;
    private final Path pathToFile;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    private final Type type = new TypeToken<Map<String, Map<String, Object>>>(){}.getType();

    public FileWithChanges(Long documentId,
                           String dirName,
                           String documentType,
                           String operationType) {
        this.documentId = documentId;
        this.dirName = dirName;
        this.documentType = documentType;
        this.operationType = operationType;
        this.pathToFile = Paths.get(DIRECTORY, dirName, getDocumentName());
        this.modifiedDate = LocalDateTime.now();
        createDirectory();
    }

    private String getDocumentName() {
        return String.format("%d) %s.%s", documentId, documentType, FILE_TYPE);
    }

    private void createDirectory() {
        Path dirDoc = Paths.get(DIRECTORY, dirName);
        try {
            if (Files.notExists(dirDoc)) {
                Files.createDirectory(dirDoc);
            }
        } catch (IOException e) {
            log.error(String.format("При создании каталога %s возникла ошибка%n%s", dirName, e));
        }
    }

    private String getFinalTextOfCurrentChanges(Map<String, String[]> changes, EmployeeDto employeeWhoChangedDocument) {
        StringBuilder result = new StringBuilder(String.format("%s (%s, %s)%n",
                operationType, employeeWhoChangedDocument.getLastName(), modifiedDate));
        for (String key : changes.keySet()) {
            String[] values = changes.get(key);
            result.append(String.format("field_name: %s\told_value: %s\tnew_value: %s%n", key, values[0], values[1]));
        }
        return result.toString();
    }

    private Map<String, Map<String, Object>> getPreviousData() {
        Gson previousJson = new Gson();
        Map<String, Map<String, Object>> data = new HashMap<>();
        try {
            if (Files.notExists(pathToFile)) {
                Files.createFile(pathToFile);
            } else {
                data = previousJson.fromJson(Files.readString(pathToFile), type);
            }
        } catch (IOException e) {
            log.error(String.format("Не удалось распарсить данные из файла %s%n%s", getDocumentName(), e.getMessage()));
        }
        return data;
    }

    private Map<String, Object> getInfoAboutChange(Map<String, String[]> changes, EmployeeDto employeeWhoChangedDocument) {
        Map<String, Object> info = new HashMap<>();
        Map<String, String> minInfoAboutEmployee = new HashMap<>();
        minInfoAboutEmployee.put("id", employeeWhoChangedDocument.getId().toString());
        minInfoAboutEmployee.put("name", String.format("%s %s", employeeWhoChangedDocument.getLastName(),
                                                                employeeWhoChangedDocument.getFirstName()));
        info.put("operation_type", operationType);
        info.put("employee_who_changes_document", minInfoAboutEmployee);
        Map<String, Object> field = new HashMap<>();
        for (String key : changes.keySet()) {
            String[] values = changes.get(key);
            Map<String, Object> value = new HashMap<>();
            value.put("old", values[0]);
            value.put("new", values[1]);
            field.put(key, value);
        }
        info.put("changes", field);
        info.put("modified_date", modifiedDate.format(dateTimeFormatter));
        return info;
    }

    public void deleteFile() {
        try {
            Files.deleteIfExists(pathToFile);
            log.info(String.format("Удален файл с изменениями %s", pathToFile));
        } catch (IOException e) {
            log.error(String.format("Не удалось удалить файл %s%n%s", pathToFile, e.getMessage()));
        }
    }

    public Map<String, Object> getLastChange() {
        Map<String, Map<String, Object>> data;
        try {
            data = new Gson().fromJson(Files.readString(pathToFile), type);
            List<String> dates = new ArrayList<>(data.keySet());
            dates.sort(Comparator.comparing(s -> LocalDateTime.parse(s, dateTimeFormatter)));

            return data.get(dates.get(dates.size() - 1));
        } catch (IOException e) {
            log.error("Не удалось прочитать файл {1}\n{1}", pathToFile, e.getMessage());
        }
        return new HashMap<>();
    }

    public Map<String, Map<String, Object>> getAllChanges() {
        try {
            return new Gson().fromJson(Files.readString(pathToFile), type);
        } catch (IOException e) {
            log.error(String.format("Не удалось прочитать файл %s%n%s", pathToFile, e.getMessage()));
        }
        return new HashMap<>();
    }

    public void writeChanges(Map<String, String[]> changes, EmployeeDto employeeWhoChangedDocument) {
        if (changes.isEmpty() && !operationType.equals("Создание")) {
            return;
        }
        Gson resultJson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        Map<String, Map<String, Object>> data = getPreviousData();
        Map<String, Object> info = getInfoAboutChange(changes, employeeWhoChangedDocument);
        data.put(modifiedDate.format(dateTimeFormatter), info);
        try {
            Files.write(pathToFile, resultJson.toJson(data).getBytes(StandardCharsets.UTF_8));
            log.info(String.format("Изменения в файл %s записаны%n%s", pathToFile,
                    getFinalTextOfCurrentChanges(changes, employeeWhoChangedDocument)));
        } catch (IOException e) {
            log.error(String.format("Не удалось записать изменения в файл %s%n%s", pathToFile, e));
        }
    }

    public Map<String, String[]> findChanges(String[] fields, Object[] oldValues, Object[] newValues) {
        Map<String, String[]> changes = new HashMap<>();
        log.info(String.format("OLD%s%nNEW%s", Arrays.toString(oldValues), Arrays.toString(newValues)));
        for (int i = 0; i < fields.length; i++) {
            String oldValue = oldValues[i].toString();
            String newValue = newValues[i].toString();
            if (!oldValue.equals(newValue)) {
                changes.put(fields[i], new String[] {oldValue, newValue});
            }
        }
        log.info("CHANGES: " + changes);
        return changes;
    }
}
