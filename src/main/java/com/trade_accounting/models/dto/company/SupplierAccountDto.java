package com.trade_accounting.models.dto.company;

import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SupplierAccountDto extends SupplierAccountDtoForValidation {

    private Long id;

    private String date;

    private Long companyId;

    private Long warehouseId;

    private Long contractId;

    private Long projectId;

    private Long contractorId;

    private Boolean isSpend;

    private String comment;

    @EqualsAndHashCode.Exclude
    private WarehouseDto warehouseDto;

    @EqualsAndHashCode.Exclude
    private ContractorDto contractorDto;

    @EqualsAndHashCode.Exclude
    private ProjectDto projectDto;


    @EqualsAndHashCode.Exclude
    private CompanyDto companyDto;

    @EqualsAndHashCode.Exclude
    private ContractDto contractDto;

    private String typeOfInvoice;

    private String plannedDatePayment;


    private Boolean isRecyclebin;

    private Boolean isSent;

    private Boolean isPrint;

    private Long employeeId;

    private String lastModifiedDate;
}
