package com.trade_accounting.models.dto.production;

import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TechnicalCardDtoForValidation {
    private String idValid;

    private LocalDateTime dateValid;

    private TechnicalCardDto technicalCardDtoValid;

    private WarehouseDto warehouseDtoValid;
}
