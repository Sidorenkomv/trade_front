package com.trade_accounting.models.dto.invoice;


import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.company.ContractDto;
import com.trade_accounting.models.dto.company.ContractorDto;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoicesToBuyersDto extends InvoicesToBuyersDtoForValidation {

    private Long id;

    private String date;

    private Long companyId;

    private Long warehouseId;

    private Long contractId;

    private Long projectId;

    private Long contractorId;

    private Boolean isSpend;

    private String comment;

    @EqualsAndHashCode.Exclude
    private WarehouseDto warehouseDto;

    @EqualsAndHashCode.Exclude
    private ContractorDto contractorDto;

    @EqualsAndHashCode.Exclude
    private ProjectDto projectDto;


    @EqualsAndHashCode.Exclude
    private CompanyDto companyDto;

    @EqualsAndHashCode.Exclude
    private ContractDto contractDto;

    private String typeOfInvoice;

    private String plannedDatePayment;

    private Boolean isRecyclebin;

    private Boolean isSent;

    private Boolean isPrint;

    private Long salesChannelId;

    private Long employeeId;

    private String lastModifiedDate;
}
