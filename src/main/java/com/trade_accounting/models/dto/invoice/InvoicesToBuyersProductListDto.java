package com.trade_accounting.models.dto.invoice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoicesToBuyersProductListDto {
    private Long id;

    private Long InvoicesToBuyersId;

    private Long ProductId;

    private BigDecimal amount;

    private BigDecimal price;

    private BigDecimal sum;

    private String percentNds;

    private BigDecimal nds;

    private BigDecimal total;
}
