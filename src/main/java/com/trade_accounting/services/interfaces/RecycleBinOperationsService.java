package com.trade_accounting.services.interfaces;

public interface RecycleBinOperationsService {
    void moveToIsRecyclebin(Long id);

    void restoreFromIsRecyclebin(Long id);
}
