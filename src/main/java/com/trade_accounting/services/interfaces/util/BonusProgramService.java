package com.trade_accounting.services.interfaces.util;

import com.trade_accounting.models.dto.util.BonusProgramDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface BonusProgramService extends BasicOperationsService<BonusProgramDto> {

}
