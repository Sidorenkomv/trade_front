package com.trade_accounting.services.interfaces.util;

import com.trade_accounting.models.dto.util.DiscountDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface DiscountService extends BasicOperationsService<DiscountDto> {

}
