package com.trade_accounting.services.interfaces.util;

import com.trade_accounting.models.dto.util.ImageDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface ImageService extends BasicOperationsService<ImageDto> {

}