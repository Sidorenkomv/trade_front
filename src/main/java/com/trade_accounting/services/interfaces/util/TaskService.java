package com.trade_accounting.services.interfaces.util;

import com.trade_accounting.models.dto.util.TaskDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface TaskService extends BasicOperationsService<TaskDto> {
    List<TaskDto> searchBy(String searchTask);

    List<TaskDto> searchByFilter(Map<String, String> query);
}