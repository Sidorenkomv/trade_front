package com.trade_accounting.services.interfaces.util;

import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ProjectService extends BasicOperationsService<ProjectDto> {
    List<ProjectDto> findByArchiveFalse();

    List<ProjectDto> findBySearch(String search);

    List<ProjectDto> search(Map<String, String> query, Boolean archive);
}
