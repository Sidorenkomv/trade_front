package com.trade_accounting.services.interfaces.util;

import com.trade_accounting.models.dto.util.TaskCommentDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface TaskCommentService extends BasicOperationsService<TaskCommentDto> {

}
