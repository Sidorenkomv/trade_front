package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface WarehouseService extends BasicOperationsService<WarehouseDto> {
    List<WarehouseDto> findBySearch(String search);

    List<WarehouseDto> search(Map<String, String> query);
}
