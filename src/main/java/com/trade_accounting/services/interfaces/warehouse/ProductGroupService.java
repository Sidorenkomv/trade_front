package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.ProductGroupDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface ProductGroupService extends BasicOperationsService<ProductGroupDto> {

}
