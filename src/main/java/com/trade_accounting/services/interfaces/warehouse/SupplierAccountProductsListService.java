package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface SupplierAccountProductsListService extends BasicOperationsService<SupplierAccountProductsListDto> {
    List<SupplierAccountProductsListDto> getBySupplierId(Long id);

    List<SupplierAccountProductsListDto> search(Map<String, String> query);
}
