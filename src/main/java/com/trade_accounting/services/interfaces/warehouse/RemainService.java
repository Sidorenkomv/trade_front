package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.RemainDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;

public interface RemainService extends BasicOperationsService<RemainDto> {
    List<RemainDto> getAll(String typeOfRemain);

    List<RemainDto> findBySearchAndTypeOfRemain(String search, String typeOfRemain);

}
