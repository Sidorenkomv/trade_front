package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.InventarizationProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface InventarizationProductService extends BasicOperationsService<InventarizationProductDto> {

}
