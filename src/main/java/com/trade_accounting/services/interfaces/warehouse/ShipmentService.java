package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.ShipmentDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface ShipmentService extends BasicOperationsService<ShipmentDto>, RecycleBinOperationsService {
    List<ShipmentDto> getAll(String typeOfInvoice);

    List<ShipmentDto> searchByFilter(Map<String,String> queryShipment);

    List<ShipmentDto> searchByString(String nameFilter);
}
