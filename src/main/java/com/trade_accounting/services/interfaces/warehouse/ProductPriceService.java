package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.ProductPriceDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface ProductPriceService extends BasicOperationsService<ProductPriceDto> {

}
