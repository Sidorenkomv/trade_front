package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.InventarizationDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface InventarizationService extends BasicOperationsService<InventarizationDto>, RecycleBinOperationsService {
    List<InventarizationDto> searchByFilter(Map<String, String> queryInventarization);

    List<InventarizationDto> searchByBetweenDataFilter(Map<String, String> queryInventarization);

    List<InventarizationDto> search(String search);
}
