package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.models.dto.warehouse.MovementDto;
import com.trade_accounting.models.dto.warehouse.WarehouseDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface MovementService extends BasicOperationsService<MovementDto>, RecycleBinOperationsService {
    List<MovementDto> searchByFilter(Map<String, String> queryMovement);

    List<MovementDto> searchByBetweenDataFilter(Map<String, String> queryMovement);

    void updateTorg13(MovementDto movementDto, CompanyDto companyDto, WarehouseDto warehouseDto);
}
