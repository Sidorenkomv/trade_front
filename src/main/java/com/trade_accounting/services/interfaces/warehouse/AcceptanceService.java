package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.AcceptanceDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface AcceptanceService extends BasicOperationsService<AcceptanceDto>, RecycleBinOperationsService {
    List<AcceptanceDto> getByProjectId(Long id);

    List<AcceptanceDto> searchByFilter(Map<String, String> queryAcceptance);

    List<AcceptanceDto> search(String search);
}
