package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.BuyersReturnDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface BuyersReturnService extends BasicOperationsService<BuyersReturnDto> {
    List<BuyersReturnDto> findBySearch(String search);

    List<BuyersReturnDto> getByContractorId(Long id);

    List<BuyersReturnDto> searchByFilter(Map<String, String> map);

}
