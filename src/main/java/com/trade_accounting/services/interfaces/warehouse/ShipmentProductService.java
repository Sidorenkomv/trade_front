package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.ShipmentProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface ShipmentProductService extends BasicOperationsService<ShipmentProductDto> {

}
