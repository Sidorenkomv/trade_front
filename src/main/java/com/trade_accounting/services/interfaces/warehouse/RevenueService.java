package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.RevenueDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RevenueService extends BasicOperationsService<RevenueDto> {
    List<RevenueDto> search(Map<String, String> query);
}