package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.MovementProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface MovementProductService extends BasicOperationsService<MovementProductDto> {

}
