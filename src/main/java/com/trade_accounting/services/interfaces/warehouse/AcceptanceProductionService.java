package com.trade_accounting.services.interfaces.warehouse;

import com.trade_accounting.models.dto.warehouse.AcceptanceProductionDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;

public interface AcceptanceProductionService extends BasicOperationsService<AcceptanceProductionDto> {
    List<AcceptanceProductionDto> search(String query);
}
