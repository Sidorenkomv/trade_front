package com.trade_accounting.services.interfaces.dadata;

import com.kuliginstepan.dadata.client.domain.Suggestion;
import com.kuliginstepan.dadata.client.domain.bank.Bank;

import java.util.List;

public interface DadataBankService {
    List<Suggestion<Bank>> getBankInfo(String s);
}
