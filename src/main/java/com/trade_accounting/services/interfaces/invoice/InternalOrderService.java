package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InternalOrderDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface InternalOrderService extends BasicOperationsService<InternalOrderDto>, RecycleBinOperationsService {
    List<InternalOrderDto> searchByTerm (String searchItem);

    List<InternalOrderDto> searchByFilter(Map<String, String> query);

    List<InternalOrderDto> searchByBetweenDataFilter(Map<String, String> query);
}
