package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductListDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface InvoicesToBuyersProductsListService extends BasicOperationsService<InvoicesToBuyersProductListDto> {
    List<InvoicesToBuyersProductListDto> getByInvoicesId(Long id);

    List<InvoicesToBuyersProductListDto> search(Map<String, String> query);
}
