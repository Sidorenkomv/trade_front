package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface InvoicesToBuyersService extends BasicOperationsService<InvoicesToBuyersDto>, RecycleBinOperationsService {
    List<InvoicesToBuyersDto> getAll(String typeOfInvoice);

    List<InvoicesToBuyersDto> searchByString(String nameFilter);

    List<InvoicesToBuyersDto> searchByFilter(Map<String,String> queryInvoicesToBuyers);

    List<InvoicesToBuyersDto> findBySearchAndTypeOfInvoice(String search, String typeOfInvoice);
}
