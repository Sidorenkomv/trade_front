package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.IssuedInvoiceDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface IssuedInvoiceService extends BasicOperationsService<IssuedInvoiceDto> {

}
