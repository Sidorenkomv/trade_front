package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesStatusDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;


public interface InvoicesStatusService extends BasicOperationsService<InvoicesStatusDto> {

}
