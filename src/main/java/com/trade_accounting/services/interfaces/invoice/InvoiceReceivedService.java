package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InvoiceReceivedDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface InvoiceReceivedService extends BasicOperationsService<InvoiceReceivedDto> {
    List<InvoiceReceivedDto> getAll(String typeOfInvoice);

    List<InvoiceReceivedDto> searchByFilter(Map<String, String> queryInvoice);

    List<InvoiceReceivedDto> searchByString(String nameFilter);
}
