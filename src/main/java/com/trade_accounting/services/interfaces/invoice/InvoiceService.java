package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InvoiceDto;
import com.trade_accounting.models.dto.purchases.PurchaseCreateOrderDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface InvoiceService extends BasicOperationsService<InvoiceDto>, RecycleBinOperationsService {
    List<InvoiceDto> getAll(String typeOfInvoice);

    List<InvoiceDto> search(Map<String, String> query);

    List<InvoiceDto> searchFromDate(LocalDateTime dateTime);

    List<InvoiceDto> findBySearchAndTypeOfInvoice(String search, String typeOfInvoice);

    List<InvoiceDto> getByContractorId(Long id);

    List<InvoiceDto> getByProjectId(Long id);

    void createAll(PurchaseCreateOrderDto purchaseCreateOrderDto);

}
