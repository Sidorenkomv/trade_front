package com.trade_accounting.services.interfaces.invoice;


import com.trade_accounting.models.dto.invoice.InvoiceProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface InvoiceProductService extends BasicOperationsService<InvoiceProductDto> {
    List<InvoiceProductDto> getByInvoiceId(Long id);

    List<InvoiceProductDto> search(Map<String, String> query);

    List<InvoiceProductDto> getByProductId(Long id);
}
