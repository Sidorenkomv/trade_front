package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.CountryDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface CountryService extends BasicOperationsService<CountryDto> {
    List<CountryDto> search(Map<String, String> query);
    List<CountryDto> searchByString(String search);
}
