package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.SalesChannelDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface SalesChannelService extends BasicOperationsService<SalesChannelDto> {
    List<SalesChannelDto> search(Map<String, String> query);
    List<SalesChannelDto> searchByString(String search);
}
