package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.OnlineStoreDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface OnlineStoreService extends BasicOperationsService<OnlineStoreDto> {
    List<OnlineStoreDto> search(Map<String, String> searchQuery);
    List<OnlineStoreDto> searchByString(String search);
}
