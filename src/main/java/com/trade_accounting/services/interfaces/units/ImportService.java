package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.ImportDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ImportService extends BasicOperationsService<ImportDto> {
    List<ImportDto> search(Map<String, String> query);
    List<ImportDto> searchByString(String search);
}
