package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.ExportDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ExportService extends BasicOperationsService<ExportDto> {
    List<ExportDto> search(Map<String, String> query);

    List<ExportDto> findBySearch(String search);
}
