package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.CurrencyDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface CurrencyService extends BasicOperationsService<CurrencyDto> {
    List<CurrencyDto> search(Map<String, String> query);

    List<CurrencyDto> findBySearch(String search);
}
