package com.trade_accounting.services.interfaces.units;

import com.trade_accounting.models.dto.units.UnitDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface UnitService extends BasicOperationsService<UnitDto> {
    List<UnitDto> search(Map<String, String> query);

    List<UnitDto> findBySearch(String search);

    void moveToIsRecyclebin(Long id);
}
