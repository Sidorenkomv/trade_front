package com.trade_accounting.services.interfaces.client;

import com.trade_accounting.models.dto.client.AccountDto;
import com.trade_accounting.models.dto.client.EmployeeDto;

public interface AccountService {

    AccountDto create(AccountDto accountDto, EmployeeDto employeeDto);
}
