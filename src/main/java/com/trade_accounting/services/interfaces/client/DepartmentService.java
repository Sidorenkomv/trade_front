package com.trade_accounting.services.interfaces.client;

import com.trade_accounting.models.dto.client.DepartmentDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface DepartmentService extends BasicOperationsService<DepartmentDto> {

}
