package com.trade_accounting.services.interfaces.client;

import com.trade_accounting.models.dto.client.RoleDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface RoleService extends BasicOperationsService<RoleDto> {

}
