package com.trade_accounting.services.interfaces.client;

import com.trade_accounting.models.dto.client.EmployeeDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.util.PageableService;

import java.util.List;
import java.util.Map;

public interface EmployeeService extends PageableService<EmployeeDto>, BasicOperationsService<EmployeeDto> {
    List<EmployeeDto> search(Map<String, String> query);

    EmployeeDto getPrincipal();

    List<EmployeeDto> findBySearch(String search);
}
