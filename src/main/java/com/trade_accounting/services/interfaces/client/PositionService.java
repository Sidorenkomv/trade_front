package com.trade_accounting.services.interfaces.client;

import com.trade_accounting.models.dto.client.PositionDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface PositionService extends BasicOperationsService<PositionDto> {

}
