package com.trade_accounting.services.interfaces;

import java.util.List;

public interface BasicOperationsService<T> {
    List<T> getAll();

    T getById(Long id);

    T create(T dto);

    void update(T dto);

    void deleteById(Long id);
}
