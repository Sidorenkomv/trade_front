package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailReturnsDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailReturnsService extends BasicOperationsService<RetailReturnsDto> {
    List<RetailReturnsDto> search(String query);

    List<RetailReturnsDto> searchRetailReturns(Map<String, String> queryRetailReturns);
}
