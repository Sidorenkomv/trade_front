package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailCloudCheckDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailCloudCheckService extends BasicOperationsService<RetailCloudCheckDto> {
    List<RetailCloudCheckDto> search(Map<String, String> query);
}
