package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.EventLogDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailEventLogService extends BasicOperationsService<EventLogDto> {
    List<EventLogDto> search(Map<String, String> query);

    List<EventLogDto> findBySearch(String search);
}
