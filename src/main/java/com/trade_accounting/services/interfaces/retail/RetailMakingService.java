package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailMakingDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailMakingService extends BasicOperationsService<RetailMakingDto> {
    List<RetailMakingDto> searchByFilter(Map<String, String> queryRetailMaking);

    List<RetailMakingDto> search(String search);
}
