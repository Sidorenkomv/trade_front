package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailStoreDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailStoreService extends BasicOperationsService<RetailStoreDto> {
    List<RetailStoreDto> search(String query);

    List<RetailStoreDto> searchRetailStoreByFilter(Map<String, String> queryRetailStore);
}
