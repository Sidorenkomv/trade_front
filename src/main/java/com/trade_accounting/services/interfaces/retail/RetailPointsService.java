package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailPointsDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface RetailPointsService extends BasicOperationsService<RetailPointsDto> {

}