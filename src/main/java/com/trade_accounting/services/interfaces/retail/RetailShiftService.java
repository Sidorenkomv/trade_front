package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailShiftDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailShiftService extends BasicOperationsService<RetailShiftDto> {
    List<RetailShiftDto> search(String query);

    List<RetailShiftDto> searchByFilter(Map<String, String> query);

}
