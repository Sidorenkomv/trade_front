package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailOperationWithPointsDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface RetailOperationWithPointsService extends BasicOperationsService<RetailOperationWithPointsDto> {

}
