package com.trade_accounting.services.interfaces.retail;

import com.trade_accounting.models.dto.retail.RetailSalesDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface RetailSalesService extends BasicOperationsService<RetailSalesDto> {
    List<RetailSalesDto> search(String query);

    List<RetailSalesDto> searchRetailSales(Map<String, String> queryRetailSales);
}
