package com.trade_accounting.services.interfaces.purchases;

import com.trade_accounting.models.dto.purchases.PurchaseForecastDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PurchaseForecastService extends BasicOperationsService<PurchaseForecastDto> {
    List<PurchaseForecastDto> search(String query);

    List<PurchaseForecastDto> searchByFilter(Map<String, String> query);
}
