package com.trade_accounting.services.interfaces.purchases;

import com.trade_accounting.models.dto.purchases.PurchaseHistoryOfSalesDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PurchaseHistoryOfSalesService extends BasicOperationsService<PurchaseHistoryOfSalesDto> {
    List<PurchaseHistoryOfSalesDto> search(String query);

    List<PurchaseHistoryOfSalesDto> searchByFilter(Map<String, String> query);
}
