package com.trade_accounting.services.interfaces.purchases;

import com.trade_accounting.models.dto.purchases.PurchaseControlDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PurchaseControlService extends BasicOperationsService<PurchaseControlDto> {
    List<PurchaseControlDto> search(String query);

    List<PurchaseControlDto> searchByFilter(Map<String, String> query);

    List<PurchaseControlDto> newFilter(Map<String, String> query);
}
