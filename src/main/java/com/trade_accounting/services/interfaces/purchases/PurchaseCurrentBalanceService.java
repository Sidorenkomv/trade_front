package com.trade_accounting.services.interfaces.purchases;

import com.trade_accounting.models.dto.purchases.PurchaseCurrentBalanceDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PurchaseCurrentBalanceService extends BasicOperationsService<PurchaseCurrentBalanceDto> {
    List<PurchaseCurrentBalanceDto> search(String query);

    List<PurchaseCurrentBalanceDto> searchByFilter(Map<String, String> query);
}
