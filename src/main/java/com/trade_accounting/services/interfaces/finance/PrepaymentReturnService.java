package com.trade_accounting.services.interfaces.finance;


import com.trade_accounting.models.dto.finance.PrepaymentReturnDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface PrepaymentReturnService extends BasicOperationsService<PrepaymentReturnDto> {

}
