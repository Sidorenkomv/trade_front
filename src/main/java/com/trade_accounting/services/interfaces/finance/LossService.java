package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.LossDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

public interface LossService extends BasicOperationsService<LossDto>, RecycleBinOperationsService {

}
