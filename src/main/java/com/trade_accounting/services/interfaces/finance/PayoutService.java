package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.PayoutDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PayoutService extends BasicOperationsService<PayoutDto> {
    List<PayoutDto> getAllByParameters(String searchTerm);

    List<PayoutDto> searchByFilter(Map<String, String> query);

    List<PayoutDto> findBySearchAndTypeOfPayout(String search, String typeOfPayout);
}
