package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.CorrectionProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface CorrectionProductService extends BasicOperationsService<CorrectionProductDto> {

}
