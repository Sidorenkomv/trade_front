package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.PaymentDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface PaymentService extends BasicOperationsService<PaymentDto>, RecycleBinOperationsService {
    List<PaymentDto> getByProjectId(Long id);

    List<PaymentDto> filter(Map<String, String> query);

    List<PaymentDto> search(String search);
}
