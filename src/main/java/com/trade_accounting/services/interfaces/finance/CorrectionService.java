package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.CorrectionDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface CorrectionService extends BasicOperationsService<CorrectionDto>, RecycleBinOperationsService {
    List<CorrectionDto> searchByFilter(Map<String, String> query);

    List<CorrectionDto> searchByBetweenDataFilter(Map<String, String> query);
}
