package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.BalanceAdjustmentDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface BalanceAdjustmentService extends BasicOperationsService<BalanceAdjustmentDto> {
    List<BalanceAdjustmentDto> searchByString(String nameFilter);

    List<BalanceAdjustmentDto> searchByFilter(Map<String, String> queryBalanceAdjustment);
}
