package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.LossProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface LossProductService extends BasicOperationsService<LossProductDto> {

}
