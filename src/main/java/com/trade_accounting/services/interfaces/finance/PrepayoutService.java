package com.trade_accounting.services.interfaces.finance;


import com.trade_accounting.models.dto.finance.PrepayoutDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface PrepayoutService extends BasicOperationsService<PrepayoutDto> {

}
