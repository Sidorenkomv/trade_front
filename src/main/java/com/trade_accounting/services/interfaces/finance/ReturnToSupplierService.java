package com.trade_accounting.services.interfaces.finance;

import com.trade_accounting.models.dto.finance.ReturnToSupplierDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ReturnToSupplierService extends BasicOperationsService<ReturnToSupplierDto> {
    List<ReturnToSupplierDto> getByProjectId(Long id);

    List<ReturnToSupplierDto> searchByString(String nameFilter);

    List<ReturnToSupplierDto> searchByFilter(Map<String, String> queryReturnToSupplier);
}
