package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.StagesProductionDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface StagesProductionService extends BasicOperationsService<StagesProductionDto> {
    List<StagesProductionDto> search(String query);
    List<StagesProductionDto> searchStagesProduction(Map<String, String> queryStagesProduction);
}
