package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.ProductionTargetsDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ProductionTargetsService extends BasicOperationsService<ProductionTargetsDto> {
    List<ProductionTargetsDto> searchProductionTargets(Map<String, String> queryProductionTargets);

    List<ProductionTargetsDto> search(String query);
}
