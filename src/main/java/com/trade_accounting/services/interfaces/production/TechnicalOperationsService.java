package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.TechnicalOperationsDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface TechnicalOperationsService extends BasicOperationsService<TechnicalOperationsDto>, RecycleBinOperationsService {
    List<TechnicalOperationsDto> search(String query);

    List<TechnicalOperationsDto> searchTechnicalOperations(Map<String, String> queryTechnicalOperations);
}
