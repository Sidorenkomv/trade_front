package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.TechnicalCardProductionDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface TechnicalCardProductionService extends BasicOperationsService<TechnicalCardProductionDto> {

}
