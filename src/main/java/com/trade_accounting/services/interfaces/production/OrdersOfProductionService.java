package com.trade_accounting.services.interfaces.production;


import com.trade_accounting.models.dto.production.OrdersOfProductionDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface OrdersOfProductionService extends BasicOperationsService<OrdersOfProductionDto> {
    List<OrdersOfProductionDto> searchOrdersOfProduction(Map<String, String> queryOrdersOfProduction);

    List<OrdersOfProductionDto> search(String query);
}
