package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.TechnicalCardDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface TechnicalCardService extends BasicOperationsService<TechnicalCardDto> {
    List<TechnicalCardDto> searchTechnicalCard(Map<String, String> queryTechnicalCard);

    List<TechnicalCardDto> search(String searchTerm);

    List<TechnicalCardDto> getAllByTechnicalCardGroupId(Long id);
}
