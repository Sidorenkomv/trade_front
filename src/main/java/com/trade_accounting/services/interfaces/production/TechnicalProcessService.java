package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.TechnicalProcessDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface TechnicalProcessService extends BasicOperationsService<TechnicalProcessDto> {
    List<TechnicalProcessDto> search(String query);
    List<TechnicalProcessDto> searchTechnicalProcess(Map<String, String> queryTechnicalProcess);
}
