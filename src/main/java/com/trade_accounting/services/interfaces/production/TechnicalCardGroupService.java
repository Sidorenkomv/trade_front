package com.trade_accounting.services.interfaces.production;

import com.trade_accounting.models.dto.production.TechnicalCardGroupDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface TechnicalCardGroupService extends BasicOperationsService<TechnicalCardGroupDto> {

}
