package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.ContactDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface ContactService extends BasicOperationsService<ContactDto> {

}
