package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.TaxSystemDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface TaxSystemService extends BasicOperationsService<TaxSystemDto> {

}
