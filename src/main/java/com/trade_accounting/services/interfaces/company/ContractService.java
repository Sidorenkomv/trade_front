package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.ContractDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ContractService extends BasicOperationsService<ContractDto> {
    List<ContractDto> searchByTerm (String searchContr);

    List<ContractDto> search(Map<String, String> query);
}
