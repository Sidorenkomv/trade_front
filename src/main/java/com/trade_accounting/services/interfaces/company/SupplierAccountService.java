package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface SupplierAccountService extends BasicOperationsService<SupplierAccountDto>, RecycleBinOperationsService {
    List<SupplierAccountDto> getAll(String typeOfInvoice);

    List<SupplierAccountDto> searchByString(String nameFilter);

    List<SupplierAccountDto> searchByFilter(Map<String,String> querySupplier);

    List<SupplierAccountDto> findBySearchAndTypeOfInvoice(String search, String typeOfInvoice);
}
