package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.PriceListProductDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PriceListProductService extends BasicOperationsService<PriceListProductDto> {
    List<PriceListProductDto> getByPriceListId(Long id);

    List<PriceListProductDto> search(Map<String, String> query);

    List<PriceListProductDto> getByProductId(Long id);

    List<PriceListProductDto> quickSearch(String text);
}
