package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.TypeOfContractorDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

public interface TypeOfContractorService extends BasicOperationsService<TypeOfContractorDto> {

}
