package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.BankAccountDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;


public interface BankAccountService extends BasicOperationsService<BankAccountDto> {

}
