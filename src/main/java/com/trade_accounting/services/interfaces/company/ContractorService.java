package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.ContractorDto;
import com.trade_accounting.models.dto.fias.FiasModelDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface ContractorService extends BasicOperationsService<ContractorDto> {
    List<ContractorDto> searchContractor(Map<String, String> queryContractor);

    List<ContractorDto> getAllLite();

    List<ContractorDto> searchByTerm(String searchTerm);

    List<FiasModelDto> getAllAddressByLevel(String searchLevel);

    List<FiasModelDto> getAddressesByGuid(String aoguid);
}
