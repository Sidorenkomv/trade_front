package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.SaleTaxDto;

import java.util.List;

public interface SaleTaxService {
    List<SaleTaxDto> getAll();
}
