package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.CompanyDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface CompanyService extends BasicOperationsService<CompanyDto> {
    List<CompanyDto> search(Map<String, String> query);

    CompanyDto getByEmail(String email);
}
