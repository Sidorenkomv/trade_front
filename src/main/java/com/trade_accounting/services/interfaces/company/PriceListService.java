package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.PriceListDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;
import com.trade_accounting.services.interfaces.RecycleBinOperationsService;

import java.util.List;
import java.util.Map;

public interface PriceListService extends BasicOperationsService<PriceListDto>, RecycleBinOperationsService {
    List<PriceListDto> quickSearch(String text);

    List<PriceListDto> searchByFilter(Map<String, String> query);

    List<PriceListDto> searchByBetweenDataFilter(Map<String, String> query);
}
