package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.PriceListProductPercentsDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;

import java.util.List;
import java.util.Map;

public interface PriceListProductPercentsService extends BasicOperationsService<PriceListProductPercentsDto> {
    List<PriceListProductPercentsDto> getByPriceListId(Long id);

    List<PriceListProductPercentsDto> search(Map<String, String> query);
}
