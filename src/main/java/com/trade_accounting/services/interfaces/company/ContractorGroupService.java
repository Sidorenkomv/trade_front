package com.trade_accounting.services.interfaces.company;

import com.trade_accounting.models.dto.company.ContractorGroupDto;
import com.trade_accounting.services.interfaces.BasicOperationsService;


public interface ContractorGroupService extends BasicOperationsService<ContractorGroupDto> {
    ContractorGroupDto getByName(String name);
}