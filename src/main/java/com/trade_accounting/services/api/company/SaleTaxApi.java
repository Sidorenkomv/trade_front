package com.trade_accounting.services.api.company;

import com.trade_accounting.models.dto.company.SaleTaxDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

import java.util.List;

public interface SaleTaxApi {
    @Headers("Accept: application/json")
    @GET("{url}")
    Call<List<SaleTaxDto>> getAll(@Path(value = "url", encoded = true) String url);
}
