package com.trade_accounting.services.api.invoice;

import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import java.util.List;
import java.util.Map;

public interface InvoicesToBuyersApi {

    @Headers("Accept: application/json")
    @GET("{url}")
    Call<List<InvoicesToBuyersDto>> getAll(@Path(value = "url", encoded = true) String url);

    @Headers("Accept: application/json")
    @GET("{url}")
    Call<List<InvoicesToBuyersDto>> getAll(@Path(value = "url", encoded = true) String url, @Query("typeOfInvoice") String typeOfInvoice);

    @Headers("Accept: application/json")
    @GET("{url}/{id}")
    Call<InvoicesToBuyersDto> getById(@Path(value = "url", encoded = true) String url, @Path("id") Long id);

    @Headers("Accept: application/json")
    @POST("{url}")
    Call<InvoicesToBuyersDto> create(@Path(value = "url", encoded = true) String url, @Body InvoicesToBuyersDto supp);

    @Headers("Accept: application/json")
    @PUT("{url}")
    Call<Void> update(@Path(value = "url", encoded = true) String url, @Body InvoicesToBuyersDto supp);

    @Headers("Accept: application/json")
    @DELETE("{url}/{id}")
    Call<Void> deleteById(@Path(value = "url", encoded = true) String url, @Path("id") Long id);

    @Headers("Accept: application/json")
    @GET("{url}/search/{nameFilter}")
    Call<List<InvoicesToBuyersDto>> searchByString(@Path(value = "url", encoded = true) String url,
                                                  @Path(value = "nameFilter", encoded = true) String nameFilter);

    @Headers("Accept: application/json")
    @GET("{url}/queryInvoicesToBuyers")
    Call<List<InvoicesToBuyersDto>> searchByFilter(@Path(value = "url", encoded = true) String url,
                                                  @QueryMap Map<String, String> queryInvoicesToBuyers);

    @Headers("Accept: application/json")
    @GET("{url}/searchByString")
    Call<List<InvoicesToBuyersDto>> search(@Path(value = "url", encoded = true) String url,
                                          @Query("search") String search,
                                          @Query("typeOfInvoice") String typeOfInvoice);

    @Headers("Accept: application/json")
    @PUT("{url}/moveToIsRecyclebin/{id}")
    Call<Void> moveToIsRecyclebin(@Path(value = "url", encoded = true) String url, @Path("id") Long id);

    @Headers("Accept: application/json")
    @PUT("{url}/restoreFromIsRecyclebin/{id}")
    Call<Void> restoreFromIsRecyclebin(@Path(value = "url", encoded = true) String url, @Path("id") Long id);

}
